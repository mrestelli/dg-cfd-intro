# 2D Navier-Stokes DG solver, V1.0 

changelog:

- The solver is now able to write paraview visualization files.
- Parameters for the simulation are now specified in a parameterfile. The simulation is started with:  
  `<path-to-executable> <path-to-parameterfile>`
- The computational domain is still a rectangle, but for each side of the domain,  
     different boundary conditions can be imposed.

The FORD documentation of the code can be found in the `doc/index.html` folder, simply open the file with a browser.

## Compiling

To compile the program, execute here:
```
make 
```
the executable `bin/solve_ns` is generated.

To turn off the debug mode and enable optimization, execute:
```
make clean
make DEBUG=0 
```


## Running the tutorials

1.    Compile the program. Its recommended for speed to ***enable optimization***:  
      `make clean ; make DEBUG=0`
1.    Go to the tutorial case folder:  
      `cd tutorials/<case>`
1.    The parameters for the simulation are given in a parameterfile, for each case called `parameter_<case>.ini`.  
      Start the simulation with:  
      `../../bin/solve_ns parameter_<case>.ini`
1.    Paraview visualization files `<CASE>_*.vtu` will be written.

Go to [tutorials/README.md](tutorials/README.md) for more details on the cases.

## Visualize the result with Paraview

You should have paraview installed on your system. On ubuntu, you simply install it with `sudo apt-get install paraview`.

***The tested version of paraview is 5.0.1***

1.    In the `tutorials` folder, open paraview:  
      `paraview &`
1.    `File->Open...` and open the collection `<CASE>_..vtu`. 
1.    Click `Apply` and -- in the *coloring* section on the left --   
      instead of *Solid Color*, choose a quantity to visualize, for example *Velocity*.
1.    A play buttom lets you animate the time series, and you can also click step by step forth and back. 

In paraview, "Filters" can be applied consecutively on the data, and the whole visualization setup can be saved 
under `File->Save State...` to a `*.pvsm` file. 

For each case in the tutorials folder, there is already a `visu_<case>.pvsm` file.  
You can load the state file under `File->Load State...` and then select under `...` the collection `<CASE>_..vtu` files.

The animation speed can be controlled via `View->Animation View`,and selection real time. 
The end time is the number of files and the duration can be chosen (don't forget to press return when changing it!).
