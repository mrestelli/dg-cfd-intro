
# Run the simulation case

1. Compile the program. Its recommended for speed to ***enable optimization***:  
    `make clean ; make DEBUG=0`
1. Go to the tutorial case folder:  
    `cd tutorials/<case>`
1. The parameters for the simulation are given in a parameterfile, for each case called `parameter_<case>.ini`.  
     Start the simulation with:  
     `../../bin/solve_ns parameter_<case>.ini`
1. Paraview visualization files `<CASE>_*.vtu` will be written.

# Tutorial cases

## Shear

The domain is fully periodic, and the initialization  is a shear layer with a small velocity perturbation.
the fluid has a viscosity of `mu=0.006`.
As the solution evolves, the density layer gets distorted.

Load `visu_shear.pvsm` in paraview and then select `SHEAR..vtu` files to visualize.

## Pulse

The domain has slip-walls as (reflecting) boundary conditions, and a pressure pulse is initialized.
The fluid has no viscosity (only euler equations)
As the solution evolves, a pressure wave forms and reflects at the boundaries.

Load `visu_pulse.pvsm` in paraview and then select `PULSE..vtu` files to visualize. 
You can change between a 2D plot and pseudo 3D with pressure as z axis.

## Cavity

*Lid-driven cavity:* The left, right and lower boundary condition are no-slip walls,
the fluid has a viscosity of `mu=0.005`.
The initial state is a fluid at rest, with `rho=1.1,p=0.8`.
The upper boundary has a Dirichlet boundary condition, with 
the same density and pressure, but a velocity in x direction of `0.2`.
The solution evolves to steady state, and a vortex should form in the cavity.

Load `visu_cavity.pvsm` in paraview and then select `CAVITY..vtu` files to visualize.

## Corner

The left and upper boundary is a no-slip wall, 
the fluid has a viscosity of `mu=0.005`.
The initial state is a fluid at rest, with `rho=1.1,p=0.8`.
The lower and right boundaries are Dirichlet boundary conditions, with 
the same density and pressure, but an inflow at the lower and outflox at the right.
The solution evolves to steady state, and the flow should make a right turn.

Load `visu_corner.pvsm` in paraview and then select `CORNER..vtu` files to visualize.

## Channel

The domain is now [-1;1]x[0;10]. lower and upper boundary is a no-slip wall, 
the fluid has a viscosity of `mu=0.002`.
The initial state is a fluid at rest, with `rho=1.1,p=0.8`.
The left and right boundaries are Dirichlet boundary conditions, zero velocity, 
same density but a higher pressure left than right.
The solution evolves to steady state, and a parabolic profile should evolve in the velocity, 
whereas the pressure decreases from left to right.

Load `visu_channel.pvsm` in paraview and then select `CHANNEL..vtu` files to visualize.

 

