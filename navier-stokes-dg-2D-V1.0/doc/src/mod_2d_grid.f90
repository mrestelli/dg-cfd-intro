!>
!!# Module 2D Grid
!!
!! Build the 2D grid data structure and geometry
!!
module mod_2d_grid

 use mod_kinds, only: wp

 use mod_grid, only: t_grid, c_s, c_e
 
 implicit none

 public :: new_2d_grid
 ! The following should be used only for testing
 public :: t_s2d, t_e2d

 private

!-----------------------------------------------------------------------------

!module variables

 !> two-dimensional side
 !!
 type, extends(c_s) :: t_s2d
  real(wp) :: n(2)   !! unit normal \(\V{n}_e\)
  real(wp) :: length !! side length \(|e|\)
 contains
  procedure, pass(s) :: get_n     => get_n_2d
  procedure, pass(s) :: get_sdetj => get_sdetj_2d
 end type t_s2d

 !> rectangular two-dimensional element
 !! \([xc_{1,1},xc_{1,2}]\times
 !!   [xc_{2,1},xc_{2,2}]\)
 !!
 type, extends(c_e) :: t_e2d
  real(wp) :: xc(2,2) !x,y of lower left and upper right element corner
 contains
  procedure, pass(e) :: get_j_it => get_j_it_2d
  procedure, pass(e) :: get_detj => get_detj_2d
  procedure, pass(e) :: map      => map_2d
 end type t_e2d

 !> \(\Delta \tilde{x}_1 = \Delta \tilde{x}_2\) for the reference
 !! element
 real(wp), parameter :: delta_x_tilde = 2.0_wp

 !> Length of the reference side \(\tilde{\tilde{e}}=[-1,1]\)
 real(wp), parameter :: length_e_tilde = 2.0_wp

!-----------------------------------------------------------------------------

contains

 !> Build the grid datastructure for a rectagular 2D domain
 !!
 subroutine new_2d_grid(grid, ne,x_left,x_right,BCtype)
  !---------------------------------------------------------------------------
  !input variables
  integer,      intent(in) :: ne(2)      !! number of elements in x,y
  real(wp),     intent(in) :: x_left(2)  !! lower left corner
  real(wp),     intent(in) :: x_right(2) !! upper right corner 
  integer,      intent(in) :: BCtype(4)  !! lower,upper,left,right BC
  !output variable
  type(t_grid), intent(out) :: grid
  !---------------------------------------------------------------------------
  !local variables
  integer :: i,is, ie1, ie2, ie, iloc
  !---------------------------------------------------------------------------
   grid%ne = ne(1)*ne(2)

   grid%ns = 2*ne(1)*ne(2) ! periodic box
   if(BCtype(1).ne.10)then !lower/upper not periodic
     grid%ns=grid%ns+ne(1)
   end if
   if(BCtype(3).ne.10)then !left/right not periodic
     grid%ns=grid%ns+ne(2)
   end if

   ! allocate and define the side array
   allocate( t_s2d::grid%s( grid%ns ) )
   select type( sides => grid%s ); type is(t_s2d)


   is=0 !side counter
  ! 1) sides with normal parallel to the x1 axis
   do ie2=1,ne(2)
     !left BC
     ie1=1
     ie = ie1 + (ie2-1)*ne(1)
     if(BCtype(3).eq.10)then !left BC is periodic
       is=is+1
       sides(is)%e_l = ie2*ne(1)             ! left neighbor (periodic element)
       sides(is)%e_r = ie                    ! current element
       sides(is)%iloc_l = 2
       sides(is)%iloc_r = 4
       sides(is)%orient_l = +1
       sides(is)%orient_r = -1
     else
       is=is+1
       sides(is)%e_l = ie                    ! current element
       sides(is)%e_r = -3                    ! NEGATIVE: points to BCtype(3)
       sides(is)%iloc_l = 4
       sides(is)%iloc_r = -9999 !not defined
       sides(is)%orient_l = -1
       sides(is)%orient_r = -9999 !not defined
     end if !BCtype(3)=10

     !inner sides (x-minus side of each element)
     do ie1=2,ne(1)
       ie = ie1 + (ie2-1)*ne(1)
       is=is+1
       sides(is)%e_l = (ie1-1) + (ie2-1)*ne(1) ! left neighbor 
       sides(is)%e_r = ie                      ! current element
       sides(is)%iloc_l = 2
       sides(is)%iloc_r = 4
       sides(is)%orient_l = +1
       sides(is)%orient_r = -1
     end do !ie1=2,ne(1)
     
     !right BC
     if(BCtype(4).ne.10)then !left&right not periodic
       ie1=ne(1)
       ie = ie1 + (ie2-1)*ne(1)
       is=is+1
       sides(is)%e_l = ie            ! current element
       sides(is)%e_r = -4            ! NEGATIVE: points to BCtype(4)
       sides(is)%iloc_l = 2
       sides(is)%iloc_r = -9999 !not defined
       sides(is)%orient_l = +1
       sides(is)%orient_r = -9999 !not defined
     end if !bctype(4) /=10
   end do !ie2=1,ne(2)

   ! 2) sides with normal parallel to the x2 axis
   do ie1=1,ne(1)
     !lower BC
     ie2=1
     ie = ie1 + (ie2-1)*ne(1)
     if(BCtype(1).eq.10)then !lower boundary is periodic
       is=is+1
       sides(is)%e_l = ne(1)*(ne(2)-1) + ie1 ! lower element (periodic!)
       sides(is)%e_r = ie                    ! current
       sides(is)%iloc_l = 3
       sides(is)%iloc_r = 1
       sides(is)%orient_l = -1
       sides(is)%orient_r = +1
     else
       is=is+1
       sides(is)%e_l = ie            ! current element
       sides(is)%e_r = -1            ! NEGATIVE: points to BCtype(1)
       sides(is)%iloc_l = 1
       sides(is)%iloc_r = -9999 !not defined
       sides(is)%orient_l = +1  
       sides(is)%orient_r = -9999 !not defined
     end if !BCtype(1)=10

     !inner sides, y-minus element side
     do ie2=2,ne(2)
       ie = ie1 + (ie2-1)*ne(1)
       is=is+1
       sides(is)%e_l = ie1 + (ie2-2)*ne(1)    !lower element
       sides(is)%e_r = ie                     !current element
       sides(is)%iloc_l = 3
       sides(is)%iloc_r = 1
       sides(is)%orient_l = -1
       sides(is)%orient_r = +1
     end do !ie2=2,ne(2)

     !upper BC
     if(BCtype(2).ne.10)then !lower&upper are not periodic
       ie2=ne(2)
       ie = ie1 + (ie2-1)*ne(1)
       is=is+1
       sides(is)%e_l = ie            ! current element
       sides(is)%e_r = -2            ! NEGATIVE: points to BCtype(2)
       sides(is)%iloc_l = 3
       sides(is)%iloc_r = -9999 !not defined
       sides(is)%orient_l = -1  
       sides(is)%orient_r = -9999 !not defined
     end if !BCtype(2) /= 10
   end do !ie1=1,ne(1)

   if(is.ne.grid%ns) stop 'PROBLEM number of sides (grid%ns)'

   ! allocate and define the element array
   allocate( t_e2d::grid%e( grid%ne ) )
   select type( elems => grid%e ); type is(t_e2d)

   do ie=1,grid%ne
     ! each element has four sides
     allocate( elems(ie)%s(4) )
   end do
   ! loop over the sides to fill the element information
   do is=1,grid%ns
     ! left element
     ie   = sides(is)%e_l
     iloc = sides(is)%iloc_l
     elems(ie)%s(iloc) = is
     ! right element
     ie   = sides(is)%e_r
     if( ie .gt. 0 ) then !no boundary condition!
       iloc = sides(is)%iloc_r
       elems(ie)%s(iloc) = is
     end if !ie>0
   end do

   ! geometry, first defined on [0,1]^2
   do ie1=1,ne(1)
     do ie2=1,ne(2)
       ie = ie1 + (ie2-1)*ne(1)
       !lower left corner of element in domain [0,1]^2
       elems(ie)%xc(:,1) = [real(ie1-1,wp)/real(ne(1),wp),real(ie2-1,wp)/real(ne(2),wp)]
       !upper right corner of element in domain [0,1]^2
       elems(ie)%xc(:,2) = [real(ie1,wp)/real(ne(1),wp),real(ie2,wp)/real(ne(2),wp)]
     end do
   end do

   !global mapping: map all element corners 
   !from reference domain  [0,1]^2 --> physical domain [x_left,x_right]^2
   do ie1=1,ne(1)
     do ie2=1,ne(2)
       ie = ie1 + (ie2-1)*ne(1)
       do i=1,2
         elems(ie)%xc(:,i) = x_left(:) + (x_right(:)-x_left(:))*elems(ie)%xc(:,i)
       end do
     end do
   end do

   !fill side geometry, with left element normal and side length
   do is=1,grid%ns
     ! left element
     ie   = sides(is)%e_l
     iloc = sides(is)%iloc_l
     select case (iloc)
     case(1)
       sides(is)%n=[ 0.0_wp,-1.0_wp]
       sides(is)%length = elems(ie)%xc(1,2) - elems(ie)%xc(1,1)
     case(2)
       sides(is)%n=[ 1.0_wp, 0.0_wp]
       sides(is)%length = elems(ie)%xc(2,2) - elems(ie)%xc(2,1)
     case(3)
       sides(is)%n=[ 0.0_wp, 1.0_wp]
       sides(is)%length = elems(ie)%xc(1,2) - elems(ie)%xc(1,1)
     case(4)
       sides(is)%n=[-1.0_wp, 0.0_wp]
       sides(is)%length = elems(ie)%xc(2,2) - elems(ie)%xc(2,1)
     end select !iloc
   end do !is=1,grid%ns

   end select !element type
   end select !side type

 end subroutine new_2d_grid
 
!-----------------------------------------------------------------------------

 !> evaluate the normal vector at side s and position \( x \in [-1;1] \).
 !!
 pure subroutine get_n_2d(n, s,x)
  !---------------------------------------------------------------------------
  !input variables
  class(t_s2d), intent(in)  :: s      !! side
  real(wp),     intent(in)  :: x(:,:) !! evaluation position [-1;1]
  !output variable
  real(wp),     intent(out) :: n(:,:) !! normal vector 
  !---------------------------------------------------------------------------

   ! the normal is the same at all the points (straight sides)
   n(1,:) = s%n(1)
   n(2,:) = s%n(2)

 end subroutine get_n_2d

!-----------------------------------------------------------------------------

 !> evaluate the surface element of side s, \( |e|/|\tilde{e}| \).
 !! since side is linear, only the side length
 !!
 pure subroutine get_sdetj_2d(detj, s,x)
  !---------------------------------------------------------------------------
  !input variables
  class(t_s2d), intent(in)  :: s
  real(wp),     intent(in)  :: x(:,:)
  !output variable
  real(wp),     intent(out) :: detj(:)
  !---------------------------------------------------------------------------
   ! the determinant is the ratio |e|/|\tilde{e}|
   detj = s%length / length_e_tilde

 end subroutine get_sdetj_2d

!-----------------------------------------------------------------------------

 !> inverse Jacobian of the element mapping 
 !!\[
 !!  \mathcal{J}_{ij}=\frac{\partial \tilde{x}_j}{\partial x_i} 
 !!\]
 !! \((\mathcal{J})=({J})^{-1}\), can be computed as the inverse of the Jacobian matrix 
 !!\[
 !!  {J}_{ij}=\frac{\partial {x}_j}{\partial \tilde{x}_i} 
 !!\]
 !! \(i,j=1,2\).
 !!
 pure subroutine get_j_it_2d(j_it, e,x)
  !---------------------------------------------------------------------------
  !input variables
  class(t_e2d), intent(in)  :: e
  real(wp),     intent(in)  :: x(:,:)
  !output variable
  real(wp),     intent(out) :: j_it(:,:,:)
  !---------------------------------------------------------------------------

   j_it = 0.0_wp
   j_it(1,1,:) = delta_x_tilde / (e%xc(1,2)-e%xc(1,1)) !x_right-x_left
   j_it(2,2,:) = delta_x_tilde / (e%xc(2,2)-e%xc(2,1))

 end subroutine get_j_it_2d

!-----------------------------------------------------------------------------

 !> determinant of the Jacobian of the element mapping \( \det(J) \)
 !!\[
 !!  \det(J)=\frac{\partial x_1}{\partial \tilde{x}_1}
 !!          \frac{\partial x_2}{\partial \tilde{x}_2}
 !!         -\frac{\partial x_2}{\partial \tilde{x}_1}
 !!          \frac{\partial x_1}{\partial \tilde{x}_2}
 !!\]
 !!
 pure subroutine get_detj_2d(detj, e,x)
  !---------------------------------------------------------------------------
  !input variables
  class(t_e2d), intent(in)  :: e
  real(wp),     intent(in)  :: x(:,:)
  !output variable
  real(wp),     intent(out) :: detj(:)
  !---------------------------------------------------------------------------

   detj = (e%xc(1,2)-e%xc(1,1))*(e%xc(2,2)-e%xc(2,1)) &
          / delta_x_tilde**2

 end subroutine get_detj_2d

!-----------------------------------------------------------------------------

 !> evaluate element mapping between physical and reference space
 !! \( \vec{x}(\tilde{x}_1,\tilde{x}_2) \) 
 !! reference space is \(\tilde{x}_1,\tilde{x}_2 \in [-1;1]^2 \)
 !!
 pure subroutine map_2d(x, e,x_tilde)
  !---------------------------------------------------------------------------
  !input variables
  class(t_e2d), intent(in)  :: e
  real(wp),     intent(in)  :: x_tilde(:,:)
  !output variable
  real(wp),     intent(out) :: x(:,:)
  !---------------------------------------------------------------------------

   x(1,:) = 0.5_wp*(e%xc(1,1)+e%xc(1,2)) + (e%xc(1,2)-e%xc(1,1))/delta_x_tilde * x_tilde(1,:)
   x(2,:) = 0.5_wp*(e%xc(2,1)+e%xc(2,2)) + (e%xc(2,2)-e%xc(2,1))/delta_x_tilde * x_tilde(2,:)
   
 end subroutine map_2d

!-----------------------------------------------------------------------

end module mod_2d_grid
