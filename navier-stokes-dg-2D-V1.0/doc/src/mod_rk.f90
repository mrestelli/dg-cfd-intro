!>
!!# Module Runge Kutta
!!
!! Collection of RK solvers
!!
module mod_rk

 use mod_kinds, only: wp
 
 use mod_ode_state, only: c_ode_state

 use mod_ode_solver, only: c_ode, c_ode_integrator

 implicit none

 public :: t_explicit_euler, t_heun_method

 private

!-----------------------------------------------------------------------------

!module variables

 type, extends(c_ode_integrator) :: t_explicit_euler
 contains
  procedure, pass(ode_int) :: compute_step => explicit_euler_step
 end type t_explicit_euler

 type, extends(c_ode_integrator) :: t_heun_method
  class(c_ode_state), allocatable :: ytmp
 contains
  procedure, pass(ode_int) :: setup        => heun_method_setup
  procedure, pass(ode_int) :: clean        => heun_method_clean
  procedure, pass(ode_int) :: compute_step => heun_method_step
 end type t_heun_method

!-----------------------------------------------------------------------------

contains

 !> The Euler step is  
 !! ynew = ynow + dt*ode%rhs( ynow )
 !!
 subroutine explicit_euler_step(ynew , ode_int,ode,dt,ynow)
  !---------------------------------------------------------------------------
  !input/output variables
  class(t_explicit_euler), intent(inout) :: ode_int
  class(c_ode),            intent(in)    :: ode
  real(wp),                intent(in)    :: dt
  class(c_ode_state),      intent(in)    :: ynow
  class(c_ode_state),      intent(inout) :: ynew
  !---------------------------------------------------------------------------

   call ode%compute_rhs( ynew , ynow ) ! ynew = rhs(ynow)
   call ynew%scalar_multiply( dt )     ! ynew = dt*rhs(ynow)
   call ynew%increment( ynow )         ! ynew = ynow + dt*rhs(ynow)

 end subroutine explicit_euler_step

!-----------------------------------------------------------------------------

 !> initialize heun method
 !!
 subroutine heun_method_setup(ode_int,yinit)
  !---------------------------------------------------------------------------
  class(t_heun_method), intent(inout) :: ode_int
  class(c_ode_state),   intent(in)    :: yinit
  !---------------------------------------------------------------------------

   call yinit%clone( ode_int%ytmp )
 end subroutine heun_method_setup

!-----------------------------------------------------------------------------

 !> clean heun method
 !!
 subroutine heun_method_clean(ode_int)
  !---------------------------------------------------------------------------
  class(t_heun_method), intent(inout) :: ode_int
  !---------------------------------------------------------------------------

   deallocate( ode_int%ytmp )
 end subroutine heun_method_clean

!-----------------------------------------------------------------------------

 !> The Heun method step is  
 !! ytmp = ynow + 1/2*dt*ode%rhs( ynow )
 !! ynew = ynow + dt*ode%rhs( ytmp )
 !!
 subroutine heun_method_step(ynew , ode_int,ode,dt,ynow)
  !---------------------------------------------------------------------------
  class(t_heun_method), intent(inout) :: ode_int
  class(c_ode),         intent(in)    :: ode
  real(wp),             intent(in)    :: dt
  class(c_ode_state),   intent(in)    :: ynow
  class(c_ode_state),   intent(inout) :: ynew
  !---------------------------------------------------------------------------

   associate( ytmp => ode_int%ytmp )

   call ode%compute_rhs( ytmp , ynow )
   call ytmp%scalar_multiply( 0.5_wp*dt )
   call ytmp%increment( ynow )

   call ode%compute_rhs( ynew , ytmp )
   call ynew%scalar_multiply( dt )
   call ynew%increment( ynow )

   end associate

 end subroutine heun_method_step

!-----------------------------------------------------------------------------

end module mod_rk

