!>
!!# Module Navier-Stokes ODE
!!
!! provides the computation of the RHS for 2D DG navier-Stokes solver, including 
!! the computations for the gradients (auxiliary variables)
!! calls routines from `mod_ns_local_int.f90` 
!!
module mod_ns_ode

 use mod_kinds,        only: wp
 use mod_ode_state,    only: c_ode_state
 use mod_ode_solver,   only: c_ode
 use mod_linalg,       only: invmat_chol
 use mod_grid,         only: t_grid
 use mod_base,         only: t_base
 use mod_ns_state,     only: t_ns_state
 use mod_ns_local_int, only: element_mass_matrix, &
                             element_integral, g_element_integral, &
                             side_integral   , g_side_integral   , &
                             side_integral_bc, g_side_integral_bc

 implicit none

 public :: t_ns_ode, ns_ode_setup, ns_ode_clean

 private

!-----------------------------------------------------------------------------

 !> Extend the ode type for 2D Navier-stokes 
 !! 
 type, extends(c_ode) :: t_ns_ode
  integer      :: d     !! space dimension
  type(t_grid) :: grid
  type(t_base) :: base
  !> \(\left(M^K\right)^{-1}\) for all the elements
  real(wp), allocatable :: imass_matrices(:,:,:)
 contains
  procedure, pass(ode) :: compute_rhs => compute_ns_rhs
  !> generate a state vector for this ODE
  procedure, pass(ode) :: new_ns_state
 end type t_ns_ode

contains

!-----------------------------------------------------------------------------

 !> Setup the ODE system for Navier-Stokes, initialization phase
 !!
 subroutine ns_ode_setup(ode, d,base,grid)
  !---------------------------------------------------------------------------
  !input variables
  integer,        intent(in)  :: d     !! space dimension
  type(t_base),   intent(in)  :: base
  type(t_grid),   intent(in)  :: grid
  !output variable
  type(t_ns_ode), intent(out) :: ode
  !---------------------------------------------------------------------------
  !local variables
  integer :: ie
  real(wp), allocatable :: m(:,:) ! work array for mass matrix
  !---------------------------------------------------------------------------

   ode%d     = d
   ode%base = base

   ode%grid = grid

   ! precompute the mass matrices
   allocate(m(ode%base%n,ode%base%n))
   allocate( ode%imass_matrices(ode%base%n,ode%base%n,ode%grid%ne) )
   do ie=1,ode%grid%ne
     ! mass matrix
     call element_mass_matrix( m, ode%base, ode%grid%e(ie) )
     ! invert the mass matrix
     call invmat_chol( m , ode%imass_matrices(:,:,ie) )
   enddo
   deallocate(m)

 end subroutine ns_ode_setup

!-----------------------------------------------------------------------------

 !> Clean the ODE for Navier-Stokes
 !!
 subroutine ns_ode_clean(ode)
  !---------------------------------------------------------------------------
  type(t_ns_ode), intent(inout) :: ode
  !---------------------------------------------------------------------------

   deallocate( ode%imass_matrices )
   call ode%base%clean()
   call ode%grid%clean()

 end subroutine ns_ode_clean

!-----------------------------------------------------------------------------

 !> Get a new state
 !!
 subroutine new_ns_state(y, ode)
  !---------------------------------------------------------------------------
  class(t_ns_ode),   intent(in)  :: ode
  class(t_ns_state), intent(out) :: y
  !---------------------------------------------------------------------------

   allocate( y%uu(ode%d+2,ode%base%n,ode%grid%ne) )

 end subroutine new_ns_state

!-----------------------------------------------------------------------------

 !> Call routines for the computation of the RHS for the ODE for Navier-Stokes
 !!
 subroutine compute_ns_rhs(rhs,ode,y)
  !---------------------------------------------------------------------------
  !input variables
  class(t_ns_ode),    intent(in) :: ode
  class(c_ode_state), intent(in)    :: y
  !output variable
  class(c_ode_state), intent(inout) :: rhs
  !---------------------------------------------------------------------------
  !local variables
  real(wp) :: g( ode%d , ode%d+1 , ode%base%n , ode%grid%ne ) !!  g = grad(a)
  !---------------------------------------------------------------------------


   select type(y);   type is(t_ns_state)
   select type(rhs); type is(t_ns_state)

   call compute_auxiliary_variable(g,ode,y)

   call compute_rhs(rhs,ode,y,g)

   end select
   end select

 end subroutine compute_ns_rhs

!-----------------------------------------------------------------------------

 !> Compute the gradient of the solution, using the same DG scheme as the 
 !! main equations
 !!
 subroutine compute_auxiliary_variable(g,ode,y)
  !---------------------------------------------------------------------------
  !input variables
  class(t_ns_ode),   intent(in) :: ode
  class(t_ns_state), intent(in) :: y
  !! \(\V{G}_h\): \(\code{g(:,:,i,ie)} = \T{G}_i^{K_{\code{ie}}}\)
  !output variable
  real(wp), intent(out) :: g(:,:,:,:)
  !---------------------------------------------------------------------------
  !local variables
  integer :: ie, is, s, t
  integer :: e_l, e_r
  !---------------------------------------------------------------------------

   ! initialize g
   g = 0.0_wp

   ! element integrals
   do ie=1,ode%grid%ne
     associate( uuk => y%uu(:,:,ie) )
     call g_element_integral( g(:,:,:,ie), ode%d, &
                              ode%base,ode%grid%e(ie), uuk )
     end associate
   enddo

   ! side fluxes
   do is=1,ode%grid%ns
     e_l = ode%grid%s(is)%e_l
     e_r = ode%grid%s(is)%e_r
     if( e_r .gt. 0 ) then ! internal side
       associate( uuk_l=>y%uu(:,:,e_l) , uuk_r=>y%uu(:,:,e_r) )
       call g_side_integral( g(:,:,:,e_l),g(:,:,:,e_r), ode%d, &
                             ode%base, ode%grid%s(is) , uuk_l,uuk_r )
       end associate !uuk_l, uuk_r
     else ! boundary side
       associate( uuk_l=>y%uu(:,:,e_l) )
       call g_side_integral_bc( g(:,:,:,e_l), ode%d, &
                                ode%base, ode%grid%s(is) , uuk_l )
       end associate !uuk_l
     endif
   enddo

   ! include the mass matrix
   do ie=1,ode%grid%ne
     do s=1,size(g,1)
       do t=1,size(g,2)
         g(s,t,:,ie) = matmul(ode%imass_matrices(:,:,ie) , g(s,t,:,ie))
       enddo
     enddo
   enddo

 end subroutine compute_auxiliary_variable

!-----------------------------------------------------------------------------

 !> Compute the rhs of the DG scheme for the Navier-Stokes equations 
 !!
 subroutine compute_rhs(rhs,ode,y,g)
  !---------------------------------------------------------------------------
  !input variables
  class(t_ns_ode),   intent(in)    :: ode
  class(t_ns_state), intent(in)    :: y
  real(wp),          intent(in)    :: g(:,:,:,:)
  !output variable
  class(t_ns_state), intent(inout) :: rhs
  !---------------------------------------------------------------------------
  !local variables
  integer :: ie, is, e_l,e_r, k
  !---------------------------------------------------------------------------

   ! initialize rhs
   rhs%uu = 0.0_wp

   do ie=1,ode%grid%ne
     associate( uuk => y%uu(:,:,ie) )
     call element_integral(rhs%uu(:,:,ie), ode%d, &
                           ode%base,ode%grid%e(ie), uuk,g(:,:,:,ie))
     end associate !uuk
   enddo

   ! side fluxes
   do is=1,ode%grid%ns
     e_l = ode%grid%s(is)%e_l
     e_r = ode%grid%s(is)%e_r
     if( e_r .gt. 0 ) then ! internal side
       call side_integral( rhs%uu(:,:,e_l) , rhs%uu(:,:,e_r) , ode%d , & 
                           ode%base,ode%grid%s(is),                    &
                           y%uu(:,:,e_l), y%uu(:,:,e_r) , g(:,:,:,e_l),g(:,:,:,e_r))
     else ! boundary side
       call side_integral_bc( rhs%uu(:,:,e_l) , ode%d , ode%base,ode%grid%s(is),   &
                              y%uu(:,:,e_l), g(:,:,:,e_l))
     endif
   enddo

   ! include the mass matrix
   do ie=1,ode%grid%ne
     do k=1,size(rhs%uu,1) ! loop over the variables
       rhs%uu(k,:,ie) = &
         matmul(ode%imass_matrices(:,:,ie),rhs%uu(k,:,ie))
     enddo
   enddo

 end subroutine compute_rhs

!-----------------------------------------------------------------------------

end module mod_ns_ode
