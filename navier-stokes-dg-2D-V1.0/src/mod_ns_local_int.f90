!>
!!# Module Navier-Stokes local integrals
!!
!! provides the routines for the Navier-Stokes ODE module and the gradient evaluation ( marked by `g_`):  
!!
!!- mass matrix
!!- element integral
!!- inner/boundary side integrals
!!
!! uses functions from `ns_eqn` module to compute the fluxes
!!
module mod_ns_local_int

 use mod_kinds,  only: wp
 use mod_grid,   only: t_grid, c_s, c_e
 use mod_base,   only: t_base
 use mod_ns_eqn, only: bc_flux, g_bc_flux, euler_flux,viscous_flux, &
                       euler_num_flux,viscous_num_flux,cons_to_psi

 implicit none

 public :: element_mass_matrix, &
   element_integral, side_integral, side_integral_bc, &
   g_element_integral, g_side_integral, g_side_integral_bc

 private

contains

!-----------------------------------------------------------------------

 !> Compute the mass matrix for element \(K\)
 !! \[
 !!  M^K_{ij} = \int_K \varphi_i^K \varphi_j^K
 !! \]
 !! for every element \(K\in\mathcal{T}_h\).  
 !! Since \(K\) is a rectagular element, the integral is computed
 !! as
 !! \[
 !! \int_K \varphi_i^K \varphi_j^K =
 !! \frac{|K|}{|\tilde{K}|}\int_{\tilde{K}} \tilde{\varphi}_i
 !! \tilde{\varphi}_j 
 !! \approx 
 !! \frac{|K|}{|\tilde{K}|} \sum_{l=1}^m \tilde{w}^Q_l\, 
 !! \tilde{\varphi}_i (\tilde{x}_1,^Q_l,\tilde{x}_2,^Q_l)\,
 !! \tilde{\varphi}_j (\tilde{x}_1,^Q_l,\tilde{x}_2,^Q_l).
 !! \]
 !!
 pure subroutine element_mass_matrix(m, base,e)
  !---------------------------------------------------------------------------
  !input variables
  type(t_base), intent(in)  :: base  !! base and quadrature formula
  class(c_e),   intent(in)  :: e     !! element \(K\)
  !output variables
  real(wp),     intent(out) :: m(:,:)
  !---------------------------------------------------------------------------
  !local variables
  integer  :: i, j, l
  real(wp) :: detj(base%m)
  !---------------------------------------------------------------------------

   ! retrieve det(J) at all the quadrature points
   call e%get_detj( detj , base%xq )

   ! evaluate the integral
   m = 0.0_wp
   do l=1,base%m
     do j=1,base%n
       do i=1,base%n
         m(i,j) = m(i,j) &
                 + base%wq(l)*detj(l) * base%phi(i,l)*base%phi(j,l)
       enddo
     enddo
   enddo

 end subroutine element_mass_matrix

!-----------------------------------------------------------------------------

 !> Compute the element integral
 !! \[
 !!  \int_K \V{F}|_K \cdot\Grad\varphi_i^K,
 !! \]
 !! for every basis function \(\varphi_i^K\in V_h(K)\) and for a given
 !! element \(K\in\mathcal{T}_h\).  
 !! The integral is computed as
 !! \[
 !! \int_K \V{F} \cdot \Grad \varphi_i^K =
 !! \int_{\tilde{K}} \V{F}(U,\nabla U)\cdot \,
 !! \left(\underline{\underline{\mathcal{J}}}\; \Grad_{\tilde{x}} \tilde{\varphi}_i \right) \det(J)
 !! \]
 !! where \(\mathcal{J}\) is the inverse jacobian of the element mapping,
 !! and \(\det(J)\) the determinant of the Jacobian.  
 !! We approximate the integral by numerical quadrature.  
 !! Each element integral is *added* to ```e_rhs(:,:)```.
 !!
 pure subroutine element_integral(e_rhs, d, base,e, uuk,ggk)
  !---------------------------------------------------------------------------
  !input/output variables
  integer,      intent(in)    :: d     !! space dimension
  type(t_base), intent(in)    :: base  !! base and quadrature formula
  class(c_e),   intent(in)    :: e     !! element
  real(wp),     intent(in)    :: uuk(:,:)
  real(wp),     intent(in)    :: ggk(:,:,:)
  real(wp),     intent(inout) :: e_rhs(:,:)
  !---------------------------------------------------------------------------
  !local variables
  integer   :: i, l
  real(wp)  :: uuh_k(size(uuk,1),base%m), ggh_k(d,d+1,base%m)
  real(wp)  :: flux(d,size(uuk,1)), gradphi(d)
  real(wp)  :: j_it(d,d,base%m), detj(base%m)
  !---------------------------------------------------------------------------

   ! retrieve J^{-T} and det(J) at all the quadrature points
   call e%get_j_it( j_it , base%xq )
   call e%get_detj( detj , base%xq )

   ! compute the local solution at all the quadrature points
   uuh_k = 0.0_wp
   ggh_k = 0.0_wp
   do l=1,base%m
     do i=1,base%n
       uuh_k(:,l)   = uuh_k(:,l)   + uuk(:,i)*base%phi(i,l)
       ggh_k(:,:,l) = ggh_k(:,:,l) + ggk(:,:,i)*base%phi(i,l)
     enddo
   enddo

   ! evaluate the integral
   do l=1,base%m
     flux = euler_flux(uuh_k(:,l))                     &
        + viscous_flux(uuh_k(:,l),ggh_k(:,:,l))
     do i=1,base%n
       gradphi  = matmul( j_it(:,:,l) , base%dphi(:,i,l) )
       e_rhs(:,i) = e_rhs(:,i) &
         + base%wq(l)*detj(l) * matmul( gradphi , flux )
     enddo
   enddo

 end subroutine element_integral

!-----------------------------------------------------------------------------

 !> the element integral for the gradient equation is simply
 !! \[
 !!  \int_K U \cdot\Grad\varphi_i^K,
 !! \]
 !! Derivation are analogous to routine `element_integral`.
 !!
 pure subroutine g_element_integral(e_rhs, d, base,e,uuk)
  !---------------------------------------------------------------------------
  !input/output variables
  integer,      intent(in)    :: d
  type(t_base), intent(in)    :: base
  class(c_e),   intent(in)    :: e
  real(wp),     intent(in)    :: uuk(:,:)
  real(wp),     intent(inout) :: e_rhs(:,:,:)
  !---------------------------------------------------------------------------
  !local variables
  integer   :: i, l, j
  real(wp)  :: uuh_k(size(uuk,1),base%m), psi(d+1,base%m)
  real(wp)  :: j_it(d,d,base%m), detj(base%m), gradphi(d)
  !---------------------------------------------------------------------------

   ! retrieve J^{-T} and det(J) at all the quadrature points
   call e%get_j_it( j_it , base%xq )
   call e%get_detj( detj , base%xq )

   ! compute the local solution at all the quadrature points
   uuh_k = 0.0_wp
   do l=1,base%m
     do i=1,base%n
       uuh_k(:,l) = uuh_k(:,l) + uuk(:,i)*base%phi(i,l)
     enddo
   enddo

   ! compute velocity and temperature
   do l=1,base%m
     psi(:,l)=cons_to_psi(uuh_k(:,l))
   enddo

   do l=1,base%m
     do i=1,base%n
       gradphi  = matmul( j_it(:,:,l) , base%dphi(:,i,l) )
       do j=1,size(e_rhs,2)
         e_rhs(:,j,i) = e_rhs(:,j,i) &
           - base%wq(l)*detj(l) * psi(j,l) * gradphi
       enddo
     enddo
   enddo

 end subroutine g_element_integral

!-----------------------------------------------------------------------------

 !> Compute the side integrals
 !! \[
 !!  -\int_{e} \widehat{F}_{n,e} \sigma_{K,e} \varphi_i^K,
 !! \]
 !! for every basis function \(\varphi_i^K\in V_h(K)\) and
 !! element sides \(e=e_L(K),e_R(K)\)  
 !! Note that the integral is approximated with a quadrature along the side
 !! Recall that the normal points from left to right side, such that
 !! \[
 !!  \sigma_{K_L(e),e} = 1, \qquad
 !!  \sigma_{K_R(e),e} = -1.
 !! \]
 !! The side integrals are *added* to ```s_rhs_L``` and ```s_rhs_L```.
 !!
 pure subroutine side_integral(s_rhs_l,s_rhs_r, d, base,s, &
                               uuk_l,uuk_r, ggk_l,ggk_r)
  !---------------------------------------------------------------------------
  !input variables
  integer,      intent(in)    :: d
  type(t_base), intent(in)    :: base
  class(c_s),   intent(in)    :: s
  real(wp),     intent(in)    :: uuk_l(:,:)
  real(wp),     intent(in)    :: uuk_r(:,:)
  real(wp),     intent(in)    :: ggk_l(:,:,:)
  real(wp),     intent(in)    :: ggk_r(:,:,:)
  !output variables
  real(wp),     intent(inout) :: s_rhs_l(:,:)
  real(wp),     intent(inout) :: s_rhs_r(:,:)
  !---------------------------------------------------------------------------
  !local variables
  integer    :: i, q, q_l, q_r
  integer    :: q_lr(base%ms,-1:+1)
  real(wp)   :: uuh_kl(2+d,base%ms), uuh_kr(2+d,base%ms)
  real(wp)   :: ggh_kl(d,d+1,base%ms), ggh_kr(d,d+1,base%ms)
  real(wp)   :: numflux(2+d)
  real(wp)   :: n(d,base%ms), detj(base%ms)
  !---------------------------------------------------------------------------

   ! fill the quadrature node indexing array
   q_lr(:,-1) = [(base%ms-q+1, q=1,base%ms)]
   q_lr(:,+1) = [(        q,   q=1,base%ms)]

   ! retrieve n and det(J) at all the quadrature points
   call s%get_n(        n , base%xqs )
   call s%get_sdetj( detj , base%xqs )

   ! compute the local solution at all the quadrature points
   uuh_kl = 0.0_wp
   uuh_kr = 0.0_wp
   ggh_kl = 0.0_wp
   ggh_kr = 0.0_wp
   do q=1,base%ms
     q_l = q_lr(q,s%orient_l)
     q_r = q_lr(q,s%orient_r)
     do i=1,base%n
       uuh_kl(:,q) = uuh_kl(:,q) + uuk_l(:,i)*base%phib(i,q_l,s%iloc_l)
       uuh_kr(:,q) = uuh_kr(:,q) + uuk_r(:,i)*base%phib(i,q_r,s%iloc_r)
       ggh_kl(:,:,q) = ggh_kl(:,:,q) &
                      + ggk_l(:,:,i)*base%phib(i,q_l,s%iloc_l)
       ggh_kr(:,:,q) = ggh_kr(:,:,q) &
                      + ggk_r(:,:,i)*base%phib(i,q_r,s%iloc_r)
     enddo
   enddo

   ! evaluate the integral
   do q=1,base%ms
     q_l = q_lr(q,s%orient_l)
     q_r = q_lr(q,s%orient_r)
     numflux = euler_num_flux(n(:,q),uuh_kl(:,q),uuh_kr(:,q)) +       &
          viscous_num_flux(n(:,q), uuh_kl(:,q),uuh_kr(:,q), &
                           ggh_kl(:,:,q),ggh_kr(:,:,q))
     do i=1,base%n
       s_rhs_l(:,i) = s_rhs_l(:,i) &
         - base%wqs(q)*detj(q) * numflux*base%phib(i,q_l,s%iloc_l)
       s_rhs_r(:,i) = s_rhs_r(:,i) &
         + base%wqs(q)*detj(q) * numflux*base%phib(i,q_r,s%iloc_r)
     enddo
   enddo

 end subroutine side_integral

!-----------------------------------------------------------------------------

 !> Side integral for boundary sides. Analogous to side integral, but only for 
 !! the left element. calls the boundary flux routine
 !!
 subroutine side_integral_bc(s_rhs_l, d, base,s, uuk_l, ggk_l)
  !---------------------------------------------------------------------------
  !input variables
  integer,      intent(in)    :: d
  type(t_base), intent(in)    :: base
  class(c_s),   intent(in)    :: s
  real(wp),     intent(in)    :: uuk_l(:,:)
  real(wp),     intent(in)    :: ggk_l(:,:,:)
  !output variable
  real(wp),     intent(inout) :: s_rhs_l(:,:)
  !---------------------------------------------------------------------------
  !local variables
  integer     :: i,q, q_l
  integer     :: q_lr(base%ms,-1:+1)
  real(wp)    :: uuh_kl(2+d,base%ms)
  real(wp)    :: ggh_kl(d,d+1,base%ms)
  real(wp)    :: numflux(2+d,base%ms)
  real(wp)    :: n(d,base%ms), detj(base%ms)
  !---------------------------------------------------------------------------

   ! fill the quadrature node indexing array
   q_lr(:,-1) = [(base%ms-q+1, q=1,base%ms)]
   q_lr(:,+1) = [(        q,   q=1,base%ms)]

   ! retrieve n and det(J) at all the quadrature points
   call s%get_n(        n , base%xqs )
   call s%get_sdetj( detj , base%xqs )

   ! compute the local solution at all the quadrature points
   uuh_kl = 0.0_wp
   ggh_kl = 0.0_wp
   do q=1,base%ms
     q_l = q_lr(q,s%orient_l)
     do i=1,base%n
       uuh_kl(:,q)   = uuh_kl(:,q)   + uuk_l(:,i)  *base%phib(i,q_l,s%iloc_l)
       ggh_kl(:,:,q) = ggh_kl(:,:,q) + ggk_l(:,:,i)*base%phib(i,q_l,s%iloc_l)
     enddo
   enddo

   ! evaluate the flux, -s%e_r is the index of boundary condition 1...4
   numflux(:,:)=bc_flux(d,base%ms,-s%e_r, &
                          n(:,:), uuh_kl(:,:), ggh_kl(:,:,:))

   ! evaluate the integral
   do q=1,base%ms
     q_l = q_lr(q,s%orient_l)
     do i=1,base%n
       s_rhs_l(:,i) = s_rhs_l(:,i) &
                      - base%wqs(q)*detj(q) * numflux(:,q)*base%phib(i,q_l,s%iloc_l)
     enddo
   enddo

 end subroutine side_integral_bc

!-----------------------------------------------------------------------------

 !> Gradient equation side integral, analogous to `side_integral` routine
 !!
 pure subroutine g_side_integral(s_rhs_l,s_rhs_r, d, base,s,uuk_l,uuk_r)
  !---------------------------------------------------------------------------
  !input variables
  integer,      intent(in)    :: d
  type(t_base), intent(in)    :: base
  class(c_s),   intent(in)    :: s
  real(wp),     intent(in)    :: uuk_l(:,:)
  real(wp),     intent(in)    :: uuk_r(:,:)
  !output variables
  real(wp),     intent(inout) :: s_rhs_l(:,:,:)
  real(wp),     intent(inout) :: s_rhs_r(:,:,:)
  !---------------------------------------------------------------------------
  !local variables
  integer      :: i, j, q, q_l, q_r
  integer      :: q_lr(base%ms,-1:+1)
  real(wp)     :: uuh_kl(size(uuk_l,1),base%ms), uuh_kr(size(uuk_l,1),base%ms)
  real(wp)     :: psi_l(d+1), psi_r(d+1)
  real(wp)     :: numflux(d+1,base%ms)
  real(wp)     :: n(d,base%ms), detj(base%ms)
  !---------------------------------------------------------------------------

   ! fill the quadrature node indexing array
   q_lr(:,-1) = [(base%ms-q+1, q=1,base%ms)]
   q_lr(:,+1) = [(        q,   q=1,base%ms)]

   ! retrieve n and det(J) at all the quadrature points
   call s%get_n(        n , base%xqs )
   call s%get_sdetj( detj , base%xqs )

   ! compute the local solution at all the quadrature points
   uuh_kl = 0.0_wp
   uuh_kr = 0.0_wp
   do q=1,base%ms
     q_l = q_lr(q,s%orient_l)
     q_r = q_lr(q,s%orient_r)
     do i=1,base%n
       uuh_kl(:,q) = uuh_kl(:,q) + uuk_l(:,i)*base%phib(i,q_l,s%iloc_l)
       uuh_kr(:,q) = uuh_kr(:,q) + uuk_r(:,i)*base%phib(i,q_r,s%iloc_r)
     enddo
   enddo

   ! compute velocity and temperature
   do q=1,base%ms
     psi_l=cons_to_psi(uuh_kl(:,q))
     psi_r=cons_to_psi(uuh_kr(:,q))
     numflux(:,q) = 0.5_wp*(psi_l + psi_r) ! centered numerical flux
   enddo


   ! evaluate the integral
   do q=1,base%ms
     q_l = q_lr(q,s%orient_l)
     q_r = q_lr(q,s%orient_r)
     do i=1,base%n
       do j=1,size(s_rhs_l,2)
         s_rhs_l(:,j,i) = s_rhs_l(:,j,i) + base%wqs(q)*detj(q) &
                * numflux(j,q)*n(:,q)*base%phib(i,q_l,s%iloc_l)
         s_rhs_r(:,j,i) = s_rhs_r(:,j,i) - base%wqs(q)*detj(q) &
                * numflux(j,q)*n(:,q)*base%phib(i,q_r,s%iloc_r)
       enddo
     enddo
   enddo

 end subroutine g_side_integral

!-----------------------------------------------------------------------------

 !> boundary side integral for gradient equation, 
 !! analogous to boundary si`de integral
 !!
 subroutine g_side_integral_bc(s_rhs_l,d, base,s,uuk_l)
  !---------------------------------------------------------------------------
  !input variables
  integer,      intent(in)    :: d
  type(t_base), intent(in)    :: base
  class(c_s),   intent(in)    :: s
  real(wp),     intent(in)    :: uuk_l(:,:)
  !output variables
  real(wp),     intent(inout) :: s_rhs_l(:,:,:)
  !---------------------------------------------------------------------------
  !local variables
  integer  :: i, j, q, q_l
  integer  :: q_lr(base%ms,-1:+1)
  real(wp) :: uuh_kl(size(uuk_l,1),base%ms)
  real(wp) :: numflux(d+1,base%ms)
  real(wp) :: n(d,base%ms), detj(base%ms)
  !---------------------------------------------------------------------------

   ! fill the quadrature node indexing array
   q_lr(:,-1) = [(base%ms-q+1, q=1,base%ms)]
   q_lr(:,+1) = [(        q,   q=1,base%ms)]

   ! retrieve n and det(J) at all the quadrature points
   call s%get_n(        n , base%xqs )
   call s%get_sdetj( detj , base%xqs )

   ! compute the local solution at all the quadrature points
   uuh_kl = 0.0_wp
   do q=1,base%ms
     q_l = q_lr(q,s%orient_l)
     do i=1,base%n
       uuh_kl(:,q) = uuh_kl(:,q) + uuk_l(:,i)*base%phib(i,q_l,s%iloc_l)
     end do
   end do

   ! evaluate the flux, -s%e_r index of boundaries 1...4
   numflux(:,:)=g_bc_flux(d,base%ms,-s%e_r,n(:,:),uuh_kl(:,:))

   ! evaluate the integral
   do q=1,base%ms
     q_l = q_lr(q,s%orient_l)
     do i=1,base%n
       do j=1,size(s_rhs_l,2)
         s_rhs_l(:,j,i) = s_rhs_l(:,j,i) + base%wqs(q)*detj(q) &
                * numflux(j,q)*n(:,q)*base%phib(i,q_l,s%iloc_l)
       end do
     end do
   end do

 end subroutine g_side_integral_bc

!-----------------------------------------------------------------------------

end module mod_ns_local_int

