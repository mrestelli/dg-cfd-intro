!>
!!# Module Base
!!
!! Provides the polynomial basis functions and derivatives
!! as well as integration points/weights for 
!! for the 1D and 2D reference element (quads)
!!
module mod_base

 use mod_kinds, only: wp

 implicit none

 public :: t_base, new_1d_base, new_2d_base
 public :: eval_2d_phi

 private

!-----------------------------------------------------------------------------

!module variables

 !> Local base on \(\tilde{K}\)  
 !! Notice that there is no need to introduce abstract classes here:
 !! allocatable arrays are enough to include all the cases.
 !!
 type :: t_base

  !> number of base functions
  integer :: n
  !> number of quadrature nodes inside \(\tilde{K}\)
  integer :: m
  !> number of quadrature nodes on each side
  !! \(\tilde{e}_k\in\partial \tilde{K}\)
  integer :: ms

  !> \(\code{xq(:,l)} = \tilde{\V{x}}^Q_l\)
  real(wp), allocatable :: xq(:,:)
  !> \(\code{wq(l)} = \tilde{w}^Q_l\)
  real(wp), allocatable :: wq(:)

  !> \(\code{xqs(:,l^s)} = \tilde{\V{x}}^{Q^s}_{l^s}\)
  real(wp), allocatable :: xqs(:,:)
  !> \(\code{wqs(l^s)} = \tilde{w}^{Q^s}_{l^s}\)
  real(wp), allocatable :: wqs(:)

  !> \(\code{phi(i,l)} = \tilde{\varphi}_i(\tilde{\V{x}}^Q_l)\)
  real(wp), allocatable :: phi(:,:)
  !> \(\code{dphi(j,i,l)} = 
  !! \partial_{\tilde{x}_j}\tilde{\varphi}_i(\tilde{\V{x}}^Q_l)\)
  real(wp), allocatable :: dphi(:,:,:)
  !> for \(\tilde{e}_k\in\partial \tilde{K}\),
  !! \(\code{phib(i,l^s,k)} = \left.
  !! \tilde{\varphi}_i(\tilde{\V{x}}^{Q^s}_{l^s})\right|_{\tilde{e}_k}\)
  real(wp), allocatable :: phib(:,:,:)

  !> \(\code{x_{dofs}(:,i)} = \tilde{\V{x}}_i^{\rm dofs}\),
  !! interpolation points for the Lagrangian finite element basis
  real(wp), allocatable :: x_dofs(:,:)

 contains
  procedure, pass(base) :: clean => clean_base
 end type t_base

!-----------------------------------------------------------------------------

contains

 !> One-dimensional base on \(\tilde{K}=[-1,1]\)  
 !! The boundary sides are ordered as follows:
 !! \[
 !!  \begin{array}{l}
 !!   \tilde{e}_1 = -1 \\
 !!   \tilde{e}_2 = +1
 !!  \end{array}
 !! \]
 !!
 subroutine new_1d_base(base,degree)
  !---------------------------------------------------------------------------
  !input/output variables
  integer,      intent(in)  :: degree !! polynomial degree
  type(t_base), intent(out) :: base
  !---------------------------------------------------------------------------

   select case(degree)

    case(0)
     base%n  = 1
     base%m  = 1
     base%ms = 1
     allocate(base%xq(1,base%m  )); base%xq  = 0.0_wp
     allocate(base%wq(  base%m  )); base%wq  = 2.0_wp
     allocate(base%xqs(1,base%ms)); base%xqs = 0.0_wp
     allocate(base%wqs(  base%ms)); base%wqs = 1.0_wp
     allocate(base%phi(   base%n,base%m));  base%phi  = 1.0_wp
     allocate(base%dphi(1,base%n,base%m));  base%dphi = 0.0_wp
     allocate(base%phib(base%n,base%ms,2)); base%phib = 1.0_wp
     ! data for the visualization
     allocate(base%x_dofs(1,1)); base%x_dofs = 0.0_wp

    case(1)
     base%n  = 2
     base%m  = 2
     base%ms = 1
     allocate(base%xq(1,base%m))
     allocate(base%wq(  base%m))
     base%xq(1,:) = [-1.0_wp/sqrt(3.0_wp) , +1.0_wp/sqrt(3.0_wp)]
     base%wq      = [1.0_wp , 1.0_wp]
     allocate(base%xqs(1,base%ms)); base%xqs = 0.0_wp
     allocate(base%wqs(  base%ms)); base%wqs = 1.0_wp
     allocate(base%phi(   base%n,base%m))
     allocate(base%dphi(1,base%n,base%m))
     allocate(base%phib(base%n,base%ms,2))
     associate( xq => base%xq )
     base%phi(1,:)  = 0.5_wp*(1.0_wp-xq(1,:)) ! 0.5*(1-x)
     base%phi(2,:)  = 0.5_wp*(1.0_wp+xq(1,:)) ! 0.5*(1+x)
     base%dphi(1,1,:) = [-0.5_wp,-0.5_wp]
     base%dphi(1,2,:) = [ 0.5_wp, 0.5_wp]
     end associate
     associate( xb => [-1.0_wp , +1.0_wp] )
     base%phib(1,1,:) = 0.5_wp*(1.0_wp-xb)
     base%phib(2,1,:) = 0.5_wp*(1.0_wp+xb)
     end associate
     ! data for the visualization
     allocate(base%x_dofs(1,2)); base%x_dofs(1,:) = [-1.0_wp,+1.0_wp]

    case default
     error stop 'In "new_1d_base": not implemented'

   end select

 end subroutine new_1d_base

!-----------------------------------------------------------------------------

 !> Two-dimensional base on reference element \(\tilde{K}=[-1,1]^2\)  
 !! Defining the four vertexes
 !! \[
 !!  \begin{array}{l}
 !!   \tilde{v}_1 = (-1,-1) \\
 !!   \tilde{v}_2 = (\phantom{-}1,-1) \\
 !!   \tilde{v}_3 = (\phantom{-}1,\phantom{-}1) \\
 !!   \tilde{v}_4 = (-1,\phantom{-}1),
 !!  \end{array}
 !! \]
 !! the boundary sides are defined as
 !! \[
 !!  \begin{array}{l}
 !!   \tilde{e}_1 = \tilde{v}_2 - \tilde{v}_1 \\
 !!   \tilde{e}_2 = \tilde{v}_3 - \tilde{v}_2 \\
 !!   \tilde{e}_3 = \tilde{v}_4 - \tilde{v}_3 \\
 !!   \tilde{e}_4 = \tilde{v}_1 - \tilde{v}_4.
 !!  \end{array}
 !! \]
 !! On each boundary sides, the values of
 !! \(\tilde{\varphi}_i|_{\tilde{e}}\) are ordered along the
 !! orientation of \(\tilde{e}\).
 !!
 subroutine new_2d_base(base,degree)
  !---------------------------------------------------------------------------
  !input/output variables
  integer,      intent(in)  :: degree !! polynomial degree
  type(t_base), intent(out) :: base
  !---------------------------------------------------------------------------
  !local variables
  integer :: i,is
  !---------------------------------------------------------------------------

   select case(degree)

    case(0)
     base%n  = 1
     base%m  = 1
     base%ms = 1
     allocate(base%xq(2,base%m  )); base%xq  = 0.0_wp
     allocate(base%wq(  base%m  )); base%wq  = 4.0_wp
     allocate(base%xqs(1,base%ms)); base%xqs = 0.0_wp
     allocate(base%wqs(  base%ms)); base%wqs = 2.0_wp
     allocate(base%phi(   base%n,base%m));  base%phi  = 1.0_wp
     allocate(base%dphi(2,base%n,base%m));  base%dphi = 0.0_wp
     allocate(base%phib(base%n,base%ms,4)); base%phib = 1.0_wp
     ! data for the visualization
     allocate(base%x_dofs(2,1)); base%x_dofs = 0.0_wp

    case(1)
     base%n  = 4
     base%m  = 4
     base%ms = 2
     allocate(base%xq(2,base%m))
     allocate(base%wq(  base%m))
     associate( tmp => 1.0_wp/sqrt(3.0_wp) )
     base%xq(1,:) = [-tmp ,  tmp , tmp , -tmp]
     base%xq(2,:) = [-tmp , -tmp , tmp ,  tmp]
     base%wq      = [1.0_wp , 1.0_wp , 1.0_wp , 1.0_wp]
     allocate(base%xqs(1,base%ms)); base%xqs(1,:) = [-tmp , +tmp]
     allocate(base%wqs(  base%ms)); base%wqs      = [1.0_wp,1.0_wp]
     allocate(base%phi(   base%n,base%m))
     allocate(base%dphi(2,base%n,base%m))
     allocate(base%phib(base%n,base%ms,4))
     do i=1,base%m
       base%phi(:,i) = eval_2d_phi(base%n,base%xq(:,i))
       base%dphi(:,:,i) = eval_2d_dphi(base%n,base%xq(:,i))
     end do
!     associate( x1q => base%xq(1,:) , x2q => base%xq(2,:) )
!     base%phi(1,:) = 0.25_wp*(1.0_wp-x1q)*(1.0_wp-x2q) ! 1/4*(1-x)*(1-y)
!     base%phi(2,:) = 0.25_wp*(1.0_wp+x1q)*(1.0_wp-x2q) ! 1/4*(1+x)*(1-y)
!     base%phi(3,:) = 0.25_wp*(1.0_wp+x1q)*(1.0_wp+x2q) ! 1/4*(1+x)*(1+y)
!     base%phi(4,:) = 0.25_wp*(1.0_wp-x1q)*(1.0_wp+x2q) ! 1/4*(1-x)*(1+y)
!     base%dphi(1,1,:) = -0.25_wp*(1.0_wp-x2q) ! -1/4*(1-y)
!     base%dphi(2,1,:) = -0.25_wp*(1.0_wp-x1q) ! -1/4*(1-x)
!     base%dphi(1,2,:) =  0.25_wp*(1.0_wp-x2q) !  1/4*(1-y)
!     base%dphi(2,2,:) = -0.25_wp*(1.0_wp+x1q) ! -1/4*(1+x)
!     base%dphi(1,3,:) =  0.25_wp*(1.0_wp+x2q) !  1/4*(1+y)
!     base%dphi(2,3,:) =  0.25_wp*(1.0_wp+x1q) !  1/4*(1+x)
!     base%dphi(1,4,:) = -0.25_wp*(1.0_wp+x2q) ! -1/4*(1+y)
!     base%dphi(2,4,:) =  0.25_wp*(1.0_wp-x1q) !  1/4*(1-x)
!     end associate
     associate( &
       x1b => [   -tmp,   +tmp,1.0_wp,1.0_wp,  +tmp,  -tmp,-1.0_wp,-1.0_wp] , &
       x2b => [-1.0_wp,-1.0_wp,  -tmp,  +tmp,1.0_wp,1.0_wp,   +tmp,   -tmp] )
     do is=1,4
       do i=1,2
         base%phib(:,i,is)=eval_2d_phi(base%n,[x1b(i+(is-1)*2),x2b(i+(is-1)*2)])
       end do
     end do
!     base%phib(1,:,:) = reshape(0.25_wp*(1.0_wp-x1b)*(1.0_wp-x2b),[2,4])
!     base%phib(2,:,:) = reshape(0.25_wp*(1.0_wp+x1b)*(1.0_wp-x2b),[2,4])
!     base%phib(3,:,:) = reshape(0.25_wp*(1.0_wp+x1b)*(1.0_wp+x2b),[2,4])
!     base%phib(4,:,:) = reshape(0.25_wp*(1.0_wp-x1b)*(1.0_wp+x2b),[2,4])
     end associate !x1b,x2b
     end associate !tmp
     ! data for the visualization
     allocate(base%x_dofs(2,4))
     base%x_dofs(1,:) = [-1.0_wp,+1.0_wp,+1.0_wp,-1.0_wp]
     base%x_dofs(2,:) = [-1.0_wp,-1.0_wp,+1.0_wp,+1.0_wp]

    case default
     error stop 'In "new_2d_base": not implemented'

   end select

 end subroutine new_2d_base
 
!-----------------------------------------------------------------------------

 !> Evaluate the 2d polynomial basis functions \(\phi_j(\tilde{x}_1,\tilde{x}_2) \)  
 !! \(j=1,\dots,(\text{degree}+1)^2\),  
 !! at position \((\tilde{x}_1,\tilde{x}_2)\in[-1;1]^2\).  
 !!
 function eval_2d_phi(nb,xi) result(phi)
  !---------------------------------------------------------------------------
  !input variables
  integer,      intent(in)  :: nb    !! number of 2d basis functions
  real(wp),     intent(in)  :: xi(2) !! evaluation point in reference space
  !output variables
  real(wp)                  :: phi(nb) !! evaluation of all basis functions at np points 
  !---------------------------------------------------------------------------
   select case(nb)

    case(1)
      phi(1)  = 1.0_wp

    case(4)
      phi(1) = 0.25_wp*(1.0_wp-xi(1))*(1.0_wp-xi(2)) ! 1/4*(1-x)*(1-y)
      phi(2) = 0.25_wp*(1.0_wp+xi(1))*(1.0_wp-xi(2)) ! 1/4*(1+x)*(1-y)
      phi(3) = 0.25_wp*(1.0_wp+xi(1))*(1.0_wp+xi(2)) ! 1/4*(1+x)*(1+y)
      phi(4) = 0.25_wp*(1.0_wp-xi(1))*(1.0_wp+xi(2)) ! 1/4*(1-x)*(1+y)

    case default
     write(*,*)'nb=',nb
      error stop 'In "new_2d_base": phi not implemented'

   end select

 end function eval_2d_phi

!-----------------------------------------------------------------------------

 !> Evaluate the gradient of the 2d polynomial basis functions 
 !!\[
 !! \frac{\partial\phi_j(\tilde{x}_1,\tilde{x}_2)}{\partial \tilde{x}_i}
 !!\]
 !!\(j=1,\dots,(\text{degree}+1)^2\),  
 !! at position \((\tilde{x}_1,\tilde{x}_2)\in[-1;1]^2\).  
 !!
 function eval_2d_dphi(nb,xi) result(dphi)
  !---------------------------------------------------------------------------
  !input variables
  integer,      intent(in)  :: nb    !! number of 2d basis functions
  real(wp),     intent(in)  :: xi(2) !! evaluation point in reference space
  !output variable
  real(wp)                  :: dphi(2,nb) !! evaluation of d/dx d/dy of all basis functions at np points 
  !---------------------------------------------------------------------------
   select case(nb)

    case(1)
      dphi(:,1)  = 0.0_wp

    case(4)
      dphi(1,1) = -0.25_wp*(1.0_wp-xi(2)) ! -1/4*(1-y)
      dphi(2,1) = -0.25_wp*(1.0_wp-xi(1)) ! -1/4*(1-x)
      dphi(1,2) =  0.25_wp*(1.0_wp-xi(2)) !  1/4*(1-y)
      dphi(2,2) = -0.25_wp*(1.0_wp+xi(1)) ! -1/4*(1+x)
      dphi(1,3) =  0.25_wp*(1.0_wp+xi(2)) !  1/4*(1+y)
      dphi(2,3) =  0.25_wp*(1.0_wp+xi(1)) !  1/4*(1+x)
      dphi(1,4) = -0.25_wp*(1.0_wp+xi(2)) ! -1/4*(1+y)
      dphi(2,4) =  0.25_wp*(1.0_wp-xi(1)) !  1/4*(1-x)

    case default
      error stop 'In "new_2d_base": dphi not implemented'

   end select

 end function eval_2d_dphi

!-----------------------------------------------------------------------------

 !> deallocate arrays and set defaults
 !!
 subroutine clean_base(base)
  !---------------------------------------------------------------------------
  class(t_base), intent(inout) :: base
  !---------------------------------------------------------------------------

   deallocate( base%x_dofs )
   deallocate( base%phi, base%dphi, base%phib )
   deallocate( base%xqs, base%wqs, base%xq, base%wq )
   base%n  = -1
   base%m  = -1
   base%ms = -1

 end subroutine clean_base

!-----------------------------------------------------------------------------

end module mod_base
