module mod_2d_grid

 use mod_kinds, only: wp

 use mod_grid, only: t_grid, c_s, c_e
 
 implicit none

 public :: new_2d_grid
 ! The following should be used only for testing
 public :: t_s2d, t_e2d

 private

 type, extends(c_s) :: t_s2d
  real(wp) :: n(2)   !! unit normal \(\V{n}_e\)
  real(wp) :: length !! side length \(|e|\)
 contains
  procedure, pass(s) :: get_n     => get_n_2d
  procedure, pass(s) :: get_sdetj => get_sdetj_2d
 end type t_s2d

 !> Rectangular two-dimensional element
 !! \([x_{1,{\rm left}},x_{1,{\rm right}}]\times
 !!   [x_{2,{\rm left}},x_{2,{\rm right}}]\)
 type, extends(c_e) :: t_e2d
  real(wp) :: x1_left, x1_right
  real(wp) :: x2_left, x2_right
 contains
  procedure, pass(e) :: get_j_it => get_j_it_2d
  procedure, pass(e) :: get_detj => get_detj_2d
  procedure, pass(e) :: map      => map_2d
 end type t_e2d

 !! \(\Delta \tilde{x}_1 = \Delta \tilde{x}_2\) for the reference
 !! element
 real(wp), parameter :: delta_x_tilde = 2.0_wp

 !! Length of the reference side \(\tilde{\tilde{e}}=[-1,1]\)
 real(wp), parameter :: length_e_tilde = 2.0_wp

contains

 ! Setup the module variable grid
 subroutine new_2d_grid(grid, ne,x_left,x_right)
  integer,      intent(in) :: ne(2)
  real(wp),     intent(in) :: x_left(2), x_right(2) ! domain size
  type(t_grid), intent(out) :: grid

  integer :: is, ie1, ie2, ie, iloc

   grid%ns = ne(1)*ne(2) + ne(1)*(ne(2)+1) ! counting x and y sides
   grid%ne = ne(1)*ne(2)

   ! allocate and define the side array
   allocate( t_s2d::grid%s( grid%ns ) )
   select type( sides => grid%s ); type is(t_s2d)

   do ie1=1,ne(1)
     do ie2=1,ne(2)

       ! 1) sides with normal parallel to the x1 axis
       is = ie1 + (ie2-1)*ne(1)
       if( ie1 .eq. 1 ) then ! periodic boundary conditions
         sides(is)%e_l = ie2*ne(1)             ! left element
         sides(is)%e_r = 1 + (ie2-1)*ne(1)     ! right element
       else
         sides(is)%e_l = ie1 + (ie2-1)*ne(1) - 1
         sides(is)%e_r = ie1 + (ie2-1)*ne(1)
       endif
       sides(is)%iloc_l = 2
       sides(is)%iloc_r = 4
       sides(is)%orient_l = +1
       sides(is)%orient_r = -1

       ! 2) sides with normal parallel to the x2 axis
       is = ie2 + (ie1-1)*ne(2) + ne(1)*ne(2)
       if( ie2 .eq. 1 ) then ! no-slip
         sides(is)%e_l = ie1 + (ie2-1)*ne(1)
         sides(is)%e_r = -1
         sides(is)%iloc_l = 1
         sides(is)%iloc_r = -9999 ! not defined
         sides(is)%orient_l = 1
         sides(is)%orient_r = -9999 ! not defined
       else
         sides(is)%e_l = ie1 + (ie2-2)*ne(1)
         sides(is)%e_r = ie1 + (ie2-1)*ne(1)
         sides(is)%iloc_l = 3
         sides(is)%iloc_r = 1
         sides(is)%orient_l = -1
         sides(is)%orient_r = +1
       endif

       if( ie2 .eq. ne(2) ) then ! no-slip
         is = ie1 + 2*ne(1)*ne(2)
         sides(is)%e_l = ie1 + (ie2-1)*ne(1)
         sides(is)%e_r = -1
         sides(is)%iloc_l = 3
         sides(is)%iloc_r = -9999 ! not defined
         sides(is)%orient_l = -1
         sides(is)%orient_r = -9999 ! not defined
       endif

     enddo
   enddo

   ! allocate and define the element array
   allocate( t_e2d::grid%e( grid%ne ) )
   select type( elems => grid%e ); type is(t_e2d)

   do ie=1,grid%ne
     ! each element has two sides
     allocate( elems(ie)%s(4) )
   end do
   ! loop over the sides to fill the element information
   do is=1,grid%ns
     ! left element
     ie   = sides(is)%e_l
     iloc = sides(is)%iloc_l
     elems(ie)%s(iloc) = is
     ! right element
     ie   = sides(is)%e_r
     if( ie .gt. 0 ) then
       iloc = sides(is)%iloc_r
       elems(ie)%s(iloc) = is
     endif
   end do
   ! geometry
   do ie1=1,ne(1)
     do ie2=1,ne(2)
       ie = ie1 + (ie2-1)*ne(1)

       elems(ie)%x1_left = x_left(1) &
         + (x_right(1)-x_left(1))/real(ne(1),wp)*(ie1-1)
       elems(ie)%x1_right = x_left(1) &
         + (x_right(1)-x_left(1))/real(ne(1),wp)* ie1

       elems(ie)%x2_left = x_left(2) &
         + (x_right(2)-x_left(2))/real(ne(2),wp)*(ie2-1)
       elems(ie)%x2_right = x_left(2) &
         + (x_right(2)-x_left(2))/real(ne(2),wp)* ie2

       if( ie2 .eq. 1 ) then
         associate( bottom_s => elems(ie)%s(1) , &
                      left_s => elems(ie)%s(4) )
         sides(bottom_s)%n = [0.0_wp,-1.0_wp]
         sides(left_s  )%n = [1.0_wp, 0.0_wp]

         sides(bottom_s)%length = elems(ie)%x1_right - elems(ie)%x1_left
         sides(left_s  )%length = elems(ie)%x2_right - elems(ie)%x2_left
         end associate
       else
         associate( bottom_s => elems(ie)%s(1) , &
                      left_s => elems(ie)%s(4) )
         sides(bottom_s)%n = [0.0_wp,1.0_wp]
         sides(left_s  )%n = [1.0_wp,0.0_wp]

         sides(bottom_s)%length = elems(ie)%x1_right - elems(ie)%x1_left
         sides(left_s  )%length = elems(ie)%x2_right - elems(ie)%x2_left
         end associate
         if( ie2 .eq. ne(2) ) then
           associate( top_s => elems(ie)%s(3) )
           sides(top_s)%n = [0.0_wp,1.0_wp]
           sides(top_s)%length = elems(ie)%x1_right - elems(ie)%x1_left
           end associate
         endif
       endif

     enddo
   enddo

   end select

   end select

 end subroutine new_2d_grid
 
!-----------------------------------------------------------------------

 pure subroutine get_n_2d(n, s,x)
  class(t_s2d), intent(in)  :: s
  real(wp),     intent(in)  :: x(:,:)
  real(wp),     intent(out) :: n(:,:)

   ! the normal is the same at all the points (straight sides)
   n(1,:) = s%n(1)
   n(2,:) = s%n(2)

 end subroutine get_n_2d

 pure subroutine get_sdetj_2d(detj, s,x)
  class(t_s2d), intent(in)  :: s
  real(wp),     intent(in)  :: x(:,:)
  real(wp),     intent(out) :: detj(:)

   ! the determinant is the ratio |e|/|\tilde{e}|
   detj = s%length / length_e_tilde

 end subroutine get_sdetj_2d

 pure subroutine get_j_it_2d(j_it, e,x)
  class(t_e2d), intent(in)  :: e
  real(wp),     intent(in)  :: x(:,:)
  real(wp),     intent(out) :: j_it(:,:,:)

   j_it = 0.0_wp
   j_it(1,1,:) = delta_x_tilde / (e%x1_right-e%x1_left)
   j_it(2,2,:) = delta_x_tilde / (e%x2_right-e%x2_left)

 end subroutine get_j_it_2d

 pure subroutine get_detj_2d(detj, e,x)
  class(t_e2d), intent(in)  :: e
  real(wp),     intent(in)  :: x(:,:)
  real(wp),     intent(out) :: detj(:)

   detj = (e%x1_right-e%x1_left)*(e%x2_right-e%x2_left) &
          / delta_x_tilde**2

 end subroutine get_detj_2d

 pure subroutine map_2d(x, e,x_tilde)
  class(t_e2d), intent(in)  :: e
  real(wp),     intent(in)  :: x_tilde(:,:)
  real(wp),     intent(out) :: x(:,:)

   x(1,:) = 0.5_wp*(e%x1_left+e%x1_right) &
           + (e%x1_right-e%x1_left)/delta_x_tilde * x_tilde(1,:)
   x(2,:) = 0.5_wp*(e%x2_left+e%x2_right) &
           + (e%x2_right-e%x2_left)/delta_x_tilde * x_tilde(2,:)
   
 end subroutine map_2d

!-----------------------------------------------------------------------

end module mod_2d_grid
