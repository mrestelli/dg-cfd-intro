module mod_scalar_ad_ode

 use mod_kinds, only: wp

 use mod_ode_state, only: c_ode_state

 use mod_ode_solver, only: c_ode

 use mod_linalg, only: invmat_chol

 use mod_grid, only: t_grid

 use mod_base, only: t_base

 use mod_scalar_ad_state, only: t_scalar_ad_state

 use mod_scalar_ad_local_int, only: &
   element_mass_matrix, element_integral, side_integral, &
   g_element_integral, g_side_integral

 implicit none

 public :: &
   t_scalar_ad_ode, scalar_ad_ode_setup, scalar_ad_ode_clean

 private

 !> Compute the rhs resulting from the DG discretization of
 !! \[
 !!  \partial_t a +\Div( \V{u}a -\nu\Grad a) = 0
 !! \]
 !! where u is constant.
 type, extends(c_ode) :: t_scalar_ad_ode
  real(wp), allocatable :: u(:) !< advection coefficient
  real(wp)              :: nu   !< diffusion coefficient
  type(t_grid) :: grid
  type(t_base) :: base
  !> \(\left(M^K\right)^{-1}\) for all the elements
  real(wp), allocatable :: imass_matrices(:,:,:)
 contains
  procedure, pass(ode) :: compute_rhs => compute_scalar_ad_rhs
  !> generate a state vector for this ODE
  procedure, pass(ode) :: new_scalar_ad_state
 end type t_scalar_ad_ode

contains

!-----------------------------------------------------------------------

 subroutine scalar_ad_ode_setup(ode, base,grid, u,nu)
  type(t_base),           intent(in) :: base
  type(t_grid),           intent(in) :: grid
  real(wp),               intent(in) :: u(:)
  real(wp),               intent(in) :: nu
  type(t_scalar_ad_ode), intent(out) :: ode

  integer :: ie
  real(wp), allocatable :: m(:,:) ! work array for mass matrix

   ode%base = base

   ode%grid = grid

   allocate( ode%u(size(u)) )
   ode%u  = u
   ode%nu = nu

   ! precompute the mass matrices
   allocate(m(ode%base%n,ode%base%n))
   allocate( ode%imass_matrices(ode%base%n,ode%base%n,ode%grid%ne) )
   do ie=1,ode%grid%ne
     ! mass matrix
     call element_mass_matrix( m, ode%base, ode%grid%e(ie) )
     ! invert the mass matrix
     call invmat_chol( m , ode%imass_matrices(:,:,ie) )
   enddo
   deallocate(m)

 end subroutine scalar_ad_ode_setup

 subroutine scalar_ad_ode_clean(ode)
  type(t_scalar_ad_ode), intent(inout) :: ode

   deallocate( ode%imass_matrices )
   call ode%base%clean()
   call ode%grid%clean()

 end subroutine scalar_ad_ode_clean

 subroutine new_scalar_ad_state(y, ode)
  class(t_scalar_ad_ode),   intent(in)  :: ode
  class(t_scalar_ad_state), intent(out) :: y

   allocate( y%aa(ode%base%n,ode%grid%ne) )

 end subroutine new_scalar_ad_state

!-----------------------------------------------------------------------

 subroutine compute_scalar_ad_rhs(rhs,ode,y)
  class(t_scalar_ad_ode), intent(in) :: ode
  class(c_ode_state), intent(in)    :: y
  class(c_ode_state), intent(inout) :: rhs

  ! g = grad(a)
  real(wp) :: g( size(ode%base%dphi,1) , ode%base%n , ode%grid%ne )

   select type(y);   type is(t_scalar_ad_state)
   select type(rhs); type is(t_scalar_ad_state)

   call compute_auxiliary_variable(g,ode,y)

   call compute_rhs(rhs,ode,y,g)

   end select
   end select

 end subroutine compute_scalar_ad_rhs

!-----------------------------------------------------------------------

 pure subroutine compute_auxiliary_variable(g,ode,y)
  class(t_scalar_ad_ode),   intent(in) :: ode
  class(t_scalar_ad_state), intent(in) :: y
  !! \(\V{g}_h\): \(\code{g(:,i,ie)} = \V{g}_i^{K_{\code{ie}}}\)
  real(wp), intent(out) :: g(:,:,:)

  integer :: ie, is, s
  integer :: e_l, e_r

   ! initialize g
   g = 0.0_wp

   ! element integrals
   do ie=1,ode%grid%ne
     associate( ak => y%aa(:,ie) )
     call g_element_integral( g(:,:,ie), ode%base,ode%grid%e(ie), ak )
     end associate
   enddo

   ! side fluxes
   do is=1,ode%grid%ns
     e_l = ode%grid%s(is)%e_l
     e_r = ode%grid%s(is)%e_r
     associate( ak_l=>y%aa(:,e_l) , ak_r=>y%aa(:,e_r) )
     call g_side_integral( g(:,:,e_l),g(:,:,e_r), ode%base, &
                           ode%grid%s(is) , ak_l,ak_r )
     end associate
   enddo

   ! include the mass matrix
   do ie=1,ode%grid%ne
     do s=1,size(g,1)
       g(s,:,ie) = matmul(ode%imass_matrices(:,:,ie) , g(s,:,ie))
     enddo
   enddo

 end subroutine compute_auxiliary_variable

!-----------------------------------------------------------------------

 pure subroutine compute_rhs(rhs,ode,y,g)
  class(t_scalar_ad_ode),   intent(in)    :: ode
  class(t_scalar_ad_state), intent(in)    :: y
  real(wp),                 intent(in)    :: g(:,:,:)
  class(t_scalar_ad_state), intent(inout) :: rhs

  integer :: ie, is
  integer :: e_l, e_r

   ! initialize rhs
   rhs%aa = 0.0_wp

   ! element integrals
   do ie=1,ode%grid%ne
     associate( ak=>y%aa(:,ie) , gk=>g(:,:,ie) )
     call element_integral(rhs%aa(:,ie), ode%u,ode%nu, &
                           ode%base,ode%grid%e(ie), ak,gk)
     end associate
   enddo

   ! side fluxes
   do is=1,ode%grid%ns
     e_l = ode%grid%s(is)%e_l
     e_r = ode%grid%s(is)%e_r
     associate( ak_l=>y%aa(:,e_l) , ak_r=>y%aa(:,e_r) , &
                gk_l=>g(:,:,e_l)  , gk_r=>g(:,:,e_r)  )
     call side_integral( rhs%aa(:,e_l),rhs%aa(:,e_r) , ode%u,ode%nu, &
              ode%base, ode%grid%s(is), ak_l,ak_r , gk_l,gk_r )
     end associate
   enddo

   ! include the mass matrix
   do ie=1,ode%grid%ne
     rhs%aa(:,ie) = matmul(ode%imass_matrices(:,:,ie) , rhs%aa(:,ie))
   enddo

 end subroutine compute_rhs

!-----------------------------------------------------------------------

end module mod_scalar_ad_ode
