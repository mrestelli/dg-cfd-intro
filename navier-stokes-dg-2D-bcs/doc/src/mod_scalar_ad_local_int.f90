module mod_scalar_ad_local_int

 use mod_kinds, only: wp

 use mod_grid, only: t_grid, c_s, c_e

 use mod_base, only: t_base

 implicit none

 public :: element_mass_matrix, &
   element_integral, side_integral, &
   g_element_integral, g_side_integral

 private

 !> Parameter \(\eta\) for the stabilization of the diffusive flux
 real(wp), parameter :: eta = 0.5_wp

contains

!-----------------------------------------------------------------------

 !> Compute the element integral
 !! \[
 !!  M^K_{ij} = \int_K \varphi_i^K \varphi_j^K
 !! \]
 !! for every element \(K\in\mathcal{T}_h\).
 !!
 !! The computation uses
 !! \begin{align*}
 !! \int_K \varphi_i^K \varphi_j^K
 !! & \approx 
 !! \sum_{l=1}^m \tilde{w}^Q_l \left[ det(J_K)\, \varphi_i
 !! \varphi_j \right](\tilde{\V{x}}^Q_l).
 !! \end{align*}
 pure subroutine element_mass_matrix(m, base,e)
  type(t_base), intent(in)  :: base  !! base and quadrature formula
  class(c_e),   intent(in)  :: e     !! element \(K\)
  real(wp),     intent(out) :: m(:,:)

  integer :: i, j, l
  real(wp) :: detj(base%m)

   ! retrieve det(J) at all the quadrature points
   call e%get_detj( detj , base%xq )

   ! evaluate the integral
   m = 0.0_wp
   do l=1,base%m
     do j=1,base%n
       do i=1,base%n
         m(i,j) = m(i,j) &
                 + base%wq(l)*detj(l) * base%phi(i,l)*base%phi(j,l)
       enddo
     enddo
   enddo

 end subroutine element_mass_matrix

!-----------------------------------------------------------------------

 !> Compute the element integral
 !! \[
 !!  \int_K (\V{u}a_h|_K -\nu \V{g}_h|_K) \cdot\Grad\varphi_i^K,
 !! \]
 !! for every basis function \(\varphi_i^K\in V_h(K)\) and for a given
 !! element \(K\in\mathcal{T}_h\).
 !!
 !! The computation uses
 !! \begin{align*}
 !! \int_K (\V{u}a_h|_K -\nu \V{g}_h|_K) \cdot \Grad\varphi_i^K 
 !! & \approx 
 !! \sum_{l=1}^m \tilde{w}^Q_l \left[ det(J_K)\, (\V{u}a_h 
 !! -\nu \V{g}_h) \cdot \Grad\varphi_i \right](\tilde{\V{x}}^Q_l)
 !! \end{align*}
 !! and the fact that
 !! \[
 !!  \Grad\varphi_i = J_K^{-T}\tilde{\Grad}\tilde{\varphi}_i.
 !! \]
 !!
 !! The element integral is *added* to ```e_rhs```.
 pure subroutine element_integral(e_rhs, u,nu, base,e,ak,gk)
  real(wp),     intent(in)    :: u(:)  !! advection coefficient
  real(wp),     intent(in)    :: nu    !! diffusion coefficient
  type(t_base), intent(in)    :: base  !! base and quadrature formula
  class(c_e),   intent(in)    :: e     !! element
  !> \(a^K_i, \quad i=1,\ldots,n\)
  real(wp),     intent(in)    :: ak(:)
  !> \(\V{g}^K_i, \quad i=1,\ldots,n\)
  real(wp),     intent(in)    :: gk(:,:)
  real(wp),     intent(inout) :: e_rhs(:)

  integer :: i, l, s
  real(wp) :: ah_k(base%m), gh_k(size(u),base%m), &
    flux(size(u)), gradphi(size(u)), &
    j_it(size(u),size(u),base%m), detj(base%m)

   ! retrieve J^{-T} and det(J) at all the quadrature points
   call e%get_j_it( j_it , base%xq )
   call e%get_detj( detj , base%xq )

   ! compute the local solution at all the quadrature points
   ah_k = 0.0_wp
   gh_k = 0.0_wp
   do i=1,base%n
     ah_k = ah_k + ak(i)*base%phi(i,:)
     do s=1,size(u)
       gh_k(s,:) = gh_k(s,:) + gk(s,i)*base%phi(i,:)
     enddo
   enddo

   ! evaluate the integral
   do l=1,base%m
     flux = u*ah_k(l) - nu*gh_k(:,l)
     do i=1,base%n
       gradphi  = matmul( j_it(:,:,l) , base%dphi(:,i,l) )
       e_rhs(i) = e_rhs(i) &
         + base%wq(l)*detj(l) * dot_product( flux , gradphi )
     enddo
   enddo

 end subroutine element_integral

!-----------------------------------------------------------------------

 !> Compute the element integral
 !! \[
 !!  -\int_K a_h|_K \Grad\varphi_i^K,
 !! \]
 !! for every basis function \(\varphi_i^K\in V_h(K)\) and for a given
 !! element \(K\in\mathcal{T}_h\).
 !!
 !! The element integral is *added* to ```e_rhs```.
 pure subroutine g_element_integral(e_rhs, base,e,ak)
  type(t_base), intent(in)    :: base  !! base and quadrature formula
  class(c_e),   intent(in)    :: e     !! element
  real(wp),     intent(in)    :: ak(:) !! \(a^K_i, \quad i=1,\ldots,n\)
  !> \(\code{e\_rhs(s,i)} = -\int_K a_h|_K \partial_{x_s}\varphi_i^K,\)
  real(wp),     intent(inout) :: e_rhs(:,:)

  integer :: i, l
  real(wp) :: ah_k(base%m), &
    j_it(size(base%dphi,1),size(base%dphi,1),base%m), detj(base%m), &
    gradphi(size(base%dphi,1))

   ! retrieve J^{-T} and det(J) at all the quadrature points
   call e%get_j_it( j_it , base%xq )
   call e%get_detj( detj , base%xq )

   ah_k = 0.0_wp
   do i=1,base%n
     ah_k = ah_k + ak(i)*base%phi(i,:)
   enddo

   do l=1,base%m
     do i=1,base%n
       gradphi  = matmul( j_it(:,:,l) , base%dphi(:,i,l) )
       e_rhs(:,i) = e_rhs(:,i) &
         - base%wq(l)*detj(l) * ah_k(l) * gradphi
     enddo
   enddo

 end subroutine g_element_integral

!-----------------------------------------------------------------------

 !> Compute the side integrals
 !! \[
 !!  -\int_{e} (\widehat{\V{u}a} - \widehat{\nu\V{g}})
 !!  \cdot\sigma_{K,e}\V{n}_e \varphi_i^K,
 !! \]
 !! for every basis function \(\varphi_i^K\in V_h(K)\), for \(
 !! K=K_L(e),K_R(e) \) and for a given side \(e\in\mathcal{E}_h\).
 !!
 !! The side integral is computed as
 !! \begin{align*}
 !!  -\int_{e} (\widehat{\V{u}a} - \widehat{\nu\V{g}})
 !!  \cdot\sigma_{K,e}\V{n}_e \varphi_i^K
 !! & \approx 
 !! \sum_{l^s=1}^{m^s} \sigma_{K,e}\tilde{w}^{Q^s}_{l^s} \left[
 !!  det(J_e) (\widehat{\V{u}a} - \widehat{\nu\V{g}}) 
 !!  \cdot \V{n}_e \varphi_i^K \right](\tilde{\V{x}}^{Q^s}_{l^s}).
 !! \end{align*}
 !!
 !! Recall that
 !! \[
 !!  \sigma_{K_L(e),e} = 1, \qquad
 !!  \sigma_{K_R(e),e} = -1.
 !! \]
 !!
 !! The side integrals are *added* to ```e_rhs1``` and ```e_rhs2```.
 pure subroutine side_integral(s_rhs_l,s_rhs_r, u,nu, base,s, &
                               ak_l,ak_r , gk_l,gk_r)
  !> advection coefficient
  real(wp),     intent(in)    :: u(:)
  !> diffusion coefficient
  real(wp),     intent(in)    :: nu
  !> base and quadrature formula
  type(t_base), intent(in)    :: base
  !> side \( e \)
  class(c_s),   intent(in)    :: s
  !> \(a^{K_L}_i, \quad i=1,\ldots,n\)
  real(wp),     intent(in)    :: ak_l(:)
  !> \(a^{K_R}_i, \quad i=1,\ldots,n\)
  real(wp),     intent(in)    :: ak_r(:)
  !> \(\V{g}^{K_L}_i, \quad i=1,\ldots,n\)
  real(wp),     intent(in)    :: gk_l(:,:)
  !> \(\V{g}^{K_R}_i, \quad i=1,\ldots,n\)
  real(wp),     intent(in)    :: gk_r(:,:)
  !> \(-\int_{e} \widehat{\V{u}a} \cdot\sigma_{K_L,e}\V{n}_e
  !! \varphi_i^{K_L}\)
  real(wp),     intent(inout) :: s_rhs_l(:)
  !> \(-\int_{e} \widehat{\V{u}a} \cdot\sigma_{K_R,e}\V{n}_e
  !! \varphi_i^{K_R}\)
  real(wp),     intent(inout) :: s_rhs_r(:)

  integer :: i, l, l_l, l_r
  integer :: l_lr(base%ms,-1:+1)
  real(wp) :: ah_kl(base%ms), ah_kr(base%ms)
  real(wp) :: gh_kl(size(u),base%ms), gh_kr(size(u),base%ms)
  real(wp) :: u_dot_n, n_adv_flux_l, n_adv_flux_r, &
              n_diff_flux_l, n_diff_flux_r, numflux
  real(wp) :: n(size(u),base%ms), detj(base%ms)

   ! fill the quadrature node indexing array
   l_lr(:,-1) = [(base%ms-l+1, l=1,base%ms)]
   l_lr(:,+1) = [(        l,   l=1,base%ms)]

   ! retrieve n and det(J) at all the quadrature points
   call s%get_n(        n , base%xqs )
   call s%get_sdetj( detj , base%xqs )

   ! compute the local solution at all the quadrature points
   ah_kl = 0.0_wp
   ah_kr = 0.0_wp
   gh_kl = 0.0_wp
   gh_kr = 0.0_wp
   do l=1,base%ms
     l_l = l_lr(l,s%orient_l)
     l_r = l_lr(l,s%orient_r)
     do i=1,base%n
       ah_kl(l)   = ah_kl(l)   +   ak_l(i)*base%phib(i,l_l,s%iloc_l)
       ah_kr(l)   = ah_kr(l)   +   ak_r(i)*base%phib(i,l_r,s%iloc_r)
       gh_kl(:,l) = gh_kl(:,l) + gk_l(:,i)*base%phib(i,l_l,s%iloc_l)
       gh_kr(:,l) = gh_kr(:,l) + gk_r(:,i)*base%phib(i,l_r,s%iloc_r)
     enddo
   enddo

   ! evaluate the integral
   do l=1,base%ms
     l_l = l_lr(l,s%orient_l)
     l_r = l_lr(l,s%orient_r)
     u_dot_n = dot_product(u,n(:,l))
     n_adv_flux_l = u_dot_n*ah_kl(l)
     n_adv_flux_r = u_dot_n*ah_kr(l)
     n_diff_flux_l = -nu*dot_product(gh_kl(:,l),n(:,l))
     n_diff_flux_r = -nu*dot_product(gh_kr(:,l),n(:,l))
     numflux = upwind_flux( u_dot_n,n_adv_flux_l,n_adv_flux_r) &
          + stab_diff_flux( nu,n_diff_flux_l,n_diff_flux_r,    &
                            ah_kl(l),ah_kr(l) )
     do i=1,base%n
       s_rhs_l(i) = s_rhs_l(i) &
         - base%wqs(l)*detj(l) * numflux*base%phib(i,l_l,s%iloc_l)
       s_rhs_r(i) = s_rhs_r(i) &
         + base%wqs(l)*detj(l) * numflux*base%phib(i,l_r,s%iloc_r)
     enddo
   enddo

 end subroutine side_integral

!-----------------------------------------------------------------------

 !> Compute the side integrals
 !! \[
 !!  \int_{e} \widehat{a} \sigma_{K,e}\V{n}_e \varphi_i^K,
 !! \]
 !! for every basis function \(\varphi_i^K\in V_h(K)\), for \(
 !! K=K_1(e),K_2(e) \) and for a given
 !! side \(e\in\mathcal{E}_h\).
 !!
 !! The side integrals are *added* to ```e_rhs1``` and ```e_rhs2```.
 pure subroutine g_side_integral(s_rhs_l,s_rhs_r, base,s,ak_l,ak_r)
  !> base and quadrature formula
  type(t_base), intent(in)    :: base
  !> side \( e \)
  class(c_s),   intent(in)    :: s
  !> \(a^{K_1}_i, \quad i=1,\ldots,n\)
  real(wp),     intent(in)    :: ak_l(:)
  !> \(a^{K_2}_i, \quad i=1,\ldots,n\)
  real(wp),     intent(in)    :: ak_r(:)
  !> \(\int_{e} \widehat{a} \sigma_{K_L,e}\V{n}_e \varphi_i^{K_L}\)
  real(wp),     intent(inout) :: s_rhs_l(:,:)
  !> \(\int_{e} \widehat{a} \sigma_{K_R,e}\V{n}_e \varphi_i^{K_R}\)
  real(wp),     intent(inout) :: s_rhs_r(:,:)

  integer :: i, l, l_l, l_r
  integer :: l_lr(base%ms,-1:+1)
  real(wp) :: ah_kl(base%ms), ah_kr(base%ms)
  real(wp) :: numflux(base%ms)
  real(wp) :: n(size(base%dphi,1),base%ms), detj(base%ms)

   ! fill the quadrature node indexing array
   l_lr(:,-1) = [(base%ms-l+1, l=1,base%ms)]
   l_lr(:,+1) = [(        l,   l=1,base%ms)]

   ! retrieve n and det(J) at all the quadrature points
   call s%get_n(        n , base%xqs )
   call s%get_sdetj( detj , base%xqs )

   ! compute the local solution at all the quadrature points
   ah_kl = 0.0_wp
   ah_kr = 0.0_wp
   do l=1,base%ms
     l_l = l_lr(l,s%orient_l)
     l_r = l_lr(l,s%orient_r)
     do i=1,base%n
       ah_kl(l) = ah_kl(l) + ak_l(i)*base%phib(i,l_l,s%iloc_l)
       ah_kr(l) = ah_kr(l) + ak_r(i)*base%phib(i,l_r,s%iloc_r)
     enddo
   enddo

   numflux = 0.5_wp*(ah_kl + ah_kr) ! centered numerical flux

   ! evaluate the integral
   do l=1,base%ms
     l_l = l_lr(l,s%orient_l)
     l_r = l_lr(l,s%orient_r)
     do i=1,base%n
       s_rhs_l(:,i) = s_rhs_l(:,i) + base%wqs(l)*detj(l) &
              * numflux(l)*n(:,l)*base%phib(i,l_l,s%iloc_l)
       s_rhs_r(:,i) = s_rhs_r(:,i) - base%wqs(l)*detj(l) &
              * numflux(l)*n(:,l)*base%phib(i,l_r,s%iloc_r)
     enddo
   enddo

 end subroutine g_side_integral

!-----------------------------------------------------------------------

 pure function upwind_flux(u_dot_n,nflux_l,nflux_r) result(upwflux)
  real(wp), intent(in) :: u_dot_n !! \( \V{u}\cdot\V{n} \)
  real(wp), intent(in) :: nflux_l !! normal flux on \(K_L\)
  real(wp), intent(in) :: nflux_r !! normal flux on \(K_R\)
  real(wp) :: upwflux

   if( u_dot_n .ge. 0 ) then
     upwflux = nflux_l
   else
     upwflux = nflux_r
   endif

 end function upwind_flux

!-----------------------------------------------------------------------

 pure function stab_diff_flux(nu,nflux_l,nflux_r,a_l,a_r) result(nflux)
  real(wp), intent(in) :: nu      !! diffusion coefficient
  real(wp), intent(in) :: nflux_l !! flux on \(K_L(e)\)
  real(wp), intent(in) :: nflux_r !! flux on \(K_R(e)\)
  real(wp), intent(in) :: a_l     !! solution a on \(K_L(e)\)
  real(wp), intent(in) :: a_r     !! solution a on \(K_R(e)\)
  real(wp) :: nflux

   nflux = 0.5_wp*(nflux_l+nflux_r) + nu*eta*(a_l-a_r)

 end function stab_diff_flux

!-----------------------------------------------------------------------

end module mod_scalar_ad_local_int

