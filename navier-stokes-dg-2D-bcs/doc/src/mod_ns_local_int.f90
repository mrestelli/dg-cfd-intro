module mod_ns_local_int

 use mod_kinds, only: wp

 use mod_grid, only: t_grid, c_s, c_e

 use mod_base, only: t_base

 implicit none

 public :: element_mass_matrix, &
   element_integral, side_integral, &
   g_element_integral, g_side_integral, &
   gamma, cp, cv, rgas, &
   ! only used to test this module
   euler_num_flux

 private

 real(wp), parameter :: &
   cp = 1004.67_wp, rgas = 287.17_wp, &
   cv = cp-rgas, gamma = cp/cv, kappa = rgas/cp

 !> Parameter \(\eta\) for the stabilization of the diffusive flux
 real(wp), parameter :: eta = 0.5_wp

contains

!-----------------------------------------------------------------------

 pure subroutine element_mass_matrix(m, base,e)
  type(t_base), intent(in)  :: base  !! base and quadrature formula
  class(c_e),   intent(in)  :: e     !! element \(K\)
  real(wp),     intent(out) :: m(:,:)

  integer :: i, j, l
  real(wp) :: detj(base%m)

   ! retrieve det(J) at all the quadrature points
   call e%get_detj( detj , base%xq )

   ! evaluate the integral
   m = 0.0_wp
   do l=1,base%m
     do j=1,base%n
       do i=1,base%n
         m(i,j) = m(i,j) &
                 + base%wq(l)*detj(l) * base%phi(i,l)*base%phi(j,l)
       enddo
     enddo
   enddo

 end subroutine element_mass_matrix

!-----------------------------------------------------------------------

 pure subroutine element_integral(e_rhs, d,mu,alpha, base,e, uuk,ggk)
  integer,      intent(in)    :: d     !! space dimension
  real(wp),     intent(in)    :: mu    !! \(\mu\)
  real(wp),     intent(in)    :: alpha !! \(\frac{c_p\mu}{\rm Pr}\)
  type(t_base), intent(in)    :: base  !! base and quadrature formula
  class(c_e),   intent(in)    :: e     !! element
  real(wp),     intent(in)    :: uuk(:,:)
  real(wp),     intent(in)    :: ggk(:,:,:)
  real(wp),     intent(inout) :: e_rhs(:,:)

  integer :: i, l
  real(wp) :: &
    uuh_k(size(uuk,1),base%m), ggh_k(d,d+1,base%m), &
    flux(d,size(uuk,1)), gradphi(d), j_it(d,d,base%m), detj(base%m)

   ! retrieve J^{-T} and det(J) at all the quadrature points
   call e%get_j_it( j_it , base%xq )
   call e%get_detj( detj , base%xq )

   ! compute the local solution at all the quadrature points
   uuh_k = 0.0_wp
   ggh_k = 0.0_wp
   do l=1,base%m
     do i=1,base%n
       uuh_k(:,l)   = uuh_k(:,l)   + uuk(:,i)*base%phi(i,l)
       ggh_k(:,:,l) = ggh_k(:,:,l) + ggk(:,:,i)*base%phi(i,l)
     enddo
   enddo

   ! evaluate the integral
   do l=1,base%m
     flux = euler_flux(uuh_k(:,l))                     &
        + viscous_flux(uuh_k(:,l),ggh_k(:,:,l),mu,alpha)
     do i=1,base%n
       gradphi  = matmul( j_it(:,:,l) , base%dphi(:,i,l) )
       e_rhs(:,i) = e_rhs(:,i) &
         + base%wq(l)*detj(l) * matmul( gradphi , flux )
     enddo
   enddo

 end subroutine element_integral

!-----------------------------------------------------------------------

 pure subroutine g_element_integral(e_rhs, d, base,e,uuk)
  integer,      intent(in)    :: d
  type(t_base), intent(in)    :: base
  class(c_e),   intent(in)    :: e
  real(wp),     intent(in)    :: uuk(:,:)
  real(wp),     intent(inout) :: e_rhs(:,:,:)

  integer :: i, l, j
  real(wp) :: uuh_k(size(uuk,1),base%m), psi(d+1,base%m), &
    j_it(d,d,base%m), detj(base%m), gradphi(d)

   ! retrieve J^{-T} and det(J) at all the quadrature points
   call e%get_j_it( j_it , base%xq )
   call e%get_detj( detj , base%xq )

   ! compute the local solution at all the quadrature points
   uuh_k = 0.0_wp
   do l=1,base%m
     do i=1,base%n
       uuh_k(:,l) = uuh_k(:,l) + uuk(:,i)*base%phi(i,l)
     enddo
   enddo

   ! compute velocity and temperature
   do l=1,base%m
     associate( v=>psi(1:d,l) , t=>psi(d+1,l) )
     associate( rho=>uuh_k(1,l) , rho_v=>uuh_k(2:1+d,l) , &
                  e=>uuh_k(d+2,l) )
       v = rho_v/rho
       t = 1.0_wp/cv*( e/rho - 0.5_wp*dot_product(v,v) )
     end associate
     end associate
   enddo

   do l=1,base%m
     do i=1,base%n
       gradphi  = matmul( j_it(:,:,l) , base%dphi(:,i,l) )
       do j=1,size(e_rhs,2)
         e_rhs(:,j,i) = e_rhs(:,j,i) &
           - base%wq(l)*detj(l) * psi(j,l) * gradphi
       enddo
     enddo
   enddo

 end subroutine g_element_integral

!-----------------------------------------------------------------------

 pure subroutine side_integral(s_rhs_l,s_rhs_r, d,mu,alpha, base,s, &
                               uuk_l,uuk_r , ggk_l,ggk_r)
  integer,      intent(in)    :: d
  real(wp),     intent(in)    :: mu    !! \(\mu\)
  real(wp),     intent(in)    :: alpha !! \(\frac{c_p\mu}{\rm Pr}\)
  type(t_base), intent(in)    :: base
  class(c_s),   intent(in)    :: s
  real(wp),     intent(in)    :: uuk_l(:,:)
  real(wp),     intent(in)    :: uuk_r(:,:)
  real(wp),     intent(in)    :: ggk_l(:,:,:)
  real(wp),     intent(in)    :: ggk_r(:,:,:)
  real(wp),     intent(inout) :: s_rhs_l(:,:)
  real(wp),     intent(inout) :: s_rhs_r(:,:)

   s_rhs_l = 0.0_wp
   s_rhs_r = 0.0_wp

 end subroutine side_integral

!-----------------------------------------------------------------------

 pure subroutine g_side_integral(s_rhs_l,s_rhs_r, d, base,s,uuk_l,uuk_r)
  integer,      intent(in)    :: d
  type(t_base), intent(in)    :: base
  class(c_s),   intent(in)    :: s
  real(wp),     intent(in)    :: uuk_l(:,:)
  real(wp),     intent(in)    :: uuk_r(:,:)
  real(wp),     intent(inout) :: s_rhs_l(:,:,:)
  real(wp),     intent(inout) :: s_rhs_r(:,:,:)

   ! TODO
   s_rhs_l = 0.0_wp
   s_rhs_r = 0.0_wp

 end subroutine g_side_integral

!-----------------------------------------------------------------------

 pure function euler_flux(uu) result(flux)
  real(wp), intent(in) :: uu(:)
  real(wp) :: flux( size(uu)-2 , size(uu) )

  integer :: d, i, j
  real(wp) :: v(size(uu)-2), p ! velocity, pressure

   d = size(uu)-2 ! space dimension

   associate( rho  => uu(1),       & 
              rhov => uu(1+1:1+d), &
              e    => uu(d+2)      )

   v = rhov/rho
   p = (gamma-1.0_wp)*(e-0.5_wp*dot_product(rhov,v))

   flux(:,1)   = rhov
   do j=1,d
     do i=1,d
       flux(i,1+j) = rhov(i)*v(j)
     enddo
     flux(j,1+j) = flux(j,1+j) + p ! diagonal contributions
   enddo
   flux(:,d+2) = v*(e+p) 

   end associate
  
 end function euler_flux

!-----------------------------------------------------------------------

 pure function viscous_flux(uu,gg,mu,alpha) result(flux)
  real(wp), intent(in) :: uu(:)   !! \(U\)
  real(wp), intent(in) :: gg(:,:) !! \(\T{G}\)
  real(wp), intent(in) :: mu      !! \(\mu\)
  real(wp), intent(in) :: alpha   !! \(\frac{c_p\mu}{\rm Pr}\)
  real(wp) :: flux( size(uu)-2 , size(uu) )

  integer :: d, i, j
  real(wp) :: v(size(uu)-2), div_v, tau(size(uu)-2,size(uu)-2), &
    q(size(uu)-2)

   d = size(uu)-2 ! space dimension
   associate( grad_v=>gg(:,1:d) , grad_t=>gg(:,d+1) )

   ! compute v
   v = uu(2:1+d)/uu(1)

   ! compute div(v)
   div_v = 0.0_wp
   do j=1,d
     div_v = div_v + grad_v(j,j)
   enddo

   ! compute tau
   do j=1,d
     do i=1,d
       tau(i,j) = -mu*( grad_v(i,j)+grad_v(j,i) )
     enddo
     ! diagonal contributions
     tau(j,j) = tau(j,j) + 2.0_wp/3.0_wp*mu*div_v
   enddo

   ! compute q
   q = -alpha*grad_t

   ! compute the fluxes
   flux(:, 1   ) = 0.0_wp
   flux(:,2:1+d) = tau
   flux(:, 2+d ) = q + matmul( tau , v )

   end associate

 end function viscous_flux

!-----------------------------------------------------------------------

 pure function euler_num_flux(n,uu_l,uu_r) result(f_hat_n)
  real(wp), intent(in) :: n(:)
  real(wp), intent(in) :: uu_l(:)
  real(wp), intent(in) :: uu_r(:)
  real(wp) :: f_hat_n(size(uu_l))

  integer :: d

  real(wp) :: v_l(size(n)), p_l, c_l ! vel. pressure, sound speed
  real(wp) :: v_r(size(n)), p_r, c_r
  real(wp) :: v_l_n, v_r_n           ! normal velocities
  real(wp) :: lam_max                ! maximum wavespeed
  real(wp) :: ef_l(size(n),size(uu_l)) ! Euer flux
  real(wp) :: ef_r(size(n),size(uu_l))

  d = size(n) ! space dimension

  associate( rho_l => uu_l(1),        rho_r => uu_r(1),       & 
            rhov_l => uu_l(1+1:1+d), rhov_r => uu_r(1+1:1+d), &
               e_l => uu_l(d+2),        e_r => uu_r(d+2)      )

  v_l = rhov_l/rho_l
  v_r = rhov_r/rho_r
  p_l = (gamma-1.0_wp)*(e_l-0.5_wp*dot_product(rhov_l,v_l))
  p_r = (gamma-1.0_wp)*(e_r-0.5_wp*dot_product(rhov_r,v_r))
  c_l = sqrt(gamma*p_l/rho_l)
  c_r = sqrt(gamma*p_r/rho_r)

  v_l_n = dot_product( v_l , n )
  v_r_n = dot_product( v_r , n )
  lam_max = max( abs(v_l_n)+c_l , abs(v_r_n)+c_r )

  end associate

  ef_l = euler_flux(uu_l)
  ef_r = euler_flux(uu_r)

  f_hat_n = 0.5_wp*matmul(n,ef_l+ef_r) - lam_max*(uu_r-uu_l)
 
 end function euler_num_flux

!-----------------------------------------------------------------------

 pure function viscous_num_flux(mu,alpha, n, uu_l,uu_r, gg_l,gg_r)  &
                                                         result(f_hat_n)
  real(wp), intent(in) :: mu, alpha
  real(wp), intent(in) :: n(:)
  real(wp), intent(in) :: uu_l(:)
  real(wp), intent(in) :: uu_r(:)
  real(wp), intent(in) :: gg_l(:,:)
  real(wp), intent(in) :: gg_r(:,:)
  real(wp) :: f_hat_n(size(uu_l))

  integer :: d
  real(wp) :: flux_l( size(uu_l)-2 , size(uu_l) )
  real(wp) :: flux_r( size(uu_l)-2 , size(uu_l) )

   d = size(uu_l)-2 ! spatial dimension

   flux_l = viscous_flux(uu_l,gg_l,mu,alpha)
   flux_r = viscous_flux(uu_r,gg_r,mu,alpha)

   f_hat_n = 0.5_wp*matmul(n , flux_l+flux_r)
   f_hat_n(2:d+1) = f_hat_n(2:d+1) +    mu*eta*(uu_l(2:d+1)-uu_r(2:d+1))
   f_hat_n( d+2 ) = f_hat_n( d+2 ) + alpha*eta*(uu_l( d+2 )-uu_r( d+2 ))

 end function viscous_num_flux

!-----------------------------------------------------------------------

end module mod_ns_local_int

