project: DG &mdash; 1D/2D Navier&ndash;Stokes equations
summary: DG discretization of the 1D/2D Navier&ndash;Stokes equations
author: Marco Restelli, Florian Hindenlang
email: mrestelli@gmail.com, hindenlang@gmail.com
website: http://www2.ipp.mpg.de/~marre
bitbucket: https://bitbucket.org/mrestelli
project_bitbucket: https://bitbucket.org/mrestelli/dg-cfd-intro
license: by
src_dir: ./../src
output_dir: ./../doc
exclude_dir: ./../doc
             ./../doc/src
mathjax_config: ./../ford-config/MathJax-latex-macros.js
predocmark: >
display: public
         protected
         private
source: false
graph: true
search: true
macro: TEST
       LOGIC=.true.

Solves the Navier&ndash;Stokes equation system
\begin{equation}
\partial_t U + \Div\V{F}(U) = 0 ,
\label{eq:Ucons-law}
\end{equation}
with
\[
U = \left[
\begin{array}{c}
\rho \\
\rho\V{v} \\
E
\end{array}
\right], \qquad
\V{F}(U) = \left[
\begin{array}{c}
\rho\V{v} \\
p\T{I} + \rho\V{v}\otimes\V{v} + \T{\tau} \\
(E+p)\V{v} + \T{\tau}\V{v} + \V{q} 
\end{array}
\right].
\]
Here, \(\rho\) is the density, \(\V{v}\) is the velocity, \(E\) is the
total energy \(p\) is the pressure, and the _viscous_ fluxes are
\begin{align*}
\T{\tau} & = -\mu\left( \Grad\V{v} + (\Grad\V{v})^T -
\frac{2}{3}\Div\V{v} \right) \\
\V{q} & = -\frac{\mu c_p}{\rm Pr}\Grad T,
\end{align*}
where \(T\) is the temperature, \(\mu\) is the dynamic viscosity and
\(c_p\) and \({\rm Pr}\) are the specific heat at constant pressure
and the Prandtl number, respectively. To close the system, we also
need the relation between \(E\) and \(T\) and the ideal gas law,
namely
\begin{equation}
E = c_v\rho T + \frac{1}{2}\rho v^2, \qquad
p = \rho R T,
\label{eq:ideal-gas-law}
\end{equation}
where \(c_v = \frac{c_p}{\gamma}\) and \(R = c_p-c_v\). Finally, the
specific internal energy \(\varepsilon=c_v T\) can be also introduced,
from which we have
\[
E = \rho\varepsilon + \frac{1}{2}\rho v^2,
\]
reflecting the fact that the total energy is the sum of the internal
and kinetic energy.
 
The DG spatial discretization of \eqref{eq:Ucons-law} is then
\begin{align}
\int_K \T{G}_h\varphi_h & = -\int_K \Psi_h\Grad\varphi_h +
\int_{\partial K} \widehat{\Psi} \varphi_h \V{n} 
\label{eq:NS-DG-1} \\
\int_K \partial_t U_h\varphi_h & = \int_K \T{F}(U_h,\T{G}_h) 
\cdot\Grad\varphi_h
- \int_{\partial K} \widehat{\T{F}_n} \varphi_h,
\label{eq:NS-DG-2}
\end{align}
with
\[
\Psi_h = \left[ \begin{array}{c}
\V{v}_h \\
T_h
\end{array} \right].
\]
The numerical fluxes in \eqref{eq:NS-DG-1} and \eqref{eq:NS-DG-2} are
defined as
\[
\widehat{\T{F}_n} = \widehat{\T{F}_n}^E + \widehat{\T{F}_n}^v,
\]
where \(\widehat{\T{F}_n}^E\) is any of the numerical fluxes used for
the Euler equation (Godunov, Lax&ndash;Friedrichs, ...) and the
viscous fluxes are
\begin{align*}
\widehat{\Psi} & = \frac{1}{2}(\Psi_h^L + \Psi_h^R ) \\
\widehat{\T{F}_n}^v & = \frac{1}{2}(\T{F}^L+\T{F}^R)\cdot\V{n}_e +
\alpha (\Psi^L-\Psi^R).
\end{align*}

### No-slip boundary conditions

No-slip boundary conditions represent a physical wall. The conditions
include:

- Dirichlet homogeneous condition for \(\rho\V{v}\)
- Neumann homogeneous conditions for \(E\)

The simplest way to include such conditions in the formulation is
prescribing directly the numerical fluxes at the boundary.

Concerning the inviscid fluxes, since the velocity vanishes, the only
remaining contribution is a momentum flux \(-p\V{n}\), where \(\V{n}\)
is the outward pointing unit normal. Hence
\begin{equation}
\widehat{\T{F}_n}^{E,b}(U) = \left[
\begin{array}{c}
0 \\
p \V{n} \\
0
\end{array}
\right].
\label{eq:euler-no-slip-flux}
\end{equation}

Concerning the viscous flux, we use homogeneous Dirichlet boundary
conditions for the velocity and homogeneous Neumann boundary
conditions for the temperature. Hence
\begin{equation}
\widehat{\Psi}^b = 
\left[
\begin{array}{c}
0 \\
T
\end{array}
\right], \qquad
\widehat{\T{F}_n}^{v,b} = \left[
\begin{array}{c}
0 \\
 \T{\tau}(\Grad\V{v}) \, \V{n} \\
0
\end{array}
\right].
\label{eq:visc-no-slip-flux}
\end{equation}

