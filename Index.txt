ode-1
* makefiles
* basic Fortran syntax:
 - modules
 - real kind parameters
 - subroutines
 - variables and parameters
 - IO
 - DO loops
 - first concepts about arrays

ode-2 
* unit testing
* more Fortran syntax:
 - arrays and multidimensional arrays
 - the ASSOCIATE construct
 - more about IO
 - first concepts about allocatable entities

ode-3
* more Fortran syntax:
 - derived types
 - functions
 - allocatable entities
 - abstract interfaces
* generic programming:
 - ODE solver for generic right-hand-side

ode-4
* more Fortran syntax:
 - abstract classes
 - type bound procedures
 - polymorphic variables
* more about unit testing:
 - set-up and tear-down helper subroutines
* object oriented programming:
 - a vector space abstract class
 - ODE solver for generic state and right-hand-side


