program rk_solve

 use mod_kinds, only: wp
 use mod_rk_ode, only: compute_rk_solution

 implicit none

 integer, parameter :: num_st = 10
 real(wp), parameter :: dt = 0.1_wp
 real(wp), parameter :: x_in = 1.0_wp

 real(wp) :: u(2,num_st+1) ! numerical solution

 ! the following variables are used to compute the exact solution
 integer :: n
 real(wp) :: t
 real(wp) :: u_ex(2,num_st+1)

  call compute_rk_solution(u , x_in,dt,num_st)

  write(*,'(a)') "Numerical solution:"
  write(*,'(a,*(e13.5))') " x =",u(1,:)
  write(*,'(a,*(e13.5))') " y =",u(2,:)

  ! Compute the exact solution
  do n=0,num_st
    t = real(n,wp)*dt
    u_ex(1,n+1) =         x_in*cos(2.0_wp*t)
    u_ex(2,n+1) = -2.0_wp*x_in*sin(2.0_wp*t)
  enddo

  write(*,'(a)') "Exact solution:"
  write(*,'(a,*(e13.5))') " x =",u_ex(1,:)
  write(*,'(a,*(e13.5))') " y =",u_ex(2,:)

  write(*,'(a,e13.5)') "Error norm:", maxval( abs( u_ex - u ) )

end program rk_solve
