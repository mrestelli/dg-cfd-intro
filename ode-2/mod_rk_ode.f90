! Runge-Kutta solution of the ODE
!
!   x' =    y
!   y' = -4*x
!
! with omega=2 and initial condition  x(t=0)=x0, y(t=0)=0 .
!
! The exact solution is 
!
!   x(t) =    x0 * cos(2*t)
!   y(t) = -2*x0 * sin(2*t)
!
! The RK method is defined for a general ODE
!
!   u' = f(u)
!
! as
!
!   w = u^n + 3/5*dt * f(u^n)
!   u^{n+1} = u^n + dt * f(w)
!
module mod_rk_ode

 use mod_kinds, only: wp

 implicit none

 public :: compute_rk_solution

 private

contains
 
 ! Solve the ODE from  t=0  to  tfinal=nsteps*dt
 !
 ! All the time levels are stored in the array u(:,:). The first
 ! indexis used to store the two components x,y and the second index
 ! is used to store the time step.
 !
 ! Since u includes also the initial condition, it needs (at least)
 ! nsteps+1 columns.
 pure subroutine compute_rk_solution(u , x0,dt,nsteps)
  real(wp), intent(in)  :: x0     ! initial condition
  real(wp), intent(in)  :: dt     ! time step
  integer,  intent(in)  :: nsteps ! number of steps; tfinal=nsteps*dt
  real(wp), intent(out) :: u(:,:) ! solution

  real(wp), parameter :: a = 3.0_wp/5.0_wp

  integer :: n
  real(wp) :: w(2) ! intermediate stage w

   ! initial condition
   u(1,1) = x0
   u(2,1) = 0.0_wp

   ! time loop
   do n=1,nsteps
     ! RK stage
     w(1) = u(1,n) + a*dt *          u(2,n)
     w(2) = u(2,n) + a*dt * (-4.0_wp*u(1,n))
     ! next time step
     u(1,n+1) = u(1,n) + dt *          w(2)
     u(2,n+1) = u(2,n) + dt * (-4.0_wp*w(1))
   enddo

   ! Some other equivalent implementations of the time loop

   ! Variation 1: using ASSOCIATE to create aliases
   !do n=1,nsteps
   !  associate(    x => u(1,n)   ,    y => u(2,n)   , &
   !               wx => w(1)     ,   wy => w(2)     , &
   !             xnew => u(1,n+1) , ynew => u(2,n+1) )
   !
   !  ! RK stage
   !  wx = x + a*dt *          y
   !  wy = y + a*dt * (-4.0_wp*x)
   !  ! next time step
   !  xnew = x + dt *          wy
   !  ynew = y + dt * (-4.0_wp*wx)
   !
   !  end associate
   !enddo

   ! Variation 2: array syntax
   !do n=1,nsteps
   !  ! RK stage
   !  w = u(:,n) + a*dt * [ u(2,n) , -4.0_wp*u(1,n) ]
   !  ! next time step
   !  u(:,n+1) = u(:,n) + dt * [ w(2) , -4.0_wp*w(1) ]
   !enddo

   ! Variation 3: combine variations 1 and 2
   !do n=1,nsteps
   !  associate( unow => u(:,n) , unew => u(:,n+1) )
   !
   !  ! RK stage
   !  w = unow + a*dt * [ unow(2) , -4.0_wp*unow(1) ]
   !  ! next time step
   !  unew = unow + dt * [ w(2) , -4.0_wp*w(1) ]
   !
   !  end associate
   !enddo

 end subroutine compute_rk_solution

end module mod_rk_ode
