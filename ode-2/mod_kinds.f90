module mod_kinds
 
 implicit none

 public :: wp

 private

 !----------------------------------------------------------------------
 ! Select here the working precision wp
 !
 ! single precision
 !integer, parameter :: wp = selected_real_kind(6,35)
 !
 ! double precision
 integer, parameter :: wp = selected_real_kind(15,307)
 !
 ! quadruple precision
 !integer, parameter :: wp = selected_real_kind(33,307)
 !----------------------------------------------------------------------

end module mod_kinds
