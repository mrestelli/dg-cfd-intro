1) run "make tests" and look at the generated files


2) run the tests, both in the standard and the "verbose" mode:
./pFUnit-tests
./pFUnit-tests -v


3) generate the tests step by step (run "make clean first!):
make mod_rk_ode_tests.F90
make mod_rk_ode_tests.o
make pFUnit-tests


4) modify mod_rk_ode.f90 introducing some error; for instance, swap x
and y at some place, or n and n+1, or modify setting the initial
condition. Recompile and repeat the tests.


5) remove the tolerance from the test on the convergence rate and
repeat it


6) look at all the possible implementations provided in
COMPUTE_RK_SOLUTION in mod_rk_ode.f90. Try uncommenting each of them,
then compile and run the code. The results should not change, since
all the options are equivalent. Just choose the one that seems more
readable to you.


7) eliminate the computation of the exact solution in rk_solve.f90,
then change the declaration of U using less columns than NUM_ST+1.
Recompile and run the code. The code should compile and a runtime
error message should point out that an index is exceeding the array
size. Remove the debug options from FFLAGS in the Makefile, make
clean, recompile and run again the code. This time a much less
informative message should be printed.

Notice: debug flags are a great help to find errors in the code! The
cost is that the code is slower, so you might want to remove them once
you are confident that the code is correct.


8) as in the previous point, but this time put more columns in U than
NUM_ST+1. Run the code. There are no errors, but the last columns of U
have not been defined and contain meaningless values.


9) modify COMPUTE_RK_SOLUTION in mod_rk_ode.f90 so that the number of
time steps is the minimum between NSTEPS and the number of columns in
U. Hence, if U is too small, the subroutine should compute only the
required values and then return without any error. Add a test in
mod_rk_ode_tests.pf to verify your implementation (choose a good name
for this new test!).

Note: to get the number of elements of an array V use SIZE(V); to get
the size along a dimension K use SIZE(V,K). For instance:
  INTEGER :: V(3,5)
  SIZE(V)   -> returns 3*5=15
  SIZE(V,1) -> returns 3
  SIZE(V,2) -> returns 5


10) The test COMPUTE_RK_SOLUTION__ERROR_IS_FIRST_ORDER_IN_DT in
mod_rk_ode_tests.pf introduces two new elements of Fortran syntax:
contained subroutines and ALLOCATABLE arrays.


11) modify rk_solve.f90 to read the number of steps NUM_ST from the
terminal, using
READ(*,*) NUM_ST

Since NUM_ST is not a constant anymore and it is not know during the
compilation, it can not be used as array dimension. Hence, use
allocatable arrays for u and u_ex:

REAL(WP), ALLOCATABLE :: u(:,:)
! read NUM_ST
ALLOCATE( u(2,NUM_ST) )
! solve the ODE
DEALLOCATE( u )

Remark 1: no changes required in COMPUTE_RK_SOLUTION: the assumed
shape dummy arguments adjust automatically to the actual argument.

Remark 2: it is not possible to mix explicit and allocatable shapes: 
REAL(WP), ALLOCATABLE :: u(2,:) ! -> error

