module mod_euler_ode

 use mod_kinds, only: wp

 use mod_ode_state, only: c_ode_state

 use mod_ode_solver, only: c_ode

 use mod_linalg, only: invmat_chol

 use mod_grid, only: t_grid, c_s, c_e

 use mod_base, only: t_base

 use mod_euler_state, only: t_euler_state

 implicit none

 public :: &
   t_euler_ode, euler_ode_setup, euler_ode_clean, gamma, &
   element_mass_matrix

 private

 !> Compute the rhs resulting from the DG discretization of the Euler
 !! equation.
 type, extends(c_ode) :: t_euler_ode
  integer      :: d     !! space dimension
  type(t_grid) :: grid
  type(t_base) :: base
  !> \(\left(M^K\right)^{-1}\) for all the elements
  real(wp), allocatable :: imass_matrices(:,:,:)
 contains
  procedure, pass(ode) :: compute_rhs => compute_euler_rhs
  !> generate a state vector for this ODE
  procedure, pass(ode) :: new_euler_state
 end type t_euler_ode

 real(wp) :: gamma = 1.4_wp

contains

!-----------------------------------------------------------------------

 subroutine euler_ode_setup(ode, d,base,grid)
  integer,           intent(in) :: d ! space dimension
  type(t_base),      intent(in) :: base
  type(t_grid),      intent(in) :: grid
  type(t_euler_ode), intent(out) :: ode

  integer :: ie
  real(wp), allocatable :: m(:,:) ! work array for mass matrix

   ode%d = d

   ode%base = base

   ode%grid = grid

   ! precompute the mass matrices
   allocate(m(ode%base%n,ode%base%n))
   allocate( ode%imass_matrices(ode%base%n,ode%base%n,ode%grid%ne) )
   do ie=1,ode%grid%ne
     ! mass matrix
     call element_mass_matrix( m, ode%base, ode%grid%e(ie) )
     ! invert the mass matrix
     call invmat_chol( m , ode%imass_matrices(:,:,ie) )
   enddo
   deallocate(m)

 end subroutine euler_ode_setup

 subroutine euler_ode_clean(ode)
  type(t_euler_ode), intent(inout) :: ode

   deallocate( ode%imass_matrices )
   call ode%base%clean()
   call ode%grid%clean()

 end subroutine euler_ode_clean

 subroutine new_euler_state(y, ode)
  class(t_euler_ode),   intent(in)  :: ode
  class(t_euler_state), intent(out) :: y

   allocate( y%uu(ode%d+2,ode%base%n,ode%grid%ne) )

 end subroutine new_euler_state

!-----------------------------------------------------------------------

 subroutine compute_euler_rhs(rhs,ode,y)
  class(t_euler_ode), intent(in) :: ode
  class(c_ode_state), intent(in)    :: y
  class(c_ode_state), intent(inout) :: rhs

  integer :: ie, is, e_l,e_r, k

   select type(y);   type is(t_euler_state)
   select type(rhs); type is(t_euler_state)

   ! initialize rhs
   rhs%uu = 0.0_wp

   do ie=1,ode%grid%ne
     associate( uuk => y%uu(:,:,ie) )
     call element_integral(rhs%uu(:,:,ie), ode%d,      &
                           ode%base,ode%grid%e(ie), uuk)
     end associate
   enddo

   ! side fluxes
   do is=1,ode%grid%ns
     e_l = ode%grid%s(is)%e_l
     e_r = ode%grid%s(is)%e_r
     call side_integral( rhs%uu(:,:,e_l) , rhs%uu(:,:,e_r) , ode%d , &
              ode%base, ode%grid%s(is), y%uu(:,:,e_l), y%uu(:,:,e_r) )
   enddo

   ! include the mass matrix
   do ie=1,ode%grid%ne
     do k=1,size(rhs%uu,1) ! loop over the variables
       rhs%uu(k,:,ie) = &
         matmul(ode%imass_matrices(:,:,ie),rhs%uu(k,:,ie))
     enddo
   enddo

   end select
   end select

 end subroutine compute_euler_rhs

!-----------------------------------------------------------------------

 pure subroutine element_mass_matrix(m, base,e)
  type(t_base), intent(in)  :: base  !! base and quadrature formula
  class(c_e),   intent(in)  :: e     !! element \(K\)
  real(wp),     intent(out) :: m(:,:)

 end subroutine element_mass_matrix

!-----------------------------------------------------------------------

 pure subroutine element_integral(e_rhs, d, base,e,uuk)
  integer,      intent(in)    :: d     !! space dimension
  type(t_base), intent(in)    :: base  !! base and quadrature formula
  class(c_e),   intent(in)    :: e     !! element
  !> \(a^K_i, \quad i=1,\ldots,n\)
  real(wp),     intent(in)    :: uuk(:,:)
  real(wp),     intent(inout) :: e_rhs(:,:)

 end subroutine element_integral

!-----------------------------------------------------------------------

 !> Compute the side integrals
 pure subroutine side_integral(s_rhs_l,s_rhs_r, d, base,s, uuk_l,uuk_r)
  !> space dimesion
  integer,      intent(in)    :: d
  !> base and quadrature formula
  type(t_base), intent(in)    :: base
  !> side \( e \)
  class(c_s),   intent(in)    :: s
  !> \(U^{K_L}_i, \quad i=1,\ldots,n\)
  real(wp),     intent(in)    :: uuk_l(:,:)
  !> \(U^{K_R}_i, \quad i=1,\ldots,n\)
  real(wp),     intent(in)    :: uuk_r(:,:)
  real(wp),     intent(inout) :: s_rhs_l(:,:)
  real(wp),     intent(inout) :: s_rhs_r(:,:)

 end subroutine side_integral

!-----------------------------------------------------------------------

 pure function euler_flux(uu) result(flux)
  real(wp), intent(in) :: uu(:)
  real(wp) :: flux( size(uu)-2 , size(uu) )

 end function euler_flux

!-----------------------------------------------------------------------

 pure function numerical_flux(n,uu_l,uu_r) result(f_hat_n)
  real(wp), intent(in) :: n(:)
  real(wp), intent(in) :: uu_l(:)
  real(wp), intent(in) :: uu_r(:)
  real(wp) :: f_hat_n(size(uu_l))

 end function numerical_flux

!-----------------------------------------------------------------------

end module mod_euler_ode
