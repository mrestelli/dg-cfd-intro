program solve_euler

 use mod_kinds, only: wp

 use mod_rk, only: t_explicit_euler, t_heun_method

 use mod_base, only: t_base, new_1d_base, new_2d_base

 use mod_grid, only: t_grid
 use mod_1d_grid, only: new_1d_grid
 use mod_2d_grid, only: new_2d_grid

 use mod_euler_state, only: t_euler_state
 use mod_euler_ode, only: &
   t_euler_ode, euler_ode_setup, euler_ode_clean, gamma

 implicit none

 integer, parameter :: d = 2
 ! Discretization parameters
 !integer,  parameter :: n_elem(1) = 50
 !real(wp), parameter :: x_left(1) = 0.0_wp, x_right(1) = 2.0_wp
 integer,  parameter :: n_elem(2) = [40,40]
 real(wp), parameter :: x_left(2) = 0.0_wp, x_right(2) = 2.0_wp
 integer,  parameter :: n_step = 500
 real(wp), parameter :: dt = 0.001_wp
 integer :: degree

 ! Discretization
 type(t_base) :: base
 type(t_grid) :: grid

 ! ODE
 integer :: n
 real(wp) :: t
 type(t_euler_state) :: ynow, ynew
 type(t_euler_ode)   :: ode

 ! Time integrator
 type(t_heun_method) :: time_integrator

 ! Output file
 integer :: ie
 integer :: file_unit
 real(wp), allocatable :: xdata(:,:,:)

  write(*,*) "Choose the polynomial degree: 0 or 1"
  read(*,*) degree

  ! Set-up the problem

  ! grid
  select case(d)
   case(1) ! one-dimensional grid and base required
    call new_1d_base(base,degree)
    call new_1d_grid(grid,n_elem,x_left,x_right)
   case(2) ! two-dimensional
    call new_2d_base(base,degree)
    call new_2d_grid(grid,n_elem,x_left,x_right)
   case default
    error stop 'In "solve_euler.f90": not implemented'
  end select

  ! define the ODE
  call euler_ode_setup( ode, d, base, grid )
  ! the grid and the base have been copied into ode: we can free them
  call base%clean()
  call grid%clean()

  ! prepare the state vectors
  call ode%new_euler_state( ynow )
  call ode%new_euler_state( ynew )

  ! initial condition and visualization
  associate(  n_coords => size(ode%base%x_dofs,1) , &
             n_out_pts => size(ode%base%x_dofs,2) )
  allocate( xdata(n_coords,n_out_pts,ode%grid%ne) )
  do ie=1,ode%grid%ne
    call ode%grid%e(ie)%map( xdata(:,:,ie) , ode%base%x_dofs )
    ynow%uu(:,:,ie) = initial_condition( xdata(:,:,ie) )
  enddo
  end associate

  ! Prepare the output file
  call output_file_setup()

  ! Integrate the system
  call time_integrator%setup(ynow)
  do n=1,n_step
    t = real(n,wp)*dt

    ! Compute the time step
    call time_integrator%compute_step(ynew , ode,dt,ynow)
    call ynow%copy( ynew )

    ! Write the solution
    call output_file_write_data( t=t , uu=ynow%uu(d+2,:,:) )

  enddo

  ! Finalize the output file
  call output_file_finalize()

  write(*,*) "Done, results written in 'solve_euler.out'."

  ! Clean-up
  deallocate( ynow%uu )
  deallocate( ynew%uu )
  deallocate( xdata )
  call euler_ode_clean(ode)
  call time_integrator%clean()

contains

 pure function initial_condition(x) result(uu0)
  real(wp), intent(in) :: x(:,:)
  real(wp) :: uu0(2+d,size(x,2))

  integer :: i
  real(wp) :: p(size(x,2)) ! pressure

   do i=1,size(x,2)
     associate( r => sqrt(sum((x(:,i)-1.0_wp)**2)) )
     if( r .le. 0.3_wp ) then
       p(i) = 0.001_wp*(1.0_wp - r/0.3_wp) + 1.0_wp
     else
       p(i) = 1.0_wp
     endif
     end associate
   enddo

   uu0(1,:)       = 1.0_wp
   uu0(1+1:1+d,:) = 0.0_wp
   uu0(d+2,:)     = p/(gamma-1.0_wp) /1.25

 end function initial_condition

 subroutine output_file_setup()
  integer :: ie2
  real(wp), allocatable :: xdata_tmp(:,:,:)
   ! Open a file and connect it to a new unit
   open(newunit=file_unit, file="solve_euler.out", &
        status='replace',action='write')
   ! Write the file header
   write(file_unit,'(a)') &
     'title  = "Scalar advection equation"'
   write(file_unit,'(a)') &
     'xlabel = "t"'
   write(file_unit,'(a)') &
     'ylabel = ""'
   write(file_unit,'(a)') &
     'yNames = "numerical solution"'
   if(d.ne.1) then
     if(degree.eq.0) then
       write(file_unit,'(a,i0)') &
         'n_x = ', n_elem(1)
       write(file_unit,'(a,i0)') &
         'n_y = ', n_elem(2)
     else
       write(file_unit,'(a,i0)') &
         'n_x = ', 2*n_elem(1)
       write(file_unit,'(a,i0)') &
         'n_y = ', 2*n_elem(d)
       allocate( xdata_tmp( 2 , 2*n_elem(1) , 2*n_elem(d) ) )
       do ie2=1,n_elem(2)
         associate( row => (ie2-1)*n_elem(1) )
         xdata_tmp(:,1::2,(ie2-1)*2+1) = xdata(:,1,row+1:row+n_elem(1))
         xdata_tmp(:,2::2,(ie2-1)*2+1) = xdata(:,2,row+1:row+n_elem(1))
         xdata_tmp(:,1::2,(ie2-1)*2+2) = xdata(:,4,row+1:row+n_elem(1))
         xdata_tmp(:,2::2,(ie2-1)*2+2) = xdata(:,3,row+1:row+n_elem(1))
         end associate
       enddo
     endif
   endif
   if(.not.(allocated(xdata_tmp))) then
     write(file_unit,'(a,*(e23.15,:,","))') &
       'xdata  = ', xdata
   else
     write(file_unit,'(a,*(e23.15,:,","))') &
       'xdata  = ', xdata_tmp
     deallocate(xdata_tmp)
   endif
 end subroutine output_file_setup

 subroutine output_file_finalize()
   ! Close the file
   close(unit=file_unit)
 end subroutine output_file_finalize

 subroutine output_file_write_data(t,uu)
  real(wp), intent(in) :: t
  real(wp), intent(in) :: uu(:,:)

  integer :: ie2
  real(wp), allocatable :: uu_tmp(:,:)

   if((d.eq.2).and.(degree.eq.1)) then
     allocate( uu_tmp( 2*n_elem(1) , 2*n_elem(d) ) )
     do ie2=1,n_elem(2)
       associate( row => (ie2-1)*n_elem(1) )
       uu_tmp(1::2,(ie2-1)*2+1) = uu(1,row+1:row+n_elem(1))
       uu_tmp(2::2,(ie2-1)*2+1) = uu(2,row+1:row+n_elem(1))
       uu_tmp(1::2,(ie2-1)*2+2) = uu(4,row+1:row+n_elem(1))
       uu_tmp(2::2,(ie2-1)*2+2) = uu(3,row+1:row+n_elem(1))
       end associate
     enddo
   endif

   write(file_unit,'(a,e23.15)') &
     'time   = ',t
   
   if(.not.(allocated(uu_tmp))) then
     write(file_unit,'(a,*(e23.15,:,","))') &
       '"numerical solution" = ',uu
   else
     write(file_unit,'(a,*(e23.15,:,","))') &
       '"numerical solution" = ',uu_tmp
     deallocate(uu_tmp)
   endif

 end subroutine output_file_write_data

end program solve_euler

