# Object files: modules and main program ------------------------------

# object files: modules
OBJ_MOD=mod_kinds.o \
  mod_ode_state.o \
  mod_ode_solver.o \
  mod_rk.o \
  mod_linalg.o \
  mod_grid.o \
  mod_1d_grid.o \
  mod_2d_grid.o \
  mod_base.o \
  mod_scalar_adv_state.o \
  mod_scalar_adv_ode.o \
  mod_euler_state.o \
  mod_euler_ode.o

# object files: main program
MAIN=solve_scalar_adv solve_euler

# Main targets and general rules --------------------------------------

# Main target: the executable programs
all: $(MAIN)

solve_scalar_adv: $(OBJ_MOD) solve_scalar_adv.o
	$(RECIPE)

solve_euler: $(OBJ_MOD) solve_euler.o
	$(RECIPE)

# General rule to compile Fortran source files
%.o: %.f90
	$(RECIPE)

# Clean-up
.PHONY: clean
clean:
	$(CLEAN_RECIPE)

# Dependencies --------------------------------------------------------

mod_ode_state.o: \
  mod_kinds.o

mod_ode_solver.o: \
  mod_kinds.o \
  mod_ode_state.o

mod_rk.o: \
  mod_kinds.o \
  mod_ode_state.o \
  mod_ode_solver.o

mod_linalg.o: \
  mod_kinds.o

mod_grid.o: \
  mod_kinds.o

mod_1d_grid.o: \
  mod_kinds.o \
  mod_grid.o

mod_2d_grid.o: \
  mod_kinds.o \
  mod_grid.o

mod_base.o: \
  mod_kinds.o

mod_scalar_adv_state.o: \
  mod_kinds.o \
  mod_ode_state.o

mod_scalar_adv_ode.o: \
  mod_kinds.o \
  mod_ode_state.o \
  mod_ode_solver.o \
  mod_linalg.o \
  mod_grid.o \
  mod_base.o \
  mod_scalar_adv_state.o

solve_scalar_adv.o: \
  mod_kinds.o \
  mod_rk.o \
  mod_base.o \
  mod_grid.o \
  mod_1d_grid.o \
  mod_2d_grid.o \
  mod_scalar_adv_state.o \
  mod_scalar_adv_ode.o

mod_euler_state.o: \
  mod_kinds.o \
  mod_ode_state.o

mod_euler_ode.o: \
  mod_kinds.o \
  mod_ode_state.o \
  mod_ode_solver.o \
  mod_linalg.o \
  mod_grid.o \
  mod_base.o \
  mod_euler_state.o

solve_euler.o: \
  mod_kinds.o \
  mod_rk.o \
  mod_base.o \
  mod_grid.o \
  mod_1d_grid.o \
  mod_2d_grid.o \
  mod_euler_state.o \
  mod_euler_ode.o

# Makefile logic: dispatching and doing real work ---------------------

# Define the recipe: 
# 1) during the dispatch phase: cd to the correct directory and make
# 2) during the make phase: compile, link or clean
ifeq (DISPATCH,$(PHASE))

# For each target, specify the working directory; then use the general
# rule from the main Makefile
$(MAIN): WDIR=bin
%.o:     WDIR=build

# Clean-up
CLEAN_RECIPE = \
  +$(MAKE) -C build -f $(ROOTDIR)/Makefile $@ && \
   $(MAKE) -C bin   -f $(ROOTDIR)/Makefile $@
	
else

# For each target, provide the build rule
$(MAIN): RECIPE= $(LD) $(LDFLAGS) $^ -o $@ 

%.o: RECIPE=$(FC) $(FFLAGS) -c $< -o $@

ifeq (build,$(strip $(notdir $(CURDIR))))
  CLEAN_RECIPE = $(RM) *.o *.mod
else ifeq (bin,$(strip $(notdir $(CURDIR))))
  CLEAN_RECIPE = $(RM) $(MAIN)
endif

endif

