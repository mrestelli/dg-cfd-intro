!> Abstract class for a finite element grid.
module mod_grid

 use mod_kinds, only: wp
 
 implicit none

 public :: c_s, c_e, t_grid

 private

 !> Abstract representation for sides \(e\in\mathcal{E}_h\)
 type, abstract :: c_s
  integer :: e_l      !! index of \(K_L(e)\)
  integer :: e_r      !! index of \(K_R(e)\)
  integer :: iloc_l   !! local side index on \(\partial K_L(e)\)
  integer :: iloc_r   !! local side index on \(\partial K_L(e)\)
  integer :: orient_l !! side-element orientation: \(\pm1\) (left)
  integer :: orient_r !! side-element orientation: \(\pm1\) (right)
 contains
  !> Side normal at the requested points from \(K_L(e)\) to \(K_R(e)\)
  procedure(i_get_n),     deferred, pass(s) :: get_n
  !> Jacobian determinant for the side coordinate map
  procedure(i_get_sdetj), deferred, pass(s) :: get_sdetj
 end type c_s

 !> Abstract representation for elements \(K\in\mathcal{T}_h\)
 type, abstract :: c_e
  !> side indexes for the sides \(e\in\partial K\)
  integer, allocatable :: s(:)
 contains
  !> Jacobian matrix at the requested points
  !! \(J_K^{-T}=
  !! \left[\left.\frac{d\V{x}}{d\tilde{\V{x}}}\right|_K\right]^{-T}\)
  procedure(i_get_j_it), deferred, pass(e) :: get_j_it
  !> Jacobian determinant at the requested points \(det(J_K)\)
  procedure(i_get_detj), deferred, pass(e) :: get_detj
  !> \(\mathcal{F}_K: \tilde{K}\to K\) (only used for visualization)
  procedure(i_map),      deferred, pass(e) :: map
 end type c_e

 !> Finite element grid \(\mathcal{T}_h\)
 type :: t_grid
  integer :: ns                   !! number of sides
  integer :: ne                   !! number of elements
  class(c_s), allocatable :: s(:) !! sides
  class(c_e), allocatable :: e(:) !! elements
 contains
  procedure, pass(grid) :: clean => clean_grid
 end type t_grid

 abstract interface
  !> Side normal at arbitrary points \(\tilde{\V{x}}_i\)
  !!
  !! Given some points \(\tilde{\V{x}}_i\) on the side, passed as
  !! columns of the input argument \(\code{x(:,i)}\), return the
  !! corresponding normals as columns of \(\code{n(:,i)}\).
  pure subroutine i_get_n(n, s,x)
   import :: wp, c_s
   implicit none
   class(c_s), intent(in)  :: s
   real(wp),   intent(in)  :: x(:,:)
   real(wp),   intent(out) :: n(:,:)
  end subroutine i_get_n
 end interface

 abstract interface
  !> Jacobian determinant at arbitrary points \(\tilde{\V{x}}_i\) on
  !! the side
  !!
  !! Given some points \(\tilde{\V{x}}_i\) on the side, passed as
  !! columns of the input argument \(\code{x(:,i)}\), return the
  !! corresponding Jacobian determinants in \(\code{detj(i)}\).
  pure subroutine i_get_sdetj(detj, s,x)
   import :: wp, c_s
   implicit none
   class(c_s), intent(in)  :: s
   real(wp),   intent(in)  :: x(:,:)
   real(wp),   intent(out) :: detj(:)
  end subroutine i_get_sdetj
 end interface

 abstract interface
  !> Jacobian matrix at arbitrary points \(\tilde{\V{x}}_i\) on the
  !! element
  !!
  !! Given some points \(\tilde{\V{x}}_i\) on the element, passed as
  !! columns of the input argument \(\code{x(:,i)}\), return the
  !! corresponding Jacobian matrices in \(\code{j(:,:,i)}\).
  pure subroutine i_get_j_it(j_it, e,x)
   import :: wp, c_e
   implicit none
   class(c_e), intent(in)  :: e
   real(wp),   intent(in)  :: x(:,:)
   real(wp),   intent(out) :: j_it(:,:,:)
  end subroutine i_get_j_it
 end interface

 abstract interface
  !> Jacobian determinant at arbitrary points \(\tilde{\V{x}}_i\) on
  !! the element
  !!
  !! Given some points \(\tilde{\V{x}}_i\) on the element, passed as
  !! columns of the input argument \(\code{x(:,i)}\), return the
  !! corresponding Jacobian determinant in \(\code{detj(i)}\).
  pure subroutine i_get_detj(detj, e,x)
   import :: wp, c_e
   implicit none
   class(c_e), intent(in)  :: e
   real(wp),   intent(in)  :: x(:,:)
   real(wp),   intent(out) :: detj(:)
  end subroutine i_get_detj
 end interface

 abstract interface
  pure subroutine i_map(x, e,x_tilde)
   import :: wp, c_e
   implicit none
   class(c_e), intent(in)  :: e
   real(wp),   intent(in)  :: x_tilde(:,:)
   real(wp),   intent(out) :: x(:,:)
  end subroutine i_map
 end interface

contains

 subroutine clean_grid(grid)
  class(t_grid), intent(inout) :: grid

   deallocate( grid%s )
   deallocate( grid%e )
   grid%ns = -1
   grid%ne = -1

 end subroutine clean_grid

end module mod_grid
