module mod_scalar_adv_ode

 use mod_kinds, only: wp

 use mod_ode_state, only: c_ode_state

 use mod_ode_solver, only: c_ode

 use mod_linalg, only: invmat_chol

 use mod_grid, only: t_grid, c_s, c_e

 use mod_base, only: t_base

 use mod_scalar_adv_state, only: t_scalar_adv_state

 implicit none

 public :: &
   t_scalar_adv_ode, scalar_adv_ode_setup, scalar_adv_ode_clean

 private

 !> Compute the rhs resulting from the DG discretization of
 !! \[
 !!  \partial_t a +\partial_{\V{x}}( \V{u}a ) = 0
 !! \]
 !! where u is constant.
 type, extends(c_ode) :: t_scalar_adv_ode
  real(wp), allocatable :: u(:) !< advection coefficient
  type(t_grid) :: grid
  type(t_base) :: base
  !> \(\left(M^K\right)^{-1}\) for all the elements
  real(wp), allocatable :: imass_matrices(:,:,:)
 contains
  procedure, pass(ode) :: compute_rhs => compute_scalar_adv_rhs
  !> generate a state vector for this ODE
  procedure, pass(ode) :: new_scalar_adv_state
 end type t_scalar_adv_ode

contains

!-----------------------------------------------------------------------

 subroutine scalar_adv_ode_setup(ode, base,grid, u)
  type(t_base),           intent(in) :: base
  type(t_grid),           intent(in) :: grid
  real(wp),               intent(in) :: u(:)
  type(t_scalar_adv_ode), intent(out) :: ode

  integer :: ie
  real(wp), allocatable :: m(:,:) ! work array for mass matrix

   ode%base = base

   ode%grid = grid

   allocate( ode%u(size(u)) )
   ode%u = u

   ! precompute the mass matrices
   allocate(m(ode%base%n,ode%base%n))
   allocate( ode%imass_matrices(ode%base%n,ode%base%n,ode%grid%ne) )
   do ie=1,ode%grid%ne
     ! mass matrix
     call element_mass_matrix( m, ode%base, ode%grid%e(ie) )
     ! invert the mass matrix
     call invmat_chol( m , ode%imass_matrices(:,:,ie) )
   enddo
   deallocate(m)

 end subroutine scalar_adv_ode_setup

 subroutine scalar_adv_ode_clean(ode)
  type(t_scalar_adv_ode), intent(inout) :: ode

   deallocate( ode%imass_matrices )
   call ode%base%clean()
   call ode%grid%clean()

 end subroutine scalar_adv_ode_clean

 subroutine new_scalar_adv_state(y, ode)
  class(t_scalar_adv_ode),   intent(in)  :: ode
  class(t_scalar_adv_state), intent(out) :: y

   allocate( y%aa(ode%base%n,ode%grid%ne) )

 end subroutine new_scalar_adv_state

!-----------------------------------------------------------------------

 subroutine compute_scalar_adv_rhs(rhs,ode,y)
  class(t_scalar_adv_ode), intent(in) :: ode
  class(c_ode_state), intent(in)    :: y
  class(c_ode_state), intent(inout) :: rhs

  integer :: ie, is, e_l,e_r

   select type(y);   type is(t_scalar_adv_state)
   select type(rhs); type is(t_scalar_adv_state)

   ! initialize rhs
   rhs%aa = 0.0_wp

   do ie=1,ode%grid%ne
     associate( ak => y%aa(:,ie) )
     call element_integral(rhs%aa(:,ie), ode%u,       &
                           ode%base,ode%grid%e(ie), ak)
     end associate
   enddo

   ! side fluxes
   do is=1,ode%grid%ns
     e_l = ode%grid%s(is)%e_l
     e_r = ode%grid%s(is)%e_r
     call side_integral( rhs%aa(:,e_l) , rhs%aa(:,e_r) , ode%u , &
              ode%base, ode%grid%s(is), y%aa(:,e_l), y%aa(:,e_r) )
   enddo

   ! include the mass matrix
   do ie=1,ode%grid%ne
     rhs%aa(:,ie) = matmul(ode%imass_matrices(:,:,ie) , rhs%aa(:,ie))
   enddo

   end select
   end select

 end subroutine compute_scalar_adv_rhs

!-----------------------------------------------------------------------

 !> Compute the element integral
 !! \[
 !!  M^K_{ij} = \int_K \varphi_i^K \varphi_j^K
 !! \]
 !! for every element \(K\in\mathcal{T}_h\).
 !!
 !! The computation uses
 !! \begin{align*}
 !! \int_K \varphi_i^K \varphi_j^K
 !! & \approx 
 !! \sum_{l=1}^m \tilde{w}^Q_l \left[ det(J_K)\, \varphi_i
 !! \varphi_j \right](\tilde{\V{x}}^Q_l).
 !! \end{align*}
 pure subroutine element_mass_matrix(m, base,e)
  type(t_base), intent(in)  :: base  !! base and quadrature formula
  class(c_e),   intent(in)  :: e     !! element \(K\)
  real(wp),     intent(out) :: m(:,:)

  integer :: i, j, l
  real(wp) :: detj(base%m)

   ! retrieve det(J) at all the quadrature points
   call e%get_detj( detj , base%xq )

   ! evaluate the integral
   m = 0.0_wp
   do l=1,base%m
     do j=1,base%n
       do i=1,base%n
         m(i,j) = m(i,j) &
                 + base%wq(l)*detj(l) * base%phi(i,l)*base%phi(j,l)
       enddo
     enddo
   enddo

 end subroutine element_mass_matrix

!-----------------------------------------------------------------------

 !> Compute the element integral
 !! \[
 !!  \int_K \V{u}a_h|_K  \cdot\Grad\varphi_i^K,
 !! \]
 !! for every basis function \(\varphi_i^K\in V_h(K)\) and for a given
 !! element \(K\in\mathcal{T}_h\).
 !!
 !! The computation uses
 !! \begin{align*}
 !! \int_K \V{u}a_h|_K \cdot \Grad\varphi_i^K 
 !! & \approx 
 !! \sum_{l=1}^m \tilde{w}^Q_l \left[ det(J_K)\, \V{u}a_h 
 !! \cdot \Grad\varphi_i \right](\tilde{\V{x}}^Q_l)
 !! \end{align*}
 !! and the fact that
 !! \[
 !!  \Grad\varphi_i = J_K^{-T}\tilde{\Grad}\tilde{\varphi}_i.
 !! \]
 !!
 !! The element integral is *added* to ```e_rhs```.
 pure subroutine element_integral(e_rhs, u, base,e,ak)
  real(wp),     intent(in)    :: u(:)  !! advection coefficient
  type(t_base), intent(in)    :: base  !! base and quadrature formula
  class(c_e),   intent(in)    :: e     !! element
  !> \(a^K_i, \quad i=1,\ldots,n\)
  real(wp),     intent(in)    :: ak(:)
  real(wp),     intent(inout) :: e_rhs(:)

  integer :: i, l
  real(wp) :: ah_k(base%m), flux(size(u)), gradphi(size(u)), &
    j_it(size(u),size(u),base%m), detj(base%m)

   ! retrieve J^{-T} and det(J) at all the quadrature points
   call e%get_j_it( j_it , base%xq )
   call e%get_detj( detj , base%xq )

   ! compute the local solution at all the quadrature points
   ah_k = 0.0_wp
   do i=1,base%n
     ah_k = ah_k + ak(i)*base%phi(i,:)
   enddo

   ! evaluate the integral
   do l=1,base%m
     flux = u*ah_k(l)
     do i=1,base%n
       gradphi  = matmul( j_it(:,:,l) , base%dphi(:,i,l) )
       e_rhs(i) = e_rhs(i) &
         + base%wq(l)*detj(l) * dot_product( flux , gradphi )
     enddo
   enddo

 end subroutine element_integral

!-----------------------------------------------------------------------

 !> Compute the side integrals
 !! \[
 !!  -\int_{e} \widehat{\V{u}a} \cdot\sigma_{K,e}\V{n}_e \varphi_i^K,
 !! \]
 !! for every basis function \(\varphi_i^K\in V_h(K)\), for \(
 !! K=K_L(e),K_R(e) \) and for a given side \(e\in\mathcal{E}_h\).
 !!
 !! The side integral is computed as
 !! \begin{align*}
 !!  -\int_{e} \widehat{\V{u}a} \cdot\sigma_{K,e}\V{n}_e \varphi_i^K
 !! & \approx 
 !! \sum_{l^s=1}^{m^s} \sigma_{K,e}\tilde{w}^{Q^s}_{l^s} \left[
 !!  det(J_e)\, \widehat{\V{u}a} \cdot \V{n}_e \varphi_i^K
 !!  \right](\tilde{\V{x}}^{Q^s}_{l^s}).
 !! \end{align*}
 !!
 !! Recall that
 !! \[
 !!  \sigma_{K_L(e),e} = 1, \qquad
 !!  \sigma_{K_R(e),e} = -1.
 !! \]
 !!
 !! The side integrals are *added* to ```e_rhs1``` and ```e_rhs2```.
 pure subroutine side_integral(s_rhs_l,s_rhs_r, u, base,s, ak_l,ak_r)
  !> advection coefficient
  real(wp),     intent(in)    :: u(:)
  !> base and quadrature formula
  type(t_base), intent(in)    :: base
  !> side \( e \)
  class(c_s),   intent(in)    :: s
  !> \(a^{K_L}_i, \quad i=1,\ldots,n\)
  real(wp),     intent(in)    :: ak_l(:)
  !> \(a^{K_R}_i, \quad i=1,\ldots,n\)
  real(wp),     intent(in)    :: ak_r(:)
  !> \(-\int_{e} \widehat{\V{u}a} \cdot\sigma_{K_L,e}\V{n}_e
  !! \varphi_i^{K_L}\)
  real(wp),     intent(inout) :: s_rhs_l(:)
  !> \(-\int_{e} \widehat{\V{u}a} \cdot\sigma_{K_R,e}\V{n}_e
  !! \varphi_i^{K_R}\)
  real(wp),     intent(inout) :: s_rhs_r(:)

  integer :: i, l, l_l, l_r
  integer :: l_lr(base%ms,-1:+1)
  real(wp) :: ah_kl(base%ms), ah_kr(base%ms)
  real(wp) :: u_dot_n, nflux_l, nflux_r, numflux
  real(wp) :: n(size(u),base%ms), detj(base%ms)

   ! fill the quadrature node indexing array
   l_lr(:,-1) = [(base%ms-l+1, l=1,base%ms)]
   l_lr(:,+1) = [(        l,   l=1,base%ms)]

   ! retrieve n and det(J) at all the quadrature points
   call s%get_n(        n , base%xqs )
   call s%get_sdetj( detj , base%xqs )

   ! compute the local solution at all the quadrature points
   ah_kl = 0.0_wp
   ah_kr = 0.0_wp
   do l=1,base%ms
     l_l = l_lr(l,s%orient_l)
     l_r = l_lr(l,s%orient_r)
     do i=1,base%n
       ah_kl(l) = ah_kl(l) + ak_l(i)*base%phib(i,l_l,s%iloc_l)
       ah_kr(l) = ah_kr(l) + ak_r(i)*base%phib(i,l_r,s%iloc_r)
     enddo
   enddo

   ! evaluate the integral
   do l=1,base%ms
     l_l = l_lr(l,s%orient_l)
     l_r = l_lr(l,s%orient_r)
     u_dot_n = dot_product(u,n(:,l))
     nflux_l = u_dot_n*ah_kl(l)
     nflux_r = u_dot_n*ah_kr(l)
     numflux = upwind_flux(u_dot_n,nflux_l,nflux_r)
     do i=1,base%n
       s_rhs_l(i) = s_rhs_l(i) &
         - base%wqs(l)*detj(l) * numflux*base%phib(i,l_l,s%iloc_l)
       s_rhs_r(i) = s_rhs_r(i) &
         + base%wqs(l)*detj(l) * numflux*base%phib(i,l_r,s%iloc_r)
     enddo
   enddo

 end subroutine side_integral

!-----------------------------------------------------------------------

 pure function upwind_flux(u_dot_n,nflux_l,nflux_r) result(upwflux)
  real(wp), intent(in) :: u_dot_n !! \( \V{u}\cdot\V{n} \)
  real(wp), intent(in) :: nflux_l !! normal flux on \(K_L\)
  real(wp), intent(in) :: nflux_r !! normal flux on \(K_R\)
  real(wp) :: upwflux

   if( u_dot_n .ge. 0 ) then
     upwflux = nflux_l
   else
     upwflux = nflux_r
   endif

 end function upwind_flux

!-----------------------------------------------------------------------

end module mod_scalar_adv_ode
