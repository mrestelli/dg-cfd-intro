module mod_1d_grid

 use mod_kinds, only: wp

 use mod_grid, only: t_grid, c_s, c_e
 
 implicit none

 public :: new_1d_grid
 ! The following should be used only for testing
 public :: t_s1d, t_e1d

 private

 type, extends(c_s) :: t_s1d
 contains
  procedure, pass(s) :: get_n     => get_n_1d
  procedure, pass(s) :: get_sdetj => get_sdetj_1d
 end type t_s1d

 type, extends(c_e) :: t_e1d
  real(wp) :: xb   !! center of the element
  real(wp) :: area !! \(|K|\)
 contains
  procedure, pass(e) :: get_j_it => get_j_it_1d
  procedure, pass(e) :: get_detj => get_detj_1d
  procedure, pass(e) :: map      => map_1d
 end type t_e1d

 !! Area of the reference element \(\tilde{K}=[-1,1]\) (see mod_base)
 real(wp), parameter :: area_k_tilde = 2.0_wp

contains

 ! Setup the module variable grid
 subroutine new_1d_grid(grid, ne,x_left,x_right)
  integer,      intent(in) :: ne(1)
  real(wp),     intent(in) :: x_left(1), x_right(1) ! domain size
  type(t_grid), intent(out) :: grid

  integer :: is, ie, iloc

   grid%ns = ne(1)
   grid%ne = ne(1)

   ! allocate and define the side array
   allocate( t_s1d::grid%s( grid%ns ) )
   select type( sides => grid%s ); type is(t_s1d)

   ! first side: periodic bcs
   sides(1)%e_l = grid%ne
   sides(1)%e_r = 1
   sides(1)%iloc_l = 2
   sides(1)%iloc_r = 1
   sides(1)%orient_l = +1 ! in 1D this makes no difference
   sides(1)%orient_r = +1
   ! remaining sides
   do is=2,grid%ns
     sides(is)%e_l = is-1
     sides(is)%e_r = is
     sides(is)%iloc_l = 2
     sides(is)%iloc_r = 1
     sides(is)%orient_l = +1
     sides(is)%orient_r = +1
   end do

   ! allocate and define the element array
   allocate( t_e1d::grid%e( grid%ne ) )
   select type( elems => grid%e ); type is(t_e1d)

   do ie=1,grid%ne
     ! each element has two sides
     allocate( elems(ie)%s(2) )
   end do
   do is=1,grid%ns
     ! loop over the sides to fill the element information
     ! left element
     ie   = sides(is)%e_l
     iloc = sides(is)%iloc_l
     elems(ie)%s(iloc) = is
     ! right element
     ie   = sides(is)%e_r
     iloc = sides(is)%iloc_r
     elems(ie)%s(iloc) = is
   end do
   do ie=1,grid%ne ! fill the geometric information
     elems(ie)%area = (x_right(1)-x_left(1))/real(grid%ne,wp)
     elems(ie)%xb   = x_left(1) + &
       (0.5_wp+real(ie-1,wp))*(x_right(1)-x_left(1))/real(grid%ne,wp)
   end do

   end select

   end select

 end subroutine new_1d_grid
 
!-----------------------------------------------------------------------

 pure subroutine get_n_1d(n, s,x)
  class(t_s1d), intent(in)  :: s
  real(wp),     intent(in)  :: x(:,:)
  real(wp),     intent(out) :: n(:,:)

   n(1,:) = 1.0_wp

 end subroutine get_n_1d

 pure subroutine get_sdetj_1d(detj, s,x)
  class(t_s1d), intent(in)  :: s
  real(wp),     intent(in)  :: x(:,:)
  real(wp),     intent(out) :: detj(:)

   detj = 1.0_wp

 end subroutine get_sdetj_1d

 !> For one-dimensional elements \(J_K = \frac{|K|}{|\tilde{K}|\)
 pure subroutine get_j_it_1d(j_it, e,x)
  class(t_e1d), intent(in)  :: e
  real(wp),     intent(in)  :: x(:,:)
  real(wp),     intent(out) :: j_it(:,:,:)

   j_it = area_k_tilde / e%area

 end subroutine get_j_it_1d

 pure subroutine get_detj_1d(detj, e,x)
  class(t_e1d), intent(in)  :: e
  real(wp),     intent(in)  :: x(:,:)
  real(wp),     intent(out) :: detj(:)

   detj = e%area / area_k_tilde

 end subroutine get_detj_1d

 pure subroutine map_1d(x, e,x_tilde)
  class(t_e1d), intent(in)  :: e
  real(wp),     intent(in)  :: x_tilde(:,:)
  real(wp),     intent(out) :: x(:,:)

   x = e%xb + e%area/area_k_tilde*x_tilde

 end subroutine map_1d

!-----------------------------------------------------------------------

end module mod_1d_grid
