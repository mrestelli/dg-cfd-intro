program solve_scalar_adv

 use mod_kinds, only: wp

 use mod_rk, only: t_explicit_euler, t_heun_method

 use mod_base, only: t_base, new_1d_base, new_2d_base

 use mod_grid, only: t_grid
 use mod_1d_grid, only: new_1d_grid
 use mod_2d_grid, only: new_2d_grid

 use mod_scalar_adv_state, only: t_scalar_adv_state
 use mod_scalar_adv_ode, only: &
   t_scalar_adv_ode, scalar_adv_ode_setup, scalar_adv_ode_clean

 implicit none

 ! Equation coefficients
 !real(wp), parameter :: u(1)  = 5.0_wp
 real(wp), parameter :: u(2)  = [10.0_wp,5.0_wp]
 ! Discretization parameters
 !integer,  parameter :: n_elem(1) = 50
 !real(wp), parameter :: x_left(1) = 0.0_wp, x_right(1) = 2.0_wp
 integer,  parameter :: n_elem(2) = [50,50]
 real(wp), parameter :: x_left(2) = 0.0_wp, x_right(2) = 2.0_wp
 integer,  parameter :: n_step = 500
 real(wp), parameter :: dt = 0.0005_wp
 integer :: degree

 ! Discretization
 type(t_base) :: base
 type(t_grid) :: grid

 ! ODE
 integer :: n
 real(wp) :: t
 type(t_scalar_adv_state) :: ynow, ynew
 type(t_scalar_adv_ode)   :: ode

 ! Time integrator
 type(t_heun_method) :: time_integrator

 ! Output file
 integer :: ie
 integer :: file_unit
 real(wp), allocatable :: xdata(:,:,:)

  write(*,*) "Choose the polynomial degree: 0 or 1"
  read(*,*) degree

  ! Set-up the problem

  ! grid
  select case(size(u))
   case(1) ! one-dimensional grid and base required
    call new_1d_base(base,degree)
    call new_1d_grid(grid,n_elem,x_left,x_right)
   case(2) ! two-dimensional
    call new_2d_base(base,degree)
    call new_2d_grid(grid,n_elem,x_left,x_right)
   case default
    error stop 'In "solve_scalar_adv.f90": not implemented'
  end select

  ! define the ODE
  call scalar_adv_ode_setup( ode, base, grid, u )
  ! the grid and the base have been copied into ode: we can free them
  call base%clean()
  call grid%clean()

  ! prepare the state vectors
  call ode%new_scalar_adv_state( ynow )
  call ode%new_scalar_adv_state( ynew )

  ! initial condition and visualization
  associate(  n_coords => size(ode%base%x_dofs,1) , &
             n_out_pts => size(ode%base%x_dofs,2) )
  allocate( xdata(n_coords,n_out_pts,ode%grid%ne) )
  do ie=1,ode%grid%ne
    call ode%grid%e(ie)%map( xdata(:,:,ie) , ode%base%x_dofs )
    ynow%aa(:,ie) = initial_condition( xdata(:,:,ie) )
  enddo
  end associate

  ! Prepare the output file
  call output_file_setup()

  ! Integrate the system
  call time_integrator%setup(ynow)
  do n=1,n_step
    t = real(n,wp)*dt

    ! Compute the time step
    call time_integrator%compute_step(ynew , ode,dt,ynow)
    call ynow%copy( ynew )

    ! Write the solution
    call output_file_write_data( t=t , a=ynow%aa ,          &
      a_ex = reshape( exact_solution( reshape(              &
          xdata,[size(xdata,1),size(xdata,2)*size(xdata,3)] &
        ) , t,ode%u),[size(xdata,2),size(xdata,3)] ) )

  enddo

  ! Finalize the output file
  call output_file_finalize()

  write(*,*) "Done, results written in 'solve_scalar_adv.out'."

  ! Clean-up
  deallocate( ynow%aa )
  deallocate( ynew%aa )
  deallocate( xdata )
  call scalar_adv_ode_clean(ode)
  call time_integrator%clean()

contains

 pure function initial_condition(x) result(a0)
  real(wp), intent(in) :: x(:,:)
  real(wp) :: a0(size(x,2))

   a0 = exact_solution(x,0.0_wp,u)

 end function initial_condition

 pure function exact_solution(x,t,u) result(a)
  real(wp), intent(in) :: x(:,:), t, u(:)
  real(wp) :: a(size(x,2))

  integer, parameter :: m = 4
  real(wp), parameter :: pi = 3.14159265358979323846264338327950288_wp
  integer :: i
  real(wp) :: l

   !l = (2.0_wp*pi)/(x_right(1)-x_left(1)) * real(m,wp)
   !a = sin( l * (x(1,:)-u(1)*t) )

   do i=1,size(x,2)
     associate( r => sqrt(sum((x(:,i)-[1.0_wp,1.0_wp])**2)) )
     if( r .le. 0.3_wp ) then
       a(i) = 1.0_wp - r/0.3_wp
       !a(i) = 1.0_wp
     else
       a(i) = 0.0_wp
     endif
     end associate
   enddo

 end function exact_solution

 subroutine output_file_setup()
  integer :: ie2
  real(wp), allocatable :: xdata_tmp(:,:,:)
   ! Open a file and connect it to a new unit
   open(newunit=file_unit, file="solve_scalar_adv.out", &
        status='replace',action='write')
   ! Write the file header
   write(file_unit,'(a)') &
     'title  = "Scalar advection equation"'
   write(file_unit,'(a)') &
     'xlabel = "t"'
   write(file_unit,'(a)') &
     'ylabel = ""'
   if(size(u).eq.1) then
     write(file_unit,'(a)') &
       'yNames = "exact solution" , "numerical solution"'
   else
     write(file_unit,'(a)') &
       'yNames = "numerical solution"'
     if(degree.eq.0) then
       write(file_unit,'(a,i0)') &
         'n_x = ', n_elem(1)
       write(file_unit,'(a,i0)') &
         'n_y = ', n_elem(2)
     else
       write(file_unit,'(a,i0)') &
         'n_x = ', 2*n_elem(1)
       write(file_unit,'(a,i0)') &
         'n_y = ', 2*n_elem(2)
       allocate( xdata_tmp( 2 , 2*n_elem(1) , 2*n_elem(2) ) )
       do ie2=1,n_elem(2)
         associate( row => (ie2-1)*n_elem(1) )
         xdata_tmp(:,1::2,(ie2-1)*2+1) = xdata(:,1,row+1:row+n_elem(1))
         xdata_tmp(:,2::2,(ie2-1)*2+1) = xdata(:,2,row+1:row+n_elem(1))
         xdata_tmp(:,1::2,(ie2-1)*2+2) = xdata(:,4,row+1:row+n_elem(1))
         xdata_tmp(:,2::2,(ie2-1)*2+2) = xdata(:,3,row+1:row+n_elem(1))
         end associate
       enddo
     endif
   endif
   if(.not.(allocated(xdata_tmp))) then
     write(file_unit,'(a,*(e23.15,:,","))') &
       'xdata  = ', xdata
   else
     write(file_unit,'(a,*(e23.15,:,","))') &
       'xdata  = ', xdata_tmp
     deallocate(xdata_tmp)
   endif
 end subroutine output_file_setup

 subroutine output_file_finalize()
   ! Close the file
   close(unit=file_unit)
 end subroutine output_file_finalize

 subroutine output_file_write_data(t,a,a_ex)
  real(wp), intent(in) :: t
  real(wp), intent(in) :: a(:,:)
  real(wp), intent(in) :: a_ex(:,:)

  integer :: ie2
  real(wp), allocatable :: a_tmp(:,:)

   if((size(u).eq.2).and.(degree.eq.1)) then
     allocate( a_tmp( 2*n_elem(1) , 2*n_elem(2) ) )
     do ie2=1,n_elem(2)
       associate( row => (ie2-1)*n_elem(1) )
       a_tmp(1::2,(ie2-1)*2+1) = a(1,row+1:row+n_elem(1))
       a_tmp(2::2,(ie2-1)*2+1) = a(2,row+1:row+n_elem(1))
       a_tmp(1::2,(ie2-1)*2+2) = a(4,row+1:row+n_elem(1))
       a_tmp(2::2,(ie2-1)*2+2) = a(3,row+1:row+n_elem(1))
       end associate
     enddo
   endif

   write(file_unit,'(a,e23.15)') &
     'time   = ',t
   
   if(.not.(allocated(a_tmp))) then
     write(file_unit,'(a,*(e23.15,:,","))') &
       '"numerical solution" = ',a
   else
     write(file_unit,'(a,*(e23.15,:,","))') &
       '"numerical solution" = ',a_tmp
     deallocate(a_tmp)
   endif

   if(size(u).eq.1) then
     write(file_unit,'(a,*(e23.15,:,","))') &
       '"exact solution"     = ',a_ex
   endif
 end subroutine output_file_write_data

end program solve_scalar_adv

