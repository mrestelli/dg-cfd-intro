module mod_euler_ode

 use mod_kinds, only: wp

 use mod_ode_state, only: c_ode_state

 use mod_ode_solver, only: c_ode

 use mod_linalg, only: invmat_chol

 use mod_grid, only: t_grid, c_s, c_e

 use mod_base, only: t_base

 use mod_euler_state, only: t_euler_state

 implicit none

 public :: &
   t_euler_ode, euler_ode_setup, euler_ode_clean, gamma, &
   element_mass_matrix

 private

 !> Compute the rhs resulting from the DG discretization of the Euler
 !! equation.
 type, extends(c_ode) :: t_euler_ode
  integer      :: d     !! space dimension
  type(t_grid) :: grid
  type(t_base) :: base
  !> \(\left(M^K\right)^{-1}\) for all the elements
  real(wp), allocatable :: imass_matrices(:,:,:)
 contains
  procedure, pass(ode) :: compute_rhs => compute_euler_rhs
  !> generate a state vector for this ODE
  procedure, pass(ode) :: new_euler_state
 end type t_euler_ode

 real(wp) :: gamma = 1.4_wp

contains

!-----------------------------------------------------------------------

 subroutine euler_ode_setup(ode, d,base,grid)
  integer,           intent(in) :: d ! space dimension
  type(t_base),      intent(in) :: base
  type(t_grid),      intent(in) :: grid
  type(t_euler_ode), intent(out) :: ode

  integer :: ie
  real(wp), allocatable :: m(:,:) ! work array for mass matrix

   ode%d = d

   ode%base = base

   ode%grid = grid

   ! precompute the mass matrices
   allocate(m(ode%base%n,ode%base%n))
   allocate( ode%imass_matrices(ode%base%n,ode%base%n,ode%grid%ne) )
   do ie=1,ode%grid%ne
     ! mass matrix
     call element_mass_matrix( m, ode%base, ode%grid%e(ie) )
     ! invert the mass matrix
     call invmat_chol( m , ode%imass_matrices(:,:,ie) )
   enddo
   deallocate(m)

 end subroutine euler_ode_setup

 subroutine euler_ode_clean(ode)
  type(t_euler_ode), intent(inout) :: ode

   deallocate( ode%imass_matrices )
   call ode%base%clean()
   call ode%grid%clean()

 end subroutine euler_ode_clean

 subroutine new_euler_state(y, ode)
  class(t_euler_ode),   intent(in)  :: ode
  class(t_euler_state), intent(out) :: y

   allocate( y%uu(ode%d+2,ode%base%n,ode%grid%ne) )

 end subroutine new_euler_state

!-----------------------------------------------------------------------

 subroutine compute_euler_rhs(rhs,ode,y)
  class(t_euler_ode), intent(in) :: ode
  class(c_ode_state), intent(in)    :: y
  class(c_ode_state), intent(inout) :: rhs

  integer :: ie, is, e_l,e_r, k

   select type(y);   type is(t_euler_state)
   select type(rhs); type is(t_euler_state)

   ! initialize rhs
   rhs%uu = 0.0_wp

   do ie=1,ode%grid%ne
     associate( uuk => y%uu(:,:,ie) )
     call element_integral(rhs%uu(:,:,ie), ode%d,      &
                           ode%base,ode%grid%e(ie), uuk)
     end associate
   enddo

   ! side fluxes
   do is=1,ode%grid%ns
     e_l = ode%grid%s(is)%e_l
     e_r = ode%grid%s(is)%e_r
     call side_integral( rhs%uu(:,:,e_l) , rhs%uu(:,:,e_r) , ode%d , &
              ode%base, ode%grid%s(is), y%uu(:,:,e_l), y%uu(:,:,e_r) )
   enddo

   ! include the mass matrix
   do ie=1,ode%grid%ne
     do k=1,size(rhs%uu,1) ! loop over the variables
       rhs%uu(k,:,ie) = &
         matmul(ode%imass_matrices(:,:,ie),rhs%uu(k,:,ie))
     enddo
   enddo

   end select
   end select

 end subroutine compute_euler_rhs

!-----------------------------------------------------------------------

 pure subroutine element_mass_matrix(m, base,e)
  type(t_base), intent(in)  :: base  !! base and quadrature formula
  class(c_e),   intent(in)  :: e     !! element \(K\)
  real(wp),     intent(out) :: m(:,:)

  integer :: i, j, l
  real(wp) :: detj(base%m)

   ! retrieve det(J) at all the quadrature points
   call e%get_detj( detj , base%xq )

   ! evaluate the integral
   m = 0.0_wp
   do l=1,base%m
     do j=1,base%n
       do i=1,base%n
         m(i,j) = m(i,j) &
                 + base%wq(l)*detj(l) * base%phi(i,l)*base%phi(j,l)
       enddo
     enddo
   enddo

 end subroutine element_mass_matrix

!-----------------------------------------------------------------------

 pure subroutine element_integral(e_rhs, d, base,e,uuk)
  integer,      intent(in)    :: d     !! space dimension
  type(t_base), intent(in)    :: base  !! base and quadrature formula
  class(c_e),   intent(in)    :: e     !! element
  !> \(a^K_i, \quad i=1,\ldots,n\)
  real(wp),     intent(in)    :: uuk(:,:)
  real(wp),     intent(inout) :: e_rhs(:,:)

  integer :: i, l
  real(wp) :: uuh_k(size(uuk,1),base%m), flux(d,size(uuk,1)), &
    gradphi(d), j_it(d,d,base%m), detj(base%m)

   ! retrieve J^{-T} and det(J) at all the quadrature points
   call e%get_j_it( j_it , base%xq )
   call e%get_detj( detj , base%xq )

   ! compute the local solution at all the quadrature points
   uuh_k = 0.0_wp
   do l=1,base%m
     do i=1,base%n
       uuh_k(:,l) = uuh_k(:,l) + uuk(:,i)*base%phi(i,l)
     enddo
   enddo

   ! evaluate the integral
   do l=1,base%m
     flux = euler_flux(uuh_k(:,l))
     do i=1,base%n
       gradphi  = matmul( j_it(:,:,l) , base%dphi(:,i,l) )
       e_rhs(:,i) = e_rhs(:,i) &
         + base%wq(l)*detj(l) * matmul( gradphi , flux )
     enddo
   enddo

 end subroutine element_integral

!-----------------------------------------------------------------------

 !> Compute the side integrals
 pure subroutine side_integral(s_rhs_l,s_rhs_r, d, base,s, uuk_l,uuk_r)
  !> space dimesion
  integer,      intent(in)    :: d
  !> base and quadrature formula
  type(t_base), intent(in)    :: base
  !> side \( e \)
  class(c_s),   intent(in)    :: s
  !> \(U^{K_L}_i, \quad i=1,\ldots,n\)
  real(wp),     intent(in)    :: uuk_l(:,:)
  !> \(U^{K_R}_i, \quad i=1,\ldots,n\)
  real(wp),     intent(in)    :: uuk_r(:,:)
  real(wp),     intent(inout) :: s_rhs_l(:,:)
  real(wp),     intent(inout) :: s_rhs_r(:,:)

  integer :: i, l, l_l, l_r
  integer :: l_lr(base%ms,-1:+1)
  real(wp) :: uuh_kl(2+d,base%ms), uuh_kr(2+d,base%ms)
  real(wp) :: numflux(2+d) ! nflux_l, nflux_r, numflux
  real(wp) :: n(d,base%ms), detj(base%ms)

   ! fill the quadrature node indexing array
   l_lr(:,-1) = [(base%ms-l+1, l=1,base%ms)]
   l_lr(:,+1) = [(        l,   l=1,base%ms)]

   ! retrieve n and det(J) at all the quadrature points
   call s%get_n(        n , base%xqs )
   call s%get_sdetj( detj , base%xqs )

   ! compute the local solution at all the quadrature points
   uuh_kl = 0.0_wp
   uuh_kr = 0.0_wp
   do l=1,base%ms
     l_l = l_lr(l,s%orient_l)
     l_r = l_lr(l,s%orient_r)
     do i=1,base%n
       uuh_kl(:,l) = uuh_kl(:,l) + uuk_l(:,i)*base%phib(i,l_l,s%iloc_l)
       uuh_kr(:,l) = uuh_kr(:,l) + uuk_r(:,i)*base%phib(i,l_r,s%iloc_r)
     enddo
   enddo

   ! evaluate the integral
   do l=1,base%ms
     l_l = l_lr(l,s%orient_l)
     l_r = l_lr(l,s%orient_r)
     numflux = numerical_flux(n(:,l),uuh_kl(:,l),uuh_kr(:,l))
     do i=1,base%n
       s_rhs_l(:,i) = s_rhs_l(:,i) &
         - base%wqs(l)*detj(l) * numflux*base%phib(i,l_l,s%iloc_l)
       s_rhs_r(:,i) = s_rhs_r(:,i) &
         + base%wqs(l)*detj(l) * numflux*base%phib(i,l_r,s%iloc_r)
     enddo
   enddo

 end subroutine side_integral

!-----------------------------------------------------------------------

 pure function euler_flux(uu) result(flux)
  real(wp), intent(in) :: uu(:)
  real(wp) :: flux( size(uu)-2 , size(uu) )

  integer :: d, i, j
  real(wp) :: v(size(uu)-2), p ! velocity, pressure

  d = size(uu)-2 ! space dimension

  associate( rho  => uu(1),       & 
             rhov => uu(1+1:1+d), &
             e    => uu(d+2)      )

  v = rhov/rho
  p = (gamma-1.0_wp)*(e-0.5_wp*dot_product(rhov,v))

  flux(:,1)   = rhov
  do j=1,d
    do i=1,d
      flux(i,1+j) = rhov(i)*v(j)
    enddo
    flux(j,1+j) = flux(j,1+j) + p ! diagonal contributions
  enddo
  flux(:,d+2) = v*(e+p) 

  end associate
  
 end function euler_flux

!-----------------------------------------------------------------------

 pure function numerical_flux(n,uu_l,uu_r) result(f_hat_n)
  real(wp), intent(in) :: n(:)
  real(wp), intent(in) :: uu_l(:)
  real(wp), intent(in) :: uu_r(:)
  real(wp) :: f_hat_n(size(uu_l))

  integer :: d

  real(wp) :: v_l(size(n)), p_l, c_l ! vel. pressure, sound speed
  real(wp) :: v_r(size(n)), p_r, c_r
  real(wp) :: v_l_n, v_r_n           ! normal velocities
  real(wp) :: lam_max                ! maximum wavespeed
  real(wp) :: ef_l(size(n),size(uu_l)) ! Euer flux
  real(wp) :: ef_r(size(n),size(uu_l))

  d = size(n) ! space dimension

  associate( rho_l => uu_l(1),        rho_r => uu_r(1),       & 
            rhov_l => uu_l(1+1:1+d), rhov_r => uu_r(1+1:1+d), &
               e_l => uu_l(d+2),        e_r => uu_r(d+2)      )

  v_l = rhov_l/rho_l
  v_r = rhov_r/rho_r
  p_l = (gamma-1.0_wp)*(e_l-0.5_wp*dot_product(rhov_l,v_l))
  p_r = (gamma-1.0_wp)*(e_r-0.5_wp*dot_product(rhov_r,v_r))
  c_l = sqrt(gamma*p_l/rho_l)
  c_r = sqrt(gamma*p_r/rho_r)

  v_l_n = dot_product( v_l , n )
  v_r_n = dot_product( v_r , n )
  lam_max = max( abs(v_l_n)+c_l , abs(v_r_n)+c_r )

  end associate

  ef_l = euler_flux(uu_l)
  ef_r = euler_flux(uu_r)

  f_hat_n = 0.5_wp*matmul(n,ef_l+ef_r) - lam_max*(uu_r-uu_l)
 
 end function numerical_flux

!-----------------------------------------------------------------------

end module mod_euler_ode
