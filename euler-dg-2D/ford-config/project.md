project: DG &ndash; 1D Euler equations
summary: DG discretization of the 1D Euler Equations
author: Marco Restelli, Florian Hindenlang
email: mrestelli@gmail.com, hindenlang@gmail.com
website: http://www2.ipp.mpg.de/~marre
bitbucket: https://bitbucket.org/mrestelli
project_bitbucket: https://bitbucket.org/mrestelli/dg-cfd-intro
license: by
src_dir: ./../src
output_dir: ./../doc
exclude_dir: ./../doc
             ./../doc/src
mathjax_config: ./../ford-config/MathJax-latex-macros.js
predocmark: >
display: public
         protected
         private
source: false
graph: true
search: true
macro: TEST
       LOGIC=.true.

Solves the Euler equation system
\begin{equation}
\partial_t U + \Div\V{F}(U) = 0 \,,
\label{eq:Ucons-law}
\end{equation}
where, in 1D, the vector of conservative variables \(U=(\rho, \rho v,E)^T\) consists of the Density \(\rho\), momentum \(\rho v\) and total Energy \(E\). The full 1D Euler equations in conservation form write as
\begin{equation}
\partial_t \Mat{\rho \\ v \\ E \\} 
 + \partial_x \Mat{\rho v \\ \rho v v + p\\ (E+p)v \\} = 0 \,.
\end{equation}
The specific energy \(\varepsilon\) and the pressure are related, assuming the thermodynamics of an ideal gas
\begin{equation}
 \varepsilon=\frac{p}{(\gamma-1)\rho}\,,
\end{equation}
 ant the relation of the total energy is
\begin{equation}
  E=\rho\varepsilon + \frac{1}{2}\rho v^2\,.
\end{equation}
The Discontinuous Galerkin (DG) discretization of \eqref{eq:Ucons-law} is
\begin{equation}
\int_K \partial_t U_h\varphi_h -\int_K\V{F}_h\cdot\Grad\varphi_h
+ \int_{\partial K} \widehat{F}_n \varphi_h = 0.
\label{eq:Ucons-DG}
\end{equation}
where the numerical flux \(\widehat{F}_n=F_\text{RP}(U_L,U_R,\V{n})\) is the solution of an approximate Riemann problem, with the consistency condition \(F_\text{RP}(U,U,\V{n})=\V{F}(U)\cdot\V{n}\)


