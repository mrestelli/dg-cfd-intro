module mod_rk_tests

 use mod_kinds, only: wp

 use mod_ode_state, only: c_ode_state

 use mod_ode_solver, only: c_ode

 use mod_rk, only: t_explicit_euler, t_heun_method

 ! This module is provided by the unit testing framework and must be
 ! used in every test collection.
 use pfunit_mod

 implicit none

 public

 ! To test the ODE solvers we need an ODE. For testing, it makes sense
 ! to use very simple cases, such as:
 !
 !  y' = 0
 !
 ! or
 !
 !  y' = 1  
 !
 ! and so on. These ODEs must be formulated using the structure
 ! required by the ODE solvers.

 type, extends(c_ode_state) :: t_scalar
  real(wp) :: r
 contains
  procedure, pass(x) :: increment       => scalar_increment
  procedure, pass(x) :: scalar_multiply => scalar_scalar_multiply
  procedure, pass(y) :: copy            => scalar_copy
  procedure, pass(x) :: clone           => scalar_clone
 end type t_scalar

 type, extends(c_ode) :: t_dydt_zero
 contains
  procedure, pass(ode) :: compute_rhs => compute_zero_rhs
 end type t_dydt_zero

 ! Add a time t to represent a nonautonomous system
 !
 ! r' = rhs(r,t)
 !
 ! as an autonomous one
 !
 ! r' = rhs(r,t)
 ! t' = 1
 type, extends(c_ode_state) :: t_scalar_and_t
  real(wp) :: r
  real(wp) :: t
 contains
  procedure, pass(x) :: increment       => scalar_and_t_increment
  procedure, pass(x) :: scalar_multiply => scalar_and_t_scalar_multiply
  procedure, pass(y) :: copy            => scalar_and_t_copy
  procedure, pass(x) :: clone           => scalar_and_t_clone
 end type t_scalar_and_t

 ! Define the ODE
 !
 !  r' = t
 type, extends(c_ode) :: t_dydt_t
 contains
  procedure, pass(ode) :: compute_rhs => compute_t_rhs
 end type t_dydt_t

contains

!-----------------------------------------------------------------------

 @Test
 subroutine explicit_euler__is_exact_for_constant_rhs()

  type(t_scalar) :: y0, ynew
  type(t_dydt_zero) :: const_rhs_ode
  type(t_explicit_euler) :: euler_integrator

  y0%r = 3.5_wp
  call euler_integrator%setup(y0)
  call euler_integrator%compute_step(ynew , const_rhs_ode,0.5_wp,y0)

  @assertEqual( 3.5_wp , ynew%r )

  call euler_integrator%clean()

 end subroutine explicit_euler__is_exact_for_constant_rhs

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine heun_method__is_exact_for_constant_rhs()

  type(t_scalar) :: y0, ynew
  type(t_dydt_zero) :: const_rhs_ode
  type(t_heun_method) :: heun_method

  y0%r = 3.5_wp
  call heun_method%setup(y0)
  call heun_method%compute_step(ynew , const_rhs_ode,0.5_wp,y0)

  @assertEqual( 3.5_wp , ynew%r )

  call heun_method%clean()

 end subroutine heun_method__is_exact_for_constant_rhs

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine heun_method__is_exact_for_linear_rhs()

  ! Consider the nonautonomous problem
  !
  !   r' = t
  !
  ! for which the method must be exact.

  real(wp), parameter :: dt = 0.1_wp
  type(t_scalar_and_t) :: y0, ynew
  type(t_dydt_t) :: linear_rhs_ode
  type(t_heun_method) :: heun_method

  y0%r = 0.0_wp
  y0%t = 0.0_wp
  call heun_method%setup(y0)
  call heun_method%compute_step(ynew , linear_rhs_ode,dt,y0)

  @assertEqual( 0.5_wp*dt**2 , ynew%r )
  @assertEqual( dt           , ynew%t )

  call heun_method%clean()

 end subroutine heun_method__is_exact_for_linear_rhs

!-----------------------------------------------------------------------

 subroutine scalar_increment(x,y)
  class(c_ode_state), intent(in)    :: y
  class(t_scalar),    intent(inout) :: x

   select type(y); type is(t_scalar)
   x%r = x%r + y%r
   end select
 end subroutine scalar_increment

 subroutine scalar_scalar_multiply(x,r)
  real(wp),        intent(in)    :: r
  class(t_scalar), intent(inout) :: x

   x%r = r*x%r
 end subroutine scalar_scalar_multiply

 subroutine scalar_copy(y,x)
  class(c_ode_state), intent(in)    :: x
  class(t_scalar),    intent(inout) :: y

   select type(x); type is(t_scalar)
   y%r = x%r
   end select
 end subroutine scalar_copy

 subroutine scalar_clone(y,x)
  class(t_scalar),                 intent(in)  :: x
  class(c_ode_state), allocatable, intent(out) :: y

   allocate( y , source=x )
 end subroutine scalar_clone

!-----------------------------------------------------------------------

 subroutine scalar_and_t_increment(x,y)
  class(c_ode_state),    intent(in)    :: y
  class(t_scalar_and_t), intent(inout) :: x

   select type(y); type is(t_scalar_and_t)
   x%r = x%r + y%r
   x%t = x%t + y%t
   end select
 end subroutine scalar_and_t_increment

 subroutine scalar_and_t_scalar_multiply(x,r)
  real(wp),              intent(in)    :: r
  class(t_scalar_and_t), intent(inout) :: x

   x%r = r*x%r
   x%t = r*x%t
 end subroutine scalar_and_t_scalar_multiply

 subroutine scalar_and_t_copy(y,x)
  class(c_ode_state),    intent(in)    :: x
  class(t_scalar_and_t), intent(inout) :: y

   select type(x); type is(t_scalar_and_t)
   y%r = x%r
   y%t = x%t
   end select
 end subroutine scalar_and_t_copy

 subroutine scalar_and_t_clone(y,x)
  class(t_scalar_and_t),           intent(in)  :: x
  class(c_ode_state), allocatable, intent(out) :: y

   allocate( y , source=x )
 end subroutine scalar_and_t_clone

!-----------------------------------------------------------------------

 subroutine compute_zero_rhs(rhs,ode,y)
  class(t_dydt_zero), intent(in)    :: ode
  class(c_ode_state), intent(in)    :: y
  class(c_ode_state), intent(inout) :: rhs

   select type(rhs); type is(t_scalar)

   rhs%r = 0.0_wp

   end select
 end subroutine compute_zero_rhs

 subroutine compute_t_rhs(rhs,ode,y)
  class(t_dydt_t),    intent(in)    :: ode
  class(c_ode_state), intent(in)    :: y
  class(c_ode_state), intent(inout) :: rhs

   select type(rhs); type is(t_scalar_and_t)
   select type(y);   type is(t_scalar_and_t)

   rhs%r = y%t
   rhs%t = 1.0_wp

   end select
   end select
 end subroutine compute_t_rhs

!-----------------------------------------------------------------------

end module mod_rk_tests

