module mod_1d_base_tests

 use mod_kinds, only: wp

 use mod_1d_base, only: t_base, base_setup, base_clean

 ! This module is provided by the unit testing framework and must be
 ! used in every test collection.
 use pfunit_mod

 implicit none

 public

 real(wp), parameter :: toll = 1.0e-14_wp

contains

!-----------------------------------------------------------------------

 @Test
 subroutine zero_order_base__built_correctly()

   ! we expect the following local base:
   !  phi = 1  (one function)
   !  x^Q = 0  (one quadrature point)
   !  w^Q = 2

   type(t_base) :: base

   call base_setup( base , degree=0 )

   @assertEqual( 1 , base%n)
   @assertEqual( 1 , base%m)

   @assertTrue( allocated(base%wq) )
   @assertTrue( allocated(base%phi) )
   @assertTrue( allocated(base%dphi) )

   @assertEqual( [2.0_wp] , base%wq , tolerance=toll)
   @assertEqual( [1.0_wp] , base%phi(1,:) , tolerance=toll)
   @assertEqual( [0.0_wp] , base%dphi(1,:) , tolerance=toll)

   call base_clean( base )

 end subroutine zero_order_base__built_correctly

!-----------------------------------------------------------------------

 @Test
 subroutine first_order_base__built_correctly()

   ! we expect the following local base:
   !  phi_1 = 0.5*(1-x),   phi_2 = 0.5*(x-1)
   !  x^Q = +-sqrt(1/3)  (two nodes)
   !  w^Q = 1            (for both nodes)

   ! position of the quadrature points
   real(wp), parameter :: xq(2) = [-1.0_wp , +1.0_wp]/sqrt(3.0_wp)
   type(t_base) :: base


   call base_setup( base , degree=1 )

   @assertEqual( 2 , base%n)
   @assertEqual( 2 , base%m)

   @assertTrue( allocated(base%wq) )
   @assertTrue( allocated(base%phi) )
   @assertTrue( allocated(base%dphi) )

   @assertEqual( [1.0_wp,1.0_wp] , base%wq , tolerance=toll)
   @assertEqual( 0.5_wp*(1.0_wp-xq) , base%phi(1,:) , tolerance=toll)
   @assertEqual( 0.5_wp*(1.0_wp+xq) , base%phi(2,:) , tolerance=toll)
   @assertEqual( [-0.5_wp,-0.5_wp] , base%dphi(1,:) , tolerance=toll)
   @assertEqual( [ 0.5_wp, 0.5_wp] , base%dphi(2,:) , tolerance=toll)

   call base_clean( base )

 end subroutine first_order_base__built_correctly

!-----------------------------------------------------------------------

end module mod_1d_base_tests
