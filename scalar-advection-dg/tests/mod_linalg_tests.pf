module mod_linalg_tests

 use mod_kinds, only: wp

 use mod_linalg, only: invmat_chol

 ! This module is provided by the unit testing framework and must be
 ! used in every test collection.
 use pfunit_mod

 implicit none

 public

 real(wp), parameter :: toll = 1.0e-14_wp

contains

!-----------------------------------------------------------------------

 @Test
 subroutine invmat_chol__works__for_1x1_matrix()

  real(wp), parameter :: a(1,1) = 3.0_wp
  real(wp) :: ai(1,1)

  call invmat_chol( a, ai )

  @assertEqual( 1.0_wp/3.0_wp , ai(1,1) ,tolerance=toll)

 end subroutine invmat_chol__works__for_1x1_matrix

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine invmat_chol__works__for_2x2_matrix()

  real(wp), parameter :: a(2,2) = reshape( &
     [3.0_wp,1.0_wp,1.0_wp,2.0_wp] , [2,2] )
  real(wp), parameter :: ai_ex(2,2) = reshape( &
     1.0_wp/5.0_wp*[2.0_wp,-1.0_wp,-1.0_wp,3.0_wp] , [2,2] )
  real(wp) :: ai(2,2)

  call invmat_chol( a, ai )

  @assertEqual( ai_ex , ai ,tolerance=toll)

 end subroutine invmat_chol__works__for_2x2_matrix

!-----------------------------------------------------------------------

end module mod_linalg_tests

