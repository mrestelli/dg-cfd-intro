module mod_1d_scalar_adv_ode_tests

 use mod_kinds, only: wp

 use mod_1d_grid, only: t_grid, grid_setup, grid_clean

 use mod_1d_base, only: t_base, base_setup, base_clean

 use mod_1d_scalar_adv_state, only: t_1d_scalar_adv_state

 use mod_1d_scalar_adv_ode, only: t_1d_scalar_adv_ode, u, &
   scalar_adv_ode_setup, scalar_adv_ode_clean, u,         &
   element_mass_matrix, element_integral, side_integral, upwind_flux

 ! This module is provided by the unit testing framework and must be
 ! used in every test collection.
 use pfunit_mod

 implicit none

 public

 real(wp), parameter :: toll = 1.0e-14_wp

 ! Tests are done using sometimes constants and sometimes linear base
 ! functions.
 type(t_1d_scalar_adv_ode) :: ode_p0
 type(t_1d_scalar_adv_ode) :: ode_p1

contains

!-----------------------------------------------------------------------

 @Before
 subroutine mod_1d_grid_tests_setup()

   call scalar_adv_ode_setup(ode_p0,                   &
      ne=5 , x_left=0.0_wp , x_right=1.0_wp , degree=0 )

   call scalar_adv_ode_setup(ode_p1,                   &
      ne=5 , x_left=0.0_wp , x_right=1.0_wp , degree=1 )

 end subroutine mod_1d_grid_tests_setup

 @After
 subroutine mod_1d_grid_tests_teardown()
 
   call scalar_adv_ode_clean(ode_p0)

   call scalar_adv_ode_clean(ode_p1)

 end subroutine mod_1d_grid_tests_teardown

!-----------------------------------------------------------------------

 @Test
 subroutine compute_1d_scalar_adv_rhs__y_const__rhs_is_zero_p0()

  type(t_1d_scalar_adv_state) :: y, rhs

   allocate(y%a(1,ode_p0%grid%ne))
   y%a = 1.0_wp
   allocate(rhs%a(1,ode_p0%grid%ne))

   call ode_p0%compute_rhs(rhs,y)

   @assertEqual( [0.0_wp,0.0_wp,0.0_wp,0.0_wp,0.0_wp] , rhs%a(1,:) )

 end subroutine compute_1d_scalar_adv_rhs__y_const__rhs_is_zero_p0

!-----------------------------------------------------------------------

 @Test
 subroutine compute_1d_scalar_adv_rhs__y_const__rhs_is_zero_p1()

  type(t_1d_scalar_adv_state) :: y, rhs

   allocate(y%a(1,ode_p0%grid%ne))
   y%a = 1.0_wp
   allocate(rhs%a(1,ode_p0%grid%ne))

   call ode_p0%compute_rhs(rhs,y)

   @assertEqual( [0.0_wp,0.0_wp,0.0_wp,0.0_wp,0.0_wp] , rhs%a(1,:) )

 end subroutine compute_1d_scalar_adv_rhs__y_const__rhs_is_zero_p1

!-----------------------------------------------------------------------

 @Test
 subroutine compute_1d_scalar_adv_rhs__y_not_const__rhs_not_zero_p0()

  type(t_1d_scalar_adv_state) :: y, rhs

   allocate(y%a(1,ode_p0%grid%ne))
   y%a(1,:) = [1.0_wp,2.0_wp,3.0_wp,2.0_wp,1.0_wp]
   allocate(rhs%a(1,ode_p0%grid%ne))

   call ode_p0%compute_rhs(rhs,y)

   @assertFalse( all( rhs%a .eq. 0.0_wp ) )

 end subroutine compute_1d_scalar_adv_rhs__y_not_const__rhs_not_zero_p0

!-----------------------------------------------------------------------

 @Test
 subroutine compute_1d_scalar_adv_rhs__corr_flux__for_riemann_pb_p0()

  ! We consider an initial condition
  !
  !   1   1   0   0   1
  ! x---x---x---x---x---
  !
  ! for which we have two discontinuities. The resulting flux can be
  ! computed exactly for each element, then divided by the element
  ! area to obtain the expected right-hand-side.
  !
  !  el1 -> inflow = u*1; outflow = u*1 -> net flux is 0
  !  el2 -> inflow = u*1; outflow = u*1 -> net flux is 0
  !  el3 -> inflow = u*1; outflow = 0
  !  el4 -> inflow = 0;   outflow = 0
  !  el5 -> inflow = 0;   outflow = u*1

  real(wp), parameter :: h = 1.0_wp/5.0_wp ! element area
  real(wp), parameter :: rhs_e3 =  u*1.0_wp/h
  real(wp), parameter :: rhs_e5 = -u*1.0_wp/h
  real(wp), parameter :: rhs_ex(1,5) = &   ! expected result
    reshape( [0.0_wp,0.0_wp,rhs_e3,0.0_wp,rhs_e5] , [1,5] )

  type(t_1d_scalar_adv_state) :: y, rhs

   allocate(y%a(1,ode_p0%grid%ne))
   y%a(1,:) = [1.0_wp,1.0_wp,0.0_wp,0.0_wp,1.0_wp]
   allocate(rhs%a(1,ode_p0%grid%ne))

   call ode_p0%compute_rhs(rhs,y)

   @assertEqual( rhs_ex , rhs%a , tolerance=toll)

 end subroutine compute_1d_scalar_adv_rhs__corr_flux__for_riemann_pb_p0

!-----------------------------------------------------------------------

 @Test
 subroutine compute_1d_scalar_adv_rhs__check_upwind_flux()
  real(wp) :: upwflux

   upwflux = upwind_flux( u=1.0_wp , n=1.0_wp , &
                    flux1=3.3_wp , flux2=2.2_wp )
   @assertEqual( 3.3_wp , upwflux )

   upwflux = upwind_flux( u=-1.0_wp , n=1.0_wp , &
                     flux1=3.3_wp , flux2=2.2_wp )
   @assertEqual( 2.2_wp , upwflux )

 end subroutine compute_1d_scalar_adv_rhs__check_upwind_flux

!-----------------------------------------------------------------------

 @Test
 subroutine element_mass_matrix__correct_for_degree_0()
  type(t_base) :: base

  ! For phi=0 the mass matrix has one entry equal to the element area
  real(wp) :: m(1,1)    ! computed mas matrix
  real(wp) :: m_ex(1,1) ! exact mass matrix

   call base_setup(base,degree=0)
   associate( e => ode_p0%grid%e(1) ) ! choose an element

   m_ex(1,1) = e%area

   call element_mass_matrix( m , base,e )

   @assertEqual( m_ex , m , tolerance=toll)

   end associate
   call base_clean(base)

 end subroutine element_mass_matrix__correct_for_degree_0

!-----------------------------------------------------------------------

 @Test
 subroutine element_mass_matrix__correct_for_degree_1()
  type(t_base) :: base

  real(wp) :: m(2,2)    ! computed mas matrix
  real(wp) :: m_ex(2,2) ! exact mass matrix

   call base_setup(base,degree=1)
   associate( e => ode_p1%grid%e(1) ) ! choose an element

   m_ex(1,1) = 1.0_wp/15.0_wp
   m_ex(1,2) = 1.0_wp/30.0_wp
   m_ex(2,1) = 1.0_wp/30.0_wp
   m_ex(2,2) = 1.0_wp/15.0_wp

   call element_mass_matrix( m , base,e )

   @assertEqual( m_ex , m , tolerance=toll)

   end associate
   call base_clean(base)

 end subroutine element_mass_matrix__correct_for_degree_1

!-----------------------------------------------------------------------

 @Test
 subroutine element_integral__ak_vanishes__e_rhs_unchanged_deg0()
  type(t_base) :: base
  real(wp) :: e_rhs(1)

   call base_setup(base,degree=0)
   e_rhs = 7.3_wp ! some arbitrary value

   call element_integral(e_rhs, base,ak=[0.0_wp])

   @assertEqual( [7.3_wp] , e_rhs , tolerance=toll)

   call base_clean(base)

 end subroutine element_integral__ak_vanishes__e_rhs_unchanged_deg0

!-----------------------------------------------------------------------

 @Test
 subroutine element_integral__ak_vanishes__e_rhs_unchanged_deg1()
  type(t_base) :: base
  real(wp) :: e_rhs(2)

   call base_setup(base,degree=1)
   e_rhs = 7.3_wp ! some arbitrary value

   call element_integral(e_rhs, base,ak=[0.0_wp,0.0_wp])

   @assertEqual( [7.3_wp,7.3_wp] , e_rhs , tolerance=toll)

   call base_clean(base)

 end subroutine element_integral__ak_vanishes__e_rhs_unchanged_deg1

!-----------------------------------------------------------------------

 @Test
 subroutine element_integral__ak_one__e_rhs_correct_deg1()
  type(t_base) :: base
  real(wp) :: e_rhs(2)

  ! Since ak is constant and equal to 1, we expect the itegral to be
  !
  !  u*( phi_i(1)-phi_i(-1) )
  !
  ! for each phi_i. Given that 
  !
  !   phi_1 = 0.5*(1-x)
  !   phi_2 = 0.5*(1+x)
  !
  ! we expect values  u*(0-1)=-u  and  u*(1-0)=u .
  !
  ! According to the documentation, these values should be added to
  ! the previous value of e_rhs.

   call base_setup(base,degree=1)
   e_rhs = 7.3_wp ! some arbitrary value

   call element_integral(e_rhs, base,ak=[1.0_wp,1.0_wp])

   @assertEqual( 7.3_wp+[-u,+u] , e_rhs , tolerance=toll)

   call base_clean(base)

 end subroutine element_integral__ak_one__e_rhs_correct_deg1

!-----------------------------------------------------------------------

 @Test
 subroutine element_integral__ak_linear__e_rhs_correct_deg1()
  type(t_base) :: base
  real(wp) :: e_rhs(2)

  ! We consider here the special case  ak = x  for which the integral
  ! should vanish.

   call base_setup(base,degree=1)
   e_rhs = 7.3_wp ! some arbitrary value

   call element_integral(e_rhs, base,ak=[-1.0_wp,1.0_wp])

   @assertEqual( [7.3_wp,7.3_wp] , e_rhs , tolerance=toll)

   call base_clean(base)

 end subroutine element_integral__ak_linear__e_rhs_correct_deg1

!-----------------------------------------------------------------------

 @Test
 subroutine side_integral__ak_vanishes__s_rhs_unchanged_deg0()
  type(t_base) :: base
  real(wp) :: s_rhs1(1), s_rhs2(1)

   call base_setup(base,degree=0)
   associate( s => ode_p0%grid%s(1) ) ! choose a side

   s_rhs1 = 7.3_wp ! some arbitrary value
   s_rhs2 = 0.0_wp

   call side_integral(s_rhs1,s_rhs2, base,s,ak1=[0.0_wp],ak2=[0.0_wp])

   @assertEqual( [7.3_wp] , s_rhs1 , tolerance=toll)
   @assertEqual( [0.0_wp] , s_rhs2 , tolerance=toll)

   end associate
   call base_clean(base)

 end subroutine side_integral__ak_vanishes__s_rhs_unchanged_deg0

!-----------------------------------------------------------------------

 @Test
 subroutine side_integral__ak_vanishes__s_rhs_unchanged_deg1()
  type(t_base) :: base
  real(wp) :: s_rhs1(2), s_rhs2(2)

   call base_setup(base,degree=1)
   associate( s => ode_p1%grid%s(1) ) ! choose a side

   s_rhs1 = 7.3_wp ! some arbitrary value
   s_rhs2 = 0.0_wp

   call side_integral(s_rhs1,s_rhs2, base,s, &
      ak1=[0.0_wp,0.0_wp],ak2=[0.0_wp,0.0_wp])

   @assertEqual( [7.3_wp,7.3_wp] , s_rhs1 , tolerance=toll)
   @assertEqual( [0.0_wp,0.0_wp] , s_rhs2 , tolerance=toll)

   end associate
   call base_clean(base)

 end subroutine side_integral__ak_vanishes__s_rhs_unchanged_deg1

!-----------------------------------------------------------------------

 @Test
 subroutine side_integral__ak_1_0__correct_upwind_flux_is_used_p0()
  type(t_base) :: base
  real(wp) :: s_rhs1(1), s_rhs2(1)

  ! We consider here the case of constant functions, with 1 on the
  ! left element and 0 on the right element. Since u>0 the flux should
  ! be computed using the uwind value 1.

   call base_setup(base,degree=0)
   associate( s => ode_p0%grid%s(1) ) ! choose a side

   s_rhs1 = 0.0_wp
   s_rhs2 = 0.0_wp

   call side_integral(s_rhs1,s_rhs2, base,s,ak1=[1.0_wp],ak2=[0.0_wp])

   @assertEqual( [-u*1.0_wp ] , s_rhs1 , tolerance=toll)
   @assertEqual( [+u*1.0_wp ] , s_rhs2 , tolerance=toll)

   end associate
   call base_clean(base)

 end subroutine side_integral__ak_1_0__correct_upwind_flux_is_used_p0

!-----------------------------------------------------------------------

 @Test
 subroutine side_integral__ak_1_0__correct_upwind_flux_is_used_p1()
  type(t_base) :: base
  real(wp) :: s_rhs1(2), s_rhs2(2)

  ! We consider here the case of constant functions, with 1 on the
  ! left element and 0 on the right element. Since u>0 the flux should
  ! be computed using the uwind value 1.
  !
  ! Notice that the exact values also depend on the values of phi_i on
  ! the side.
  real(wp), parameter :: s_rhs1_ex(2) = [0.0_wp,-u*1.0_wp]
  real(wp), parameter :: s_rhs2_ex(2) = [+u*1.0_wp,0.0_wp]

   call base_setup(base,degree=1)
   associate( s => ode_p1%grid%s(1) ) ! choose a side

   s_rhs1 = 0.0_wp
   s_rhs2 = 0.0_wp

   call side_integral(s_rhs1,s_rhs2, base,s, &
      ak1=[1.0_wp,1.0_wp],ak2=[0.0_wp,0.0_wp])

   @assertEqual( s_rhs1_ex , s_rhs1 , tolerance=toll)
   @assertEqual( s_rhs2_ex , s_rhs2 , tolerance=toll)

   end associate
   call base_clean(base)

 end subroutine side_integral__ak_1_0__correct_upwind_flux_is_used_p1

!-----------------------------------------------------------------------

 @Test
 subroutine side_integral__ak_lin__correct_upwind_flux_is_used_p1()
  type(t_base) :: base
  real(wp) :: s_rhs1(2), s_rhs2(2)

  ! This test is very similar to
  ! side_integral__ak_1_0__correct_upwind_flux_is_used_p1, except that
  ! the solution ak1,ak2 is not constant on the two elements.
  ! Nevertheless, the solution on the side has the two values 1,0 so
  ! that the resulting side_integrals are the same.
  real(wp), parameter :: s_rhs1_ex(2) = [0.0_wp,-u*1.0_wp]
  real(wp), parameter :: s_rhs2_ex(2) = [+u*1.0_wp,0.0_wp]

   call base_setup(base,degree=1)
   associate( s => ode_p1%grid%s(1) ) ! choose a side

   s_rhs1 = 0.0_wp
   s_rhs2 = 0.0_wp

   call side_integral(s_rhs1,s_rhs2, base,s, &
      ak1=[5.0_wp,1.0_wp],ak2=[0.0_wp,-5.0_wp])

   @assertEqual( s_rhs1_ex , s_rhs1 , tolerance=toll)
   @assertEqual( s_rhs2_ex , s_rhs2 , tolerance=toll)

   end associate
   call base_clean(base)

 end subroutine side_integral__ak_lin__correct_upwind_flux_is_used_p1

!-----------------------------------------------------------------------

end module mod_1d_scalar_adv_ode_tests

