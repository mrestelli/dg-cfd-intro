module mod_linalg

 use mod_kinds, only: wp

 implicit none

 public :: invmat_chol

 private

contains
 
 !> Compute \(A^{-1}\) with the Cholesky factorization.
 !!
 !! *\(A\) must be symmetric positive definite for this to make
 !! sense!*
 !!
 !! The matrix is factored as 
 !! \[
 !!  A = L\,L^T
 !! \]
 !! and then \(n\) linear systems are solved. We take advantage of the
 !! fact that \(A^{-1}\) is also symmetric.
 pure subroutine invmat_chol(a,ai)
  real(wp), intent(in)  :: a(:,:)  !! \(A\)
  real(wp), intent(out) :: ai(:,:) !! \(A^{-1}\)

  integer :: i, j, n
  real(wp), dimension(size(a,1),size(a,2)) :: l, y

   n = size(a,1)
 
   !1) Cholesky factorization
   l = a
   do i=1,n-1
     l(i,i) = sqrt(l(i,i));
     l(i+1:n,i) = l(i+1:n,i)/l(i,i);
     do j=i+1,n
       l(j:n,j) = l(j:n,j) - l(j:n,i)*l(j,i);
     enddo
   enddo
   l(n,n) = sqrt(l(n,n));
   ! notice that only the lower triangular part of l is meaningful

   !2) solution of the triangular systems
   y = 0.0_wp
   do i=1,n ! loop on the rows
     y(i,i) = 1.0_wp/l(i,i)
     do j=1,i-1
       y(i,j) = -y(i,i)*dot_product( l(i,j:i-1) , y(j:i-1,j)  )
     enddo
   enddo
   
   do i=n,1,-1 ! loop on the rows
     do j=1,i-1
       ai(i,j) = y(i,i)*(y(i,j) - dot_product(l(i+1:n,i) , ai(i+1:n,j)))
       ai(j,i) = ai(i,j)
     enddo
     ai(i,i) = y(i,i)*(y(i,i) - dot_product( l(i+1:n,i) , ai(i+1:n,i) ))
   enddo

 end subroutine invmat_chol
 
end module mod_linalg

