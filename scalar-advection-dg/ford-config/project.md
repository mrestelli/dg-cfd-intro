project: DG &ndash; scalar advection equation
summary: DG discretization of the scalar advection equation
author: Marco Restelli
email: mrestelli@gmail.com
website: http://www2.ipp.mpg.de/~marre
bitbucket: https://bitbucket.org/mrestelli
project_bitbucket: https://bitbucket.org/mrestelli/dg-cfd-intro
license: by
src_dir: ./../src
output_dir: ./../doc
exclude_dir: ./../doc
             ./../doc/src
mathjax_config: ./../ford-config/MathJax-latex-macros.js
predocmark: >
display: public
         protected
         private
source: false
graph: true
search: true
macro: TEST
       LOGIC=.true.

Solves the scalar advection equation
\begin{equation}
\partial_t a + \Div(\V{u} a) = 0
\label{eq:scalar-adv}
\end{equation}
with the Discontinuous Galerkin (DG) discretization
\begin{equation}
\int_K \partial_t a_h\varphi_h -\int_K\V{u}a_h\cdot\Grad\varphi_h
+ \int_{\partial K} \widehat{\V{u}a}\cdot\V{n} \varphi_h = 0.
\label{eq:scalar-adv-DG}
\end{equation}

