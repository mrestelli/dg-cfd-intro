module mod_1d_scalar_adv_ode

 use mod_kinds, only: wp

 use mod_ode_state, only: c_ode_state

 use mod_ode_solver, only: c_ode

 use mod_linalg, only: invmat_chol

 use mod_1d_grid, only: t_grid, t_s, t_e, grid_setup, grid_clean

 use mod_1d_base, only: t_base, base_setup, base_clean

 use mod_1d_scalar_adv_state, only: t_1d_scalar_adv_state

 implicit none

 public :: &
   t_1d_scalar_adv_ode, scalar_adv_ode_setup, scalar_adv_ode_clean, u, &
   element_mass_matrix, element_integral, side_integral, upwind_flux

 private

 !> Compute the rhs resulting from the DG discretization of
 !! \[
 !!  \partial_t a +\partial_x( \V{u}a ) = 0
 !! \]
 !! where u is a constant.
 type, extends(c_ode) :: t_1d_scalar_adv_ode
  type(t_grid) :: grid
  type(t_base) :: base
  !> \(\left(M^K\right)^{-1}\) for all the elements
  real(wp), allocatable :: imass_matrices(:,:,:)
 contains
  procedure, pass(ode) :: compute_rhs => compute_1d_scalar_adv_rhs
 end type t_1d_scalar_adv_ode

 real(wp), parameter :: u = 1.5_wp ! advection coefficient

contains

!-----------------------------------------------------------------------

 subroutine scalar_adv_ode_setup(ode, ne,x_left,x_right,degree)
  integer,  intent(in) :: ne              ! number of elements
  real(wp), intent(in) :: x_left, x_right ! domain size
  integer,  intent(in) :: degree          ! polynomial degree
  type(t_1d_scalar_adv_ode), intent(out) :: ode

  integer :: ie
  real(wp), allocatable :: m(:,:) ! work array for mass matrix

   call grid_setup(ode%grid,ne,x_left,x_right)
   call base_setup(ode%base,degree)

   ! precompute the mass matrices
   allocate(m(ode%base%n,ode%base%n))
   allocate( ode%imass_matrices(ode%base%n,ode%base%n,ode%grid%ne) )
   do ie=1,ode%grid%ne
     ! mass matrix
     call element_mass_matrix( m, ode%base, ode%grid%e(ie) )
     ! invert the mass matrix
     call invmat_chol( m , ode%imass_matrices(:,:,ie) )
   enddo
   deallocate(m)

 end subroutine scalar_adv_ode_setup

 subroutine scalar_adv_ode_clean(ode)
  type(t_1d_scalar_adv_ode), intent(inout) :: ode

   deallocate( ode%imass_matrices )
   call base_clean(ode%base)
   call grid_clean(ode%grid)

 end subroutine scalar_adv_ode_clean

!-----------------------------------------------------------------------

 subroutine compute_1d_scalar_adv_rhs(rhs,ode,y)
  class(t_1d_scalar_adv_ode), intent(in) :: ode
  class(c_ode_state), intent(in)    :: y
  class(c_ode_state), intent(inout) :: rhs

  integer :: ie ! element index
  integer :: is ! side index
  integer :: e1, e2

   select type(y);   type is(t_1d_scalar_adv_state)
   select type(rhs); type is(t_1d_scalar_adv_state)

   ! initialize rhs
   rhs%a = 0.0_wp

   do ie=1,ode%grid%ne
     call element_integral(rhs%a(:,ie), ode%base,y%a(:,ie))
   enddo

   ! side fluxes
   do is=1,ode%grid%ns
     associate( side => ode%grid%s(is) )
     e1 = side%e1
     e2 = side%e2
     call side_integral( s_rhs1=rhs%a(:,e1) , s_rhs2=rhs%a(:,e2), &
           base=ode%base , s=side , ak1=y%a(:,e1) , ak2=y%a(:,e2) )
     end associate
   enddo

   ! include the mass matrix
   do ie=1,ode%grid%ne
     associate( elem => ode%grid%e(ie) )
     rhs%a(:,ie) = matmul(ode%imass_matrices(:,:,ie) , rhs%a(:,ie))
     end associate
   enddo

   end select
   end select
 end subroutine compute_1d_scalar_adv_rhs

!-----------------------------------------------------------------------

 !> Compute the element integral
 !! \[
 !!  M^K_{ij} = \int_K \varphi_i^K \varphi_j^K
 !! \]
 !! for every element \(K\in\mathcal{T}_h\).
 !!
 !! Since \(K\) is a one-dimensional element, the integral is computed
 !! as
 !! \[
 !! \int_K \varphi_i^K \varphi_j^K =
 !! \frac{|K|}{|\tilde{K}|}\int_{\tilde{K}} \tilde{\varphi}_i
 !! \tilde{\varphi}_j 
 !! \approx 
 !! \frac{|K|}{|\tilde{K}|} \sum_{l=1}^m \tilde{w}^Q_l\, 
 !! \tilde{\varphi}_i (\tilde{x}^Q_l)\,
 !! \tilde{\varphi}_j (\tilde{x}^Q_l).
 !! \]
 pure subroutine element_mass_matrix(m, base,e)
  type(t_base), intent(in)  :: base  !! base and quadrature formula
  type(t_e),    intent(in)  :: e     !! element \(K\)
  real(wp),     intent(out) :: m(:,:)

 end subroutine element_mass_matrix

!-----------------------------------------------------------------------

 !> Compute the element integral
 !! \[
 !!  \int_K \V{u}a_h|_K \cdot\Grad\varphi_i^K,
 !! \]
 !! for every basis function \(\varphi_i^K\in V_h(K)\) and for a given
 !! element \(K\in\mathcal{T}_h\).
 !!
 !! Since \(K\) is a one-dimensional element, the integral is computed
 !! as
 !! \[
 !! \int_K ua_h|_K \partial_x\varphi_i^K =
 !! \frac{|K|}{|\tilde{K}|}\int_{\tilde{K}} ua_h |_{\tilde{K}} \,
 !! \frac{|\tilde{K}|}{|K|}\partial_{\tilde{x}} \tilde{\varphi}_i 
 !! \approx 
 !! \sum_{l=1}^m \tilde{w}^Q_l\, ua_h(\tilde{x}^Q_l) \,
 !! \partial_{\tilde{x}}\tilde{\varphi}_i (\tilde{x}^Q_l),
 !! \]
 !! using the fact that
 !! \[
 !!  a_h(\tilde{x}^Q_l) =
 !!  \sum_{j=1}^na^K_j\tilde{\varphi}_j(\tilde{x}^Q_l).
 !! \]
 !!
 !! The element integral is *added* to ```e_rhs```.
 pure subroutine element_integral(e_rhs, base,ak)
  type(t_base), intent(in)    :: base  !! base and quadrature formula
  real(wp),     intent(in)    :: ak(:) !! \(a^K_i, \quad i=1,\ldots,n\)
  real(wp),     intent(inout) :: e_rhs(:)

 end subroutine element_integral

!-----------------------------------------------------------------------

 !> Compute the side integrals
 !! \[
 !!  -\int_{e} \widehat{\V{u}a} \cdot\sigma_{K,e}\V{n}_e \varphi_i^K,
 !! \]
 !! for every basis function \(\varphi_i^K\in V_h(K)\), for \(
 !! K=K_1(e),K_2(e) \) and for a given
 !! side \(e\in\mathcal{E}_h\).
 !!
 !! In one spatial dimension, the sides reduce to points so that the
 !! integral yields
 !! \[
 !!  - \sigma_{K,e} \, \widehat{ua} \, \varphi_i^K.
 !! \]
 !!
 !! Recall that
 !! \[
 !!  \sigma_{K_1(e),e} = 1, \qquad
 !!  \sigma_{K_2(e),e} = -1.
 !! \]
 !! The side integrals are *added* to ```e_rhs1``` and ```e_rhs2```.
 pure subroutine side_integral(s_rhs1,s_rhs2, base,s,ak1,ak2)
  !> base and quadrature formula
  type(t_base), intent(in)    :: base
  !> side \( e \)
  type(t_s),    intent(in)    :: s
  !> \(a^{K_1}_i, \quad i=1,\ldots,n\)
  real(wp),     intent(in)    :: ak1(:)
  !> \(a^{K_2}_i, \quad i=1,\ldots,n\)
  real(wp),     intent(in)    :: ak2(:)
  !> \( - \sigma_{K_1(e),e} \, \widehat{ua} \, \varphi_i^{K_1(e)} \)
  real(wp),     intent(inout) :: s_rhs1(:)
  !> \( - \sigma_{K_2(e),e} \, \widehat{ua} \, \varphi_i^{K_2(e)} \)
  real(wp),     intent(inout) :: s_rhs2(:)

 end subroutine side_integral

!-----------------------------------------------------------------------

 pure function upwind_flux(u,n,flux1,flux2) result(upwflux)
  real(wp), intent(in) :: u     ! advection velocity
  real(wp), intent(in) :: n     ! unit normal form e1 to e2
  real(wp), intent(in) :: flux1 ! flux on e1
  real(wp), intent(in) :: flux2 ! flux on e2
  real(wp) :: upwflux
   if( u*n .ge. 0 ) then
     upwflux = flux1
   else
     upwflux = flux2
   endif
   
 end function upwind_flux

!-----------------------------------------------------------------------

end module mod_1d_scalar_adv_ode
