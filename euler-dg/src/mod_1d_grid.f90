module mod_1d_grid

 use mod_kinds, only: wp
 
 implicit none

 public :: t_grid, grid_setup, grid_clean, t_s, t_e

 private

 type :: t_s
  integer  :: e_L ! index of left connected element
  integer  :: e_R ! index of right connected element
  integer  :: iloc_L ! local side index on e_L (1 or 2)
  integer  :: iloc_R ! local side index on e_R (1 or 2)
  real(wp) :: n  ! normal unit vector from e_L to e_R
 end type t_s

 type :: t_e
  real(wp) :: xb    ! center of the element
  real(wp) :: xv(2) ! vertexes of the element
  real(wp) :: area
  integer  :: s(2)  ! side index of first (xi=-1) and second (xi=+1) element side
  integer  :: LR(2) ! element side is left(=1) or right(=2) state of side
 end type t_e

 type :: t_grid
  integer :: ns ! number of sides
  integer :: ne ! number of elements
  type(t_s), allocatable :: s(:) ! sides
  type(t_e), allocatable :: e(:) ! elements
 end type t_grid

contains

 ! Setup the module variable grid
 subroutine grid_setup(grid, ne,x_left,x_right)
  integer,      intent(in) :: ne
  real(wp),     intent(in) :: x_left, x_right ! domain size
  type(t_grid), intent(out) :: grid

  integer :: is, ie
  integer,parameter :: LEFT=1
  integer,parameter :: RIGHT=2

   grid%ns = ne !periodic sides
   grid%ne = ne

   ! allocate and define the side array
   allocate( grid%s( grid%ns ) )
   ! first side: periodic bcs
   associate( side => grid%s(1) )
   side%e_L = grid%ne
   side%e_R = 1
   side%iloc_L = 2
   side%iloc_R = 1
   side%n  = 1.0_wp
   end associate
   do is=2,grid%ns
     associate( side => grid%s(is) )
     side%e_L = is-1
     side%e_R = is
     side%iloc_L = 2
     side%iloc_R = 1
     side%n  = 1.0_wp
     end associate
   end do

   ! allocate and define the element array
   allocate( grid%e( grid%ne ) )
   do ie=1,grid%ne
     associate( elem => grid%e(ie) )
     elem%area = (x_right-x_left)/real(grid%ne,wp)
     elem%xb   = x_left + &
       (0.5_wp+real(ie-1,wp))*(x_right-x_left)/real(grid%ne,wp)
     elem%xv(1) = x_left + &
       real(ie-1,wp)*(x_right-x_left)/real(grid%ne,wp)
     elem%xv(2) = x_left + &
       real(ie  ,wp)*(x_right-x_left)/real(grid%ne,wp)
     end associate
   end do
   do is=1,grid%ns
     associate( side => grid%s(is) )
     grid%e(side%e_L)%s(side%iloc_L)  = is
     grid%e(side%e_L)%LR(side%iloc_L) = LEFT
     grid%e(side%e_R)%s(side%iloc_R)  = is
     grid%e(side%e_R)%LR(side%iloc_R) = RIGHT
     end associate
   end do

 end subroutine grid_setup
 
 subroutine grid_clean(grid)
  type(t_grid), intent(inout) :: grid
   deallocate( grid%s )
   deallocate( grid%e )
 end subroutine grid_clean

end module mod_1d_grid
