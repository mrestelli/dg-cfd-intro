module mod_1d_base

 use mod_kinds, only: wp

 implicit none

 public :: t_base, base_setup, base_clean

 private

 !> Local base on \( \tilde{K} = (-1\,,1) \).
 type :: t_base
  !> number of base functions
  integer :: n
  !> number of quadrature nodes
  integer :: m
  
  real(wp), allocatable :: xq(:)     !! \( \tilde{x}^Q_l \)
  real(wp), allocatable :: wq(:)     !! \( \tilde{w}^Q_l \)
                                     
  real(wp), allocatable :: phi(:,:)  !! \( \tilde{\varphi}_i(\tilde{x}^Q_l) \)
  real(wp), allocatable :: dphi(:,:) !! \( \partial_{\tilde{x}}\tilde{\varphi}_i(\tilde{x}^Q_l) \)

  real(wp), allocatable :: phib(:,:) !! \( \tilde{\varphi}_i(\partial\tilde{K}) \)
                                     !!
                                     !! In particular, 
                                     !! \[
                                     !!  \mathrm{phib(:,1)} = \tilde{\varphi}_i(\tilde{e}_1), \qquad
                                     !!  \mathrm{phib(:,2)} = \tilde{\varphi}_i(\tilde{e}_2).
                                     !! \]
  real(wp) :: e_ref_area             !! \(|\tilde{K}|\)
 end type t_base

contains

 subroutine base_setup(base,degree)
  integer,      intent(in)  :: degree !! polynomial degree
  type(t_base), intent(out) :: base


   select case(degree)
    case(0)
     base%n = degree+1
     base%m = degree+1
     allocate(base%xq(         base%m))
     allocate(base%wq(         base%m))
     allocate(base%phi( base%n,base%m))
     allocate(base%dphi(base%n,base%m))
     allocate(base%phib(base%n,2))
     base%e_ref_area = 2.0_wp
     base%xq = [0.0_wp]
     base%wq = [2.0_wp]
     base%phi(1,:)  = [1.0_wp]
     base%dphi(1,:) = [0.0_wp]
     base%phib(:,1) = 1.0_wp ! phi is constant and equal to 1
     base%phib(:,2) = 1.0_wp

    case(1)
     base%n = degree+1
     base%m = degree+1
     allocate(base%xq(         base%m))
     allocate(base%wq(         base%m))
     allocate(base%phi( base%n,base%m))
     allocate(base%dphi(base%n,base%m))
     allocate(base%phib(base%n,2))
     base%e_ref_area = 2.0_wp
     base%xq = [-1.0_wp/sqrt(3.0_wp) , +1.0_wp/sqrt(3.0_wp)]
     associate ( xq => base%xq )
     base%wq = [1.0_wp,1.0_wp]
     base%phi(1,:)  = 0.5_wp*(1.0_wp-xq) ! 0.5*(1-x)
     base%phi(2,:)  = 0.5_wp*(1.0_wp+xq) ! 0.5*(1+x)
     base%dphi(1,:) = [-0.5_wp,-0.5_wp]
     base%dphi(2,:) = [ 0.5_wp, 0.5_wp]
     end associate !xq
     associate( xb => [-1.0_wp , +1.0_wp] ) ! boundary of tilde{K}
     base%phib(1,:) = 0.5_wp*(1.0_wp-xb)
     base%phib(2,:) = 0.5_wp*(1.0_wp+xb)
     end associate !xb
    case default
     error stop "Not implemented"

   end select

 end subroutine base_setup

 pure subroutine base_clean(base)
  type(t_base), intent(inout) :: base
   deallocate( base%wq )
   deallocate( base%phi )
   deallocate( base%dphi )
   deallocate( base%phib )
 end subroutine base_clean

end module mod_1d_base

