!> 
!!# Module 1D Euler ODE 
!!
!!
!!
!! Compute the rhs resulting from the DG discretization of the 1D Euler equations
!! \[
!!  \partial_t U +\partial_x  F_\text{euler}(U ) = 0
!! \]
!! where \(U\) is the vector of 1D conservative variables \(U=(\rho , \rho v, E)^T\) 
!!
!!
!!
module mod_1d_euler_ode

 use mod_kinds,          only: wp
 use mod_ode_state,      only: c_ode_state
 use mod_ode_solver,     only: c_ode
 use mod_linalg,         only: invmat_chol
 use mod_1d_grid,        only: t_grid, t_s, t_e, grid_setup, grid_clean
 use mod_1d_base,        only: t_base, base_setup, base_clean
 use mod_1d_euler_state, only: t_1d_euler_state

 implicit none

 public :: &
  t_1d_euler_ode       &
  ,euler_ode_setup     &
  ,euler_ode_clean     &
  ,prolong_to_side     &
  ,eval_1d_euler_flux  &
  ,numerical_flux      &
  ,side_integral       &
  ,element_integral    &
  ,element_mass_matrix

 public :: nvar,var_names,gamma

 private

!-----------------------------------------------------------------------------

!module variables

 type, extends(c_ode) :: t_1d_euler_ode
  type(t_grid) :: grid
  type(t_base) :: base 
 
  real(wp), allocatable :: imass_matrices(:,:,:) !! \(\left(M^K\right)^{-1}\) for all the elements 
                                                 !! and each variable, same as scalar advection

 contains
  procedure, pass(ode) :: compute_rhs => compute_1d_euler_rhs
 end type t_1d_euler_ode

 integer, parameter :: nvar=3                !! number of equations/variables in the equation system, 
                                             !! in 1D: \( (\rho, \rho v, E  )^T \), 
                                             !! used as first dimension in state vector
 !> names of the variables in 1D 
 character(len=20)  :: var_names(3) = [ character(len=20) :: &
                                        '"Density"'        , &
                                        '"MomentumX"'      , &
                                        '"TotalEnergy"'      ]
 real(wp)            :: gamma=1.4_wp          !! isentropic exponent \(\gamma\)  

!-----------------------------------------------------------------------------

contains


 !> initialize the euler ode class 
 !! 
 subroutine euler_ode_setup(ode, ne,x_left,x_right,degree)
  !---------------------------------------------------------------------------
  !input/output variables
  integer,  intent(in) :: ne              ! number of elements
  real(wp), intent(in) :: x_left, x_right ! domain size
  integer,  intent(in) :: degree          ! polynomial degree
  type(t_1d_euler_ode), intent(out) :: ode
  !---------------------------------------------------------------------------
  !local variables
  integer :: ie
  real(wp), allocatable :: m(:,:) ! work array for mass matrix
  !---------------------------------------------------------------------------

   call grid_setup(ode%grid,ne,x_left,x_right)
   call base_setup(ode%base,degree)

   ! precompute the mass matrices
   allocate(m(ode%base%n,ode%base%n))
   allocate( ode%imass_matrices(ode%base%n,ode%base%n,ode%grid%ne) )
   do ie=1,ode%grid%ne
     ! mass matrix
     call element_mass_matrix( m, ode%base, ode%grid%e(ie) )
     ! invert the mass matrix
     call invmat_chol( m , ode%imass_matrices(:,:,ie) )
   end do !
   deallocate(m)

 end subroutine euler_ode_setup

!-----------------------------------------------------------------------------

 !> clean the euler ode class 
 !! 
 subroutine euler_ode_clean(ode)
  !---------------------------------------------------------------------------
  !input/output variables
  type(t_1d_euler_ode), intent(inout) :: ode
  !---------------------------------------------------------------------------

   deallocate( ode%imass_matrices )
   call base_clean(ode%base)
   call grid_clean(ode%grid)

 end subroutine euler_ode_clean

!-----------------------------------------------------------------------------

 !> Compute the rhs resulting from the DG discretization of the 1D Euler equations
 !! \[
 !!  \partial_t U +\partial_x  F_\text{euler}(U ) = 0
 !! \]
 !! where U is the vector of 1D conservative variables \(U=(\rho , \rho v, E)^T\) 
 !!  
 subroutine compute_1d_euler_rhs(rhs,ode,y)
  !---------------------------------------------------------------------------
  !input/output variables
  class(t_1d_euler_ode), intent(in) :: ode
  class(c_ode_state), intent(in)    :: y
  class(c_ode_state), intent(inout) :: rhs
  !---------------------------------------------------------------------------
  !local variables
  real(wp) :: U_s(nvar,2,ode%grid%ns) !! local solution of left(1) and right(2) element on each side
  real(wp) :: flux(nvar,ode%grid%ns) !! local flux f(U_L,U_R), on all side gausspoints 
                                     !! (only 1 gausspoint per side in 1D)
  integer :: ie      ! element index
  integer :: is      ! side index
  integer :: ivar    ! equation variable index
  integer :: e_L,e_R ! left and right element index
  integer,parameter:: LEFT=1
  integer,parameter:: RIGHT=2
  !---------------------------------------------------------------------------

   select type(y);   type is(t_1d_euler_state)
   select type(rhs); type is(t_1d_euler_state)

   associate( ns => ode%grid%ns, ne => ode%grid%ne)

   ! initialize rhs
   rhs%U = 0.0_wp

   ! interpolate left and right element solution at each side
   do is=1,ns
     associate( side => ode%grid%s(is) )
       e_L = side%e_L
       e_R = side%e_R
       call prolong_to_side(U_s(:,LEFT,is),U_s(:,RIGHT,is), &
                            ode%base,side,y%U(:,:,e_L),y%U(:,:,e_R))
     end associate !side 
   end do !is=1,ns

   ! compute all side fluxes (no boundary conditions yet, since periodic connectivity)
   do is=1,ns
     flux(:,is)= numerical_flux(U_s(:,LEFT,is),U_s(:,RIGHT,is))
   end do !is=1,ns


   ! evaluate side integral for each side

     !TODO : PUT YOUR CODE HERE


   ! evaluate the volume integral for each element, if base%n>1
   if(ode%base%n.GT.1) then 

     !TODO : PUT YOUR CODE HERE

   end if !ode%base%n > 1

   ! include the mass matrix, apply the inverse to each variable!
   do ie=1,ne
     do ivar=1,nvar
       rhs%U(ivar,:,ie) = matmul(ode%imass_matrices(:,:,ie) , rhs%U(ivar,:,ie))
     end do !ivar=1,nvar
   end do !ie=1,ne
   
   end associate !ns, ne

   end select ! type(y);   type is(t_1d_euler_state)
   end select ! type(rhs); type is(t_1d_euler_state)

 end subroutine compute_1d_euler_rhs

!-----------------------------------------------------------------------------


 !> Evaluate the two solutions at side \(e=e_L(K),e_R(K)\)
 !! \[
 !! U_L = \sum_j (U_h)_j \varphi^{K_L}_j(\xi_{K_L,e}) \\
 !! U_R = \sum_j (U_h)_j \varphi^{K_R}_j(\xi_{K_R,e})
 !! \]
 !! where \( \xi_{K,e} \) is either \(-1/+1\), depending on the
 !! element's local side index 1 or 2.   
 !! Using the basis functions that are already evaluated at the element sides (```base%phib```)!
 !! 
 subroutine prolong_to_side(U_L, U_R, base, s, Uk_L,Uk_R)
  !---------------------------------------------------------------------------
  !input/output variables
  type(t_base), intent(in)  :: base      !! base and quadrature formula
  type(t_s),    intent(in)  :: s         !! side \(e\)
  real(wp),     intent(in)  :: Uk_L(:,:) !! left  element, dimension```(1:nvar,1:base%n)```
  real(wp),     intent(in)  :: Uk_R(:,:) !! right element, dimension```(1:nvar,1:base%n)```
  real(wp),     intent(out) :: U_L(:)    !! left  solution, dimension```(1:nvar)```
  real(wp),     intent(out) :: U_R(:)    !! right solution, dimension```(1:nvar)```
  !---------------------------------------------------------------------------
  !local variables
  integer :: j  !! index of basis function
  !---------------------------------------------------------------------------

  U_L=-999.999_wp !dummy output, TODO: REPLACE WITH YOUR CODE!
  U_R=-999.999_wp !dummy output, TODO: REPLACE WITH YOUR CODE!


 end subroutine prolong_to_side

!-----------------------------------------------------------------------------

 !>  evaluate the 1D euler flux
 !! \[
 !!    F(U) =\Mat{\rho v \\ \rho v v + p\\ (E+p)v \\} 
 !! \]
 !! where \(U=(\rho,\rho v, E) \)
 !!
 pure function eval_1d_euler_flux(U) result(F)
  !---------------------------------------------------------------------------
  !input/output variables
  real(wp), intent(in) :: U(1:nvar)  !! state  \(\rho,\rho v, E \) 
  real(wp) :: F(1:nvar)              !! output: 1d euler flux 
  !---------------------------------------------------------------------------
  !local variables
  real(wp) :: v,p          ! velocity, pressure
  !---------------------------------------------------------------------------

  F=-888.999_wp !dummy output,TODO : REPLACE WITH YOUR CODE!
 
 end function eval_1d_euler_flux

!-----------------------------------------------------------------------------

 !>  compute the Lax-Friedrichs for 1D euler eqn. flux from left and right state
 !! \[
 !!  \widehat{F}_n=F_\text{RP}(U_L,U_R,\V{n}) 
 !! \]
 !! in 1D, we compute the flux for a positive normal vector, pointing from left to right!  
 !! The simplest numerical flux, the Lax-Friedrichs flux, writes as
 !! \[
 !!  \widehat{F}_n=\frac{1}{2}\left(F_{euler}(U_L)+F_{euler}(U_R)\right) 
 !!                - \frac{\lambda_\text{max}}{2}\left (U_R-U_L \right)
 !! \]
 !! where \(\lambda_\text{max}\) is the absolute maximum wavespeed, computed as
 !! \[
 !! \lambda_\text{max}=\max(|v_L|+ c_L \, , \, |v_R|+c_R) 
 !! \]
 !! with the sound speed \( c= \sqrt{\gamma \frac{p}{\rho}} \).
 !!
 pure function numerical_flux(U_L,U_R) result(Fhat_n)
  !---------------------------------------------------------------------------
  !input/output variables
  real(wp), intent(in) :: U_L(1:nvar)  !! state in negative normal direction 
  real(wp), intent(in) :: U_R(1:nvar)  !! state in positive normal direction
  real(wp) :: Fhat_n(1:nvar)           !! output: numerical flux 
  !---------------------------------------------------------------------------
  !local variables
  real(wp) :: lam_max      ! maximum wavespeed
  real(wp) :: v_L,p_L,c_L  ! left velocity, pressure, sound speed
  real(wp) :: F_L(1:nvar)  ! left euler flux
  real(wp) :: v_R,p_R,c_R  ! right velocity, pressure, sound speed
  real(wp) :: F_R(1:nvar)  ! right euler flux
  !---------------------------------------------------------------------------

   
  Fhat_n=-777.999_wp !dummy output,TODO: REPLACE WITH YOUR CODE!
 
 end function numerical_flux

!-----------------------------------------------------------------------------

 !> Compute the side integrals
 !! \[
 !!  -\int_{e} \widehat{F}_{n,e} \sigma_{K,e} \varphi_i^K,
 !! \]
 !! for every basis function \(\varphi_i^K\in V_h(K)\) and
 !! element sides \(e=e_L(K),e_R(K)\)  
 !! In one spatial dimension, the sides reduce to points so that the integral yields
 !! \[
 !!  - \sigma_{K,e} \, \widehat{F}_{n,e}\, \varphi_i^K.
 !! \]
 !! Recall that
 !! \[
 !!  \sigma_{K_L(e),e} = 1, \qquad
 !!  \sigma_{K_R(e),e} = -1.
 !! \]
 !! The side integrals are *added* to ```e_rhs_L``` and ```e_rhs_L```.
 !!
 pure subroutine side_integral(e_rhs_L, e_rhs_R, base, s, f_s)
  !---------------------------------------------------------------------------
  !input/output variables
  type(t_base), intent(in)    :: base   !! base and quadrature formula
  type(t_s),    intent(in)    :: s      !! side \(e\)
  real(wp),     intent(in)    :: f_s(:) !! flux on element side, dimension ```(1:nvar)```
  !> \( - \sigma_{K_L(e),e} \, \widehat{F}_{n,e} \, \varphi_i^{K_L(e)} \) ,dimension```(1:nvar,1:base%n)```
  real(wp),     intent(inout) :: e_rhs_L(:,:)
  !> \( - \sigma_{K_R(e),e} \, \widehat{F}_{n,e} \, \varphi_i^{K_R(e)} \) ,dimension```(1:nvar,1:base%n)```
  real(wp),     intent(inout) :: e_rhs_R(:,:)
  !---------------------------------------------------------------------------
  !local variables
  integer :: i  !! index of test function
  !---------------------------------------------------------------------------

   !TODO: PUT YOUR CODE HERE!

 end subroutine side_integral

!-----------------------------------------------------------------------------

 !> Compute the element integral
 !! \[
 !!  \int_K \V{F}(U_h)|_K \cdot\Grad\varphi_i^K,
 !! \]
 !! for every basis function \(\varphi_i^K\in V_h(K)\) and for a given
 !! element \(K\in\mathcal{T}_h\).  
 !! Since \(K\) is a one-dimensional element, the integral is computed as
 !! \[
 !! \int_K F(U_h)|_K \partial_x\varphi_i^K =
 !! \frac{|K|}{|\tilde{K}|}\int_{\tilde{K}} ua_h |_{\tilde{K}} \,
 !! \frac{|\tilde{K}|}{|K|}\partial_{\tilde{x}} \tilde{\varphi}_i 
 !! \approx 
 !! \sum_{l=1}^m \tilde{w}^Q_l\, F^Q_l \,
 !! \partial_{\tilde{x}}\tilde{\varphi}_i (\tilde{x}^Q_l),
 !! \]
 !! Note the approximate sign, since the flux is non-linear and cannot be computed exactly!  
 !! Using the fact that
 !! \[
 !!  U_h(\tilde{x}^Q_l) =
 !!  \sum_{j=1}^nU^K_j\tilde{\varphi}_j(\tilde{x}^Q_l).
 !! \]
 !! we have to evaluate the 1D euler flux at each quadrature point
 !! \[
 !!  F^Q_l = F_\text{euler}\left (U_h(\tilde{x}^Q_l) \right)
 !! \]
 !! Each element integral is *added* to ```e_rhs(:,:)```.
 !!
 pure subroutine element_integral(e_rhs,base,Uk)
  !---------------------------------------------------------------------------
  !input/output variables
  type(t_base), intent(in)    :: base       !! base and quadrature formula
  real(wp),     intent(in)    :: Uk(  :,:)  !! solution for one elements, dimension```(1:nvar,1:base%n)```
  real(wp),     intent(inout) :: e_rhs(:,:) !! rhs for one elements, dimension```(1:nvar,1:base%n)```
  !---------------------------------------------------------------------------
  !local variables
  integer  :: i                 !! index of test function 
  integer  :: j                 !! index of basis function 
  integer  :: l                 !! index of quadrature point
  real(wp) :: Uk_q(nvar,base%m) !! solution at quadrature points in element ie
  real(wp) :: Fk_q(nvar,base%m) !! euler flux at each quadrature points in element ie
  !---------------------------------------------------------------------------
   !interpolate solution on gauss points

   !TODO: PUT YOUR CODE HERE!   

   !evaluate flux on gauss points

   !TODO: PUT YOUR CODE HERE!

   !do the integral

   !TODO: PUT YOUR CODE HERE!

 end subroutine element_integral

!-----------------------------------------------------------------------------

 !> Compute the mass matrix for element \(K\)
 !! \[
 !!  M^K_{ij} = \int_K \varphi_i^K \varphi_j^K
 !! \]
 !! for every element \(K\in\mathcal{T}_h\).  
 !! Since \(K\) is a one-dimensional element, the integral is computed
 !! as
 !! \[
 !! \int_K \varphi_i^K \varphi_j^K =
 !! \frac{|K|}{|\tilde{K}|}\int_{\tilde{K}} \tilde{\varphi}_i
 !! \tilde{\varphi}_j 
 !! \approx 
 !! \frac{|K|}{|\tilde{K}|} \sum_{l=1}^m \tilde{w}^Q_l\, 
 !! \tilde{\varphi}_i (\tilde{x}^Q_l)\,
 !! \tilde{\varphi}_j (\tilde{x}^Q_l).
 !! \]
 !!
 pure subroutine element_mass_matrix(m, base,e)
  !---------------------------------------------------------------------------
  !input/output variables
  type(t_base), intent(in)  :: base  !! base and quadrature formula
  type(t_e),    intent(in)  :: e      !! element \(K\)
  real(wp),     intent(out) :: m(:,:) !! mass matrix of element \(K\)
  !---------------------------------------------------------------------------
  !local variables
  integer :: i, j, l
  !---------------------------------------------------------------------------

   m = 0.0_wp
   do l=1,base%m
     do j=1,base%n
       do i=1,base%n
         m(i,j) = m(i,j) + base%wq(l) * base%phi(i,l)*base%phi(j,l)
       end do !i=1,base%n
     end do !j=1,base%n
   end do !l=1,base%m

   ! scale the matrix with the element area
   m = e%area/base%e_ref_area * m

 end subroutine element_mass_matrix

end module mod_1d_euler_ode
