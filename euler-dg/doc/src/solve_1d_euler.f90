  program solve_1d_euler

   use mod_kinds,          only: wp
   use mod_1d_euler_state, only: t_1d_euler_state
   use mod_1d_euler_ode,   only: t_1d_euler_ode, euler_ode_setup, euler_ode_clean
   use mod_1d_euler_ode,   only: nvar,var_names,gamma 
   use mod_rk,             only: t_explicit_euler, t_heun_method

   implicit none

   ! Discretization parameters
   integer,  parameter :: n_elem = 100
   real(wp), parameter :: x_left = 0.0_wp, x_right = 2.0_wp
   integer , parameter :: n_steps = 3000   !total number of steps
   integer , parameter :: write_steps = 30 !skip steps for output
   real(wp)            :: dt = 0.0005_wp 
   integer :: degree
   integer :: exfunc

   ! ODE
   integer :: n
   real(wp) :: time
   type(t_1d_euler_state) :: ynow, ynew
   type(t_1d_euler_ode)   :: ode

   ! Time integrator (choose one)
   !type(t_explicit_euler) :: time_integrator
   type(t_heun_method)    :: time_integrator

   integer               :: file_unit
   real(wp), allocatable :: xdata(:,:)

    write(*,*) "Choose the polynomial degree: 0/1"
    read(*,*) degree
    write(*,*) "Choose initial solution ... "
    write(*,*) "      1: density transport"
    write(*,*) "      2: pressure pulse"
    read(*,*) exfunc

    ! Set-up the problem: discretization and initial condition
    call euler_ode_setup( ode, ne=n_elem ,       &
      x_left=x_left , x_right=x_right , degree=degree )

    allocate( ynow%U(nvar,ode%base%n,ode%grid%ne) )
    allocate( ynew%U(nvar,ode%base%n,ode%grid%ne) )

    allocate(xdata(ode%base%n,ode%grid%ne))
    select case(degree)
     case(0)
      xdata(1,:) = ode%grid%e%xb
     case(1)
      xdata(1,:) = ode%grid%e%xv(1)
      xdata(2,:) = ode%grid%e%xv(2)
     case(2)
      xdata(1,:) = ode%grid%e%xv(1)
      xdata(2,:) = ode%grid%e%xb
      xdata(3,:) = ode%grid%e%xv(2)
     case default
      error stop "Not implemented"
    end select

  call init_solution(ynow%U, ode, xdata)

  ! Prepare the output file
  call output_file_setup(n_elem,ode%base%n-1)
  ! write initial condition
  call output_file_write_data( 0.0_wp , ynow%U )
  ! Integrate the system
  call time_integrator%setup(ynow)
 
  do n=1,n_steps
    time = real(n,wp)*dt

    ! Compute the time step
    call time_integrator%compute_step(ynew , ode,dt,ynow)
    call ynow%copy( ynew )

    ! Write the solution
    if(MOD(n,write_steps).eq.0)then
      call output_file_write_data( time , ynow%U )
    end if ! write_steps

  end do !n=1,n_steps

  ! Finalize the output file
  call output_file_finalize()


  write(*,*) "Done, results written in 'solve_1d_euler.out'."

  ! Clean-up
  deallocate( ynow%U )
  deallocate( ynew%U )
  deallocate( xdata )
  call euler_ode_clean(ode)
  call time_integrator%clean()

contains

 pure subroutine init_solution(U0,ode,x) 
  !------------------------------------------
  type(t_1d_euler_ode), intent(in)  :: ode
  real(wp), intent(in)              :: x(:,:)
  real(wp), intent(out)             :: U0(:,:,:)
  !------------------------------------------
  integer               :: ie,i
  !------------------------------------------
   do ie=1,ode%grid%ne
     do i=1,ode%base%n
       U0(:,i,ie) = exact_solution( x(i,ie),0.0_wp )
     end do !i=1,ode%base%n
   end do !iElem=1,ode%grid%ne
 end subroutine init_solution

 pure function exact_solution(x,t) result(U)
  !------------------------------------------
  real(wp), intent(in) :: x, t
  real(wp) :: U(1:nvar)
  !------------------------------------------
  integer, parameter :: m = 4
  real(wp), parameter :: pi = 3.14159265358979323846264338327950288_wp
  real(wp) :: rho,vel,pres
  !------------------------------------------
   
   select case(exfunc)
   case(1) !sinusodial density, only transported, v=-0.5, c=1
     rho=1.0_wp
     vel=-0.5_wp
     pres  = 1.0_wp/1.4_wp
     U(1) = rho*(1.0_wp+ 0.1_wp*sin( (2.0_wp*pi)/(x_right-x_left) * real(m,wp) * (x-vel*t) ))
     U(2) = U(1)*vel
     U(3) = pres/(gamma-1.0_wp) + 0.5_wp*U(2)**2/U(1)
   case(2) ! pressure pulse
     rho=1.0_wp
     vel=-0.1_wp
     pres  = (1.0_wp+ 0.2*exp(-(((x-vel*t)-0.5_wp*(x_right+x_left))/(0.05_wp*(x_right-x_left)))**2.0_wp))/1.4_wp
     U(1) = rho
     U(2) = rho*vel
     U(3) = pres/(gamma-1.0_wp) + 0.5_wp*rho*vel*vel
   end select !case(exfunc)
 end function exact_solution

 subroutine output_file_setup(ne,deg)
   integer:: ne,deg
   ! Open a file and connect it to a new unit
   open(newunit=file_unit, file="solve_1d_euler.out", &
        status='replace',action='write')
   ! Write the file header
   write(file_unit,'(a,I4,a,I2,a)') &
     'title  = "DG for 1D Euler equation, nElems=',ne,' degree=',deg,' "'
   write(file_unit,'(a)') &
     'xlabel = "x"'
   write(file_unit,'(a)') &
     'ylabel = ""'
   write(file_unit,'(a)') &
     'yNames = "Density" , "Velocity" , "Pressure" '
   write(file_unit,'(a)') &
     TRIM(var_names(nvar))
   write(file_unit,'(a,*(e23.15,:,","))') &
     'xdata  = ', xdata
 end subroutine output_file_setup

 subroutine output_file_finalize()
   ! Close the file
   close(unit=file_unit)
 end subroutine output_file_finalize

 subroutine output_file_write_data(t,U)
  real(wp), intent(in) :: t
  real(wp), intent(in) :: U(:,:,:)
   write(file_unit,'(a,e23.15)') &
     'time   = ',t
   write(file_unit,'(a,*(e23.15,:,","))') &
   '"Density"  = ',U(1,:,:)
   write(file_unit,'(a,*(e23.15,:,","))') &
   '"Velocity"  = ',U(2,:,:)/U(1,:,:)
   write(file_unit,'(a,*(e23.15,:,","))') &
   '"Pressure"  = ',(U(3,:,:)-0.5_wp*U(2,:,:)*U(2,:,:)/U(1,:,:))/(gamma-1.0_wp)
 end subroutine output_file_write_data

end program solve_1d_euler

