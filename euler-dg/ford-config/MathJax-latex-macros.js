window.MathJax = {
  TeX: {
     Macros: {
       V:    ["{\\boldsymbol #1}",1],
       Grad: "{\\nabla}",
       Div:  "{\\nabla\\cdot}",
       Mat:  ["{\\begin\{pmatrix\} #1 \\end\{pmatrix\}}",1],
       DD:   ["{\\frac\{\\partial #1 \}\{\\partial #2 \}}",2],
     }
   }
}
