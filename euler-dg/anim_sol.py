
"""
=====

execute with
  
  python3 anim_sinewave.py

will generate a sample data file data.txt and then read from it and animate in time, its a

 sinewave cos(k*pi*x)*sin(freq*pi*t)*exp(-alpha*t) animated over x[-1,1]

For other data written in the same format, use

python3 anim_sinewave.py -f <datafile.txt>

EXAMPLE OF THE DATAFORMAT, lines starting with # and empty lines are ignored :

#################################################################################################
# OPTIONAL: Plot title , string with " " 
  title  = "EXAMPLE data" 

# OPTIONAL: label of x and y axis, string with " "
  xlabel  = "X"
  ylabel  = "Solution A and B"

# Name for each ydata set, appear in legend, comma separated with " "
  yNames = "Solution A" , "Solution B"

# x data points 1...n_x, comma separated:
  xdata  =  -1.0 , -0.5, 0.0 , 0.5, 1.0

# time dependent data, time in first line,
#  then for each y variable the variable name and the data in one line 1...n_x, comma separated:

  time   = 0.00
  "Solution A" =  1.0 ,  2.0 ,  3.0 ,  2.0 ,  1.0
  "Solution B" = -1.0 , -2.0 , -3.0 , -2.0 , -1.0
  
  time   = 0.3
  "Solution A" =  1.5 ,  2.5 ,  3.5 ,  2.5 ,  1.5
  "Solution B" = -1.5 , -2.5 , -3.5 , -2.5 , -1.5
  
  time   = 0.6
  "Solution A" =  2.0 ,  3.0 ,  4.0 ,  3.0 ,  2.0
  "Solution B" = -2.0 , -3.0 , -4.0 , -3.0 , -2.0
#################################################################################################

=====

"""


import argparse
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
import matplotlib.animation as animation


####################################
amp = 0.5 # initial amplitude
wavenumber = 1.5  # wavenumber in x
freq = 50.  # frequency 
tdecay = 0.2 # decay time to 1/2
tend = 1.  # length of pendulum 2 in m
dt = 0.001 # number of output time levels
n_x  = 101 # number of points in x
####################################

decay = -np.log(0.5)/tdecay

########################################################################################################

# SAMPLE DATA GENERATION (FILE FORMAT!!)

########################################################################################################
def evalfunc(t,x):
    return  (amp*np.cos(2*np.pi*freq*t) * np.exp(-decay*t))*np.cos(np.pi*wavenumber*x)

def data_gen():

  n_t = int(tend/dt)+1
  time = np.linspace(0., tend, n_t)
  xdata = np.linspace(-1.0,1.0,n_x)
  ydata = np.zeros((n_x,n_t,2))
  
  print ( "generate data")
  for j in range(n_t) :
    ydata[:,j,0] = evalfunc(time[j],xdata)
    ydata[:,j,1] = 0.5*ydata[:,j,0] 
    
  
  outf = open("data.txt", 'w')
  yNames=[ '"Solution A"', '"Solution B"']
  #header
  outf.write('# Plot title, string with " " \n')
  title_out=("Sinus with k=%6.3f , f=%6.3f, T_decay=%6.3f" % (wavenumber,freq,tdecay))
  outf.write('  title  = "%s" \n' % title_out)
  outf.write('# label of x and y axis, string with " "  \n')
  outf.write('  xlabel  = "%s" \n' % "X")
  outf.write('  ylabel  = "%s" \n' % "Solution A and B")
  outf.write('# Name for each ydata set, appear in legend, comma separated with " " \n')
  outf.write("  yNames = %s\n" % ((" , ".join([('%s' % (yn) ) for yn in yNames ]) )) )
  outf.write('# x data points 1...n_x, comma separated: \n')
  outf.write("  xdata  = %s\n" % ((" , ".join([('%21.15e' % (x) ) for x in xdata ]) )) )
  outf.write('# time dependent data, time in first line, \n')
  outf.write('#  then for each y variable the variable name and the data in one line 1...n_x, comma separated: \n')
  for j in range(n_t) :
    outf.write("  time   = %21.15e\n" % (time[j] )) 
    for k in range(len(yNames)) :
      outf.write("  %s = %s\n" % (yNames[k],(" , ".join([('%21.15e' % (y) ) for y in ydata[:,j,k] ]) )) )
  
  outf.close()

  return True


########################################################################################################

# MAIN PROGRAM

########################################################################################################
parser = argparse.ArgumentParser(description='Tool to animate 1D time dependent data',\
                                 formatter_class=argparse.RawTextHelpFormatter)

parser.add_argument('-f'     , type=str, default=' ', help="  filename of input data. \n"
                                                           "  if not specified, a file data.txt will be generated" )

parser.add_argument('-skip'  , type=int, default=1, help="  skipping of time steps (default=1)" )

parser.add_argument('-wait'  , type=int, default=25, help="  waiting time between frames" )
parser.add_argument('-xlim'  , type=str, default='"1.0,-1.0"', help='  overwrite automatic x-axis range, format: \'xmin,xmax\'' )
parser.add_argument('-ylim'  , type=str, default='"1.0,-1.0"', help='  overwrite automatic y-axis range, format: \'ymin,ymax\'' )
parser.add_argument('-title' , type=str, default='" "', help="  overwrite plot title, format: \'your title \' " )
parser.add_argument('-xlabel', type=str, default='" "', help="  overwrite x-axis label, format: \'x-axis title\'" )
parser.add_argument('-ylabel', type=str, default='" "', help="  overwrite y-axis label, format: \'y-axis title\'" )

args = parser.parse_args()

####################
#### write sample data if no filename given 
####################
if (args.f == ' ') :
  stat = data_gen()
  filename="data.txt"
else :
  filename=args.f 

x_range=[float(x) for x in args.xlim.strip('"').split(',')]
y_range=[float(y) for y in args.ylim.strip('"').split(',')]

####################
#### read data 
####################
print ( "reading data from file: %s" % (filename))

inplines=open(filename,'r').readlines()
#filter all # lines
inplines = [ line for line in inplines if (not ('#' in line))]

for i  in range(len(inplines)) :
  line=inplines[i]
  removeline=True
  if (("title" in line) & ("=" in line)) :
    title = line.split('"')[1].strip().strip('"')
    print ("title found : %s " % title)
  elif(("xlabel" in line)  & ("=" in line)):
    xlabel = line.split('"')[1].strip().strip('"')
    print ("xlabel found : %s " % xlabel)
  elif(("ylabel" in line)  & ("=" in line)):
    ylabel = line.split('"')[1].strip().strip('"')
    print ("ylabel found : %s " % ylabel)
  elif(("yNames" in line) & ("=" in line)) :
    yNames = (line.split('=')[1]).split(',')
    n_yn = len(yNames)
    print ("number of y Names : %d " % (n_yn)) 
    for k in range(n_yn) :
      yNames[k]=yNames[k].strip().strip('"')
      print ("yname found : %s " % (yNames[k])) 
  else :
    removeline = False
  if (removeline) :
    inplines[i]='###'+inplines[i]

#filter all # lines
inplines = [ line for line in inplines if (not ('#' in line))]


tmpline = [ line.split("=")[1] for line in inplines if (('xdata' in line) & ("=" in line)) ]
if (len(tmpline) > 1) :
  print ("!!! WARING: more than 1 xdata line")

n_x = len(tmpline[0].split(","))
print ("number of x points : %d " % (n_x)) 
xdata=np.zeros(n_x)
xdata[0:n_x] = [float(x) for x in tmpline[0].split(",")]

xlimits=[xdata[0]-0.1*(xdata[n_x-1]-xdata[0]),xdata[n_x-1]+0.1*(xdata[n_x-1]-xdata[0])]
#filter
inplines = [ line for line in inplines if (not ('xdata' in line))]

tmpline = [ line.split("=")[1] for line in inplines if (('time' in line) & ("=" in line)) ]

n_t = len(tmpline)
print ("number of time intervals : %d " % (n_t)) 

time=np.zeros(n_t)
time = [float(x) for x in tmpline ]

ydata=np.zeros((n_x,n_t,n_yn))
ymin=np.zeros((n_yn))
ymax=np.zeros((n_yn))
ylimits=np.zeros((2,n_yn))
#filter
inplines = [ line for line in inplines if (not ('time' in line))]
for k in range(len(yNames)) :
  tmpline = [ line.split("=")[1] for line in inplines if (('"'+yNames[k].strip()+'"' in line) & ("=" in line)) ]
  if (len(tmpline) != n_t ) :
    print("WARNING: number of y-values for %s  /= n_t" % (yNames[k]) )
  for j in range(len(tmpline)) :
    if (len(tmpline[j].split(",")) != n_x):
      print("WARNING: size of y-values for %s  /= n_x" % (yNames[k]) )
    ydata[0:n_x,j,k] = [float(x) for x in tmpline[j].split(",")]

for k in range(len(yNames)) :
  ymin[k]= np.amin(ydata[:,:,k])-0.001
  ymax[k]= np.amax(ydata[:,:,k])+0.001
  ylimits[:,k]= [ymin[k]-0.25*(ymax[k]-ymin[k])-1.0e-05,ymax[k]+0.35*(ymax[k]-ymin[k])]

####################
#### read data finished, now visualize
####################

#overwrite with argument options     
if(args.title.strip('"') != ' '):
  title=args.title.strip('"')
if (x_range[1]-x_range[0] > 0) :
  xlimits=x_range
if (y_range[1]-y_range[0] > 0) :
  ylimits=y_range
if(args.xlabel.strip('"') != ' '):
  xlabel=args.xlabel.strip('"')
if(args.ylabel.strip('"') != ' '):
  ylabel=args.ylabel.strip('"')

fig = plt.figure(figsize=[10,10])

fig.suptitle(title, fontsize=14) #, fontweight='bold')
#ax = fig.add_subplot(111, autoscale_on=False, xlim=xlimits, ylim=ylimits)
#ax.grid()

gs = gridspec.GridSpec(len(yNames),1)

axarr=[]
pltlines=[]
for k in range(len(yNames)) :
  ax=fig.add_subplot(gs[k], autoscale_on=False, xlim=xlimits, ylim=ylimits[:,k])
  axarr.append(ax)
  plt.xlabel(xlabel)
  plt.ylabel(yNames[k])

  axarr[k].grid()
  pltline, = axarr[k].plot([],[],label=yNames[k])
#  pltline, = ax.plot([],[],label=yNames[k])
  pltlines.append(pltline)

  plt.legend(bbox_to_anchor=(0.35, 0.99), loc=2, borderaxespad=0.)

time_template = 'time = %.4fs'
#time_text = ax.text(0.05, 0.95, '', transform=ax.transAxes)
time_text = axarr[0].text(0.05, 0.9, '', transform=axarr[0].transAxes)

def init():
    for k in range(len(yNames)) :
       pltlines[k].set_data([],[])
    time_text.set_text('')
    return  tuple(pltlines) + (time_text,) 


def animate(j):
    for k in range(len(yNames)) :
       pltlines[k].set_data(xdata,ydata[:,j,k])
    time_text.set_text(time_template % time[j])
    return  tuple(pltlines) + (time_text,) 

ani = animation.FuncAnimation(fig, animate, np.arange(0, len(time),args.skip),
                              interval=args.wait, blit=False, init_func=init)

plt.show()
# ani.save('anim.mp4', fps=15)
