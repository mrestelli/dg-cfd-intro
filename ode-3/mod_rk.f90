! Collection of RK solvers
!
module mod_rk

 use mod_kinds, only: wp
 
 use mod_ode_state, only: c_ode_state

 use mod_ode_solver, only: c_ode, c_ode_integrator

 implicit none

 public :: t_explicit_euler

 private

 type, extends(c_ode_integrator) :: t_explicit_euler
 contains
  procedure, nopass :: compute_step => explicit_euler_step
 end type t_explicit_euler

contains

 ! The Euler step is
 !
 !  ynew = ynow + dt*ode%rhs( ynow )
 subroutine explicit_euler_step(ynew , ode,dt,ynow)
  class(c_ode),       intent(in)    :: ode
  real(wp),           intent(in)    :: dt
  class(c_ode_state), intent(in)    :: ynow
  class(c_ode_state), intent(inout) :: ynew

   call ode%compute_rhs( ynew , ynow ) ! ynew = rhs(ynow)
   call ynew%scalar_multiply( dt )     ! ynew = dt*rhs(ynow)
   call ynew%increment( ynow )         ! ynew = ynow + dt*rhs(ynow)

 end subroutine explicit_euler_step

end module mod_rk

