# To use this include file, define the following variables:
#
# PFUNIT          : root directory of the pFUnit test framework
# PFUNIT_TEST_MOD : list of the .pf files
# FC FFLAGS       : fortran compiler and corresponding flags
# LD LDFLAGS      : linker and corresponding flags
# OBJ_MOD         : module object files for the production code
#
# For a production code module  prod_code_module.f90  the test module
# must be called  prod_code_module_tests.pf  and must be listed in
# PFUNIT_TEST_MOD.

PFUNIT_PP=$(PFUNIT)/bin/pFUnitParser.py
PFUNIT_INC=-I$(PFUNIT)/mod -I$(PFUNIT)/include 
PFUNIT_DRIVER=$(PFUNIT)/include/driver.F90
PFUNIT_LDFLAGS=-L$(PFUNIT)/lib -static
PFUNIT_LIBS=-lpfunit

# pFUnit source and object files
#
# see "Functions for String Substitution and Analysis"
# https://www.gnu.org/software/make/manual/html_node/Text-Functions.html
PFUNIT_SRC=$(patsubst %.pf,%.F90,$(PFUNIT_TEST_MOD))
PFUNIT_OBJ=$(patsubst %.pf,%.o,  $(PFUNIT_TEST_MOD))

# The pFUnit preprocessor converts a _tests.pf file into a fortran
# source file _tests.F90 taking care of the pFUnit macros such as
# @Test, @asssertTrue, @assertEqual etc.
#
# See also "Static Pattern Rules"
# https://www.gnu.org/software/make/manual/html_node/Static-Pattern.html#Static-Pattern
$(PFUNIT_SRC): %_tests.F90: %_tests.pf
	$(PFUNIT_PP) $< $@

# Compile the _tests.F90 test collections
$(PFUNIT_OBJ): %_tests.o: %_tests.F90 %.o
	$(FC) $(FFLAGS)  -ffree-line-length-300 $(PFUNIT_INC) -c $<

# Compile the pFUnit driver
driver.o: testSuites.inc
	$(FC) $(filter-out -pedantic -Wall -Wconversion-extra \
	  -fcheck=all, $(FFLAGS)) $(PFUNIT_INC) -I. -c $(PFUNIT_DRIVER)

# Link the test program
pFUnit-tests: $(OBJ_MOD) $(PFUNIT_OBJ) driver.o
	$(LD) $(PFUNIT_LDFLAGS) $(LDFLAGS) $(OBJ_MOD) $(PFUNIT_OBJ) \
          driver.o $(PFUNIT_LIBS) -o $@

# See also "Special Built-in Target Names"
# https://www.gnu.org/software/make/manual/html_node/Special-Targets.html#Special-Targets
.PRECIOUS: %_tests.F90 %_tests.o

.PHONY: tests
tests: pFUnit-tests

.PHONY: clean-tests
clean-tests:
	$(RM) *_tests.F90 *_tests.o *_tests.mod pFUnit-tests

