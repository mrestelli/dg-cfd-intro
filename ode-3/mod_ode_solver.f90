! Generic structure for explicit ODE integrators
!
! This module defines two types: one to represent an ODE and one to
! represent an ODE integrator.
!
! The ODE is completely defined by its righ-hand-side:
!  
!  y' = rhs(y)
!
! An ODE integrator has one method to compte ynew given ynow.
!
module mod_ode_solver

 use mod_kinds, only: wp
 
 use mod_ode_state, only: c_ode_state

 implicit none

 public :: c_ode, c_ode_integrator

 private

 ! ODE
 type, abstract :: c_ode
 contains
  procedure(i_compute_rhs), deferred, nopass :: compute_rhs
 end type c_ode

 abstract interface
  subroutine i_compute_rhs(rhs,y)
   import :: c_ode, c_ode_state
   implicit none
   class(c_ode_state), intent(in)    :: y
   class(c_ode_state), intent(inout) :: rhs
  end subroutine i_compute_rhs
 end interface

 ! ODE integrator
 type, abstract :: c_ode_integrator
 contains
  procedure(i_compute_step), deferred, nopass :: compute_step
 end type c_ode_integrator

 abstract interface
  subroutine i_compute_step(ynew , ode,dt,ynow)
   import :: wp, c_ode_state, c_ode, c_ode_integrator
   implicit none
   class(c_ode),       intent(in)    :: ode
   real(wp),           intent(in)    :: dt
   class(c_ode_state), intent(in)    :: ynow
   class(c_ode_state), intent(inout) :: ynew
  end subroutine i_compute_step
 end interface

end module mod_ode_solver

