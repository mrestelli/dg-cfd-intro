program rk_solve

 use mod_kinds,               only: wp
 use mod_harmonic_oscillator, only: t_ho_state, t_ho_ode
 use mod_rk,                  only: t_explicit_euler

 implicit none

 integer, parameter :: num_st = 50
 real(wp), parameter :: dt = 0.5_wp
 real(wp), parameter :: u0 = 1.0_wp, v0 = 0.0_wp ! initial condition

 type(t_ho_state) :: ynow, ynew
 type(t_ho_ode) :: ho_ode
 type(t_explicit_euler) :: explicit_euler

 integer :: n
 real(wp) :: u(num_st), v(num_st)

 character(len=100) :: out_format

  ynow%u = u0
  ynow%v = v0
  do n=1,num_st
    call explicit_euler%compute_step(ynew , ho_ode,dt,ynow)
    call ynow%copy( ynew )
    ! store the solution
    u(n) = ynow%u
    v(n) = ynow%v
  enddo

  write(out_format,'(a,i4,a)') '(a," = [",',num_st,'(e13.5,",")," ]")'
  write(*,out_format) "u", u
  write(*,out_format) "v", v

end program rk_solve
