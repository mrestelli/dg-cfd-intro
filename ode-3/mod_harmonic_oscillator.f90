! Define the ODE
!
!  u'' + omega^2 u = 0
!
! The ODE is rewritten as a first order system:
!
!  u' = v
!  v' = -omega^2 u
!
! To define the ODE we need two types:
!
! * a type for the state variable  y = [u,v]
!
! * a type for the ODE, defining the right-hand-side
!              [      v      ]
!     rhs(y) = [             ]
!              [ -omega^2 u  ]
!
module mod_harmonic_oscillator

 use mod_kinds, only: wp

 use mod_ode_state, only: c_ode_state

 use mod_ode_solver, only: c_ode

 implicit none

 public :: t_ho_state, t_ho_ode

 private

 type, extends(c_ode_state) :: t_ho_state
  real(wp) :: u
  real(wp) :: v
 contains
  procedure, pass(x) :: increment       => ho_increment
  procedure, pass(x) :: scalar_multiply => ho_scalar_multiply
  procedure, pass(y) :: copy            => ho_copy
  procedure, pass(x) :: clone           => ho_clone
 end type t_ho_state

 type, extends(c_ode) :: t_ho_ode
 contains
  procedure, nopass :: compute_rhs => ho_compute_rhs
 end type t_ho_ode

 real(wp), parameter :: omega = 0.25_wp

contains

!-----------------------------------------------------------------------

 subroutine ho_increment(x,y)
  class(c_ode_state), intent(in)    :: y
  class(t_ho_state),  intent(inout) :: x

   select type(y); type is(t_ho_state)
   x%u = x%u + y%u
   x%v = x%v + y%v
   end select
 end subroutine ho_increment

 subroutine ho_scalar_multiply(x,r)
  real(wp),          intent(in)    :: r
  class(t_ho_state), intent(inout) :: x

   x%u = r*x%u
   x%v = r*x%v
 end subroutine ho_scalar_multiply

 subroutine ho_copy(y,x)
  class(c_ode_state), intent(in)    :: x
  class(t_ho_state),  intent(inout) :: y

   select type(x); type is(t_ho_state)
   y%u = x%u
   y%v = x%v
   end select
 end subroutine ho_copy

 subroutine ho_clone(y,x)
  class(t_ho_state),               intent(in)  :: x
  class(c_ode_state), allocatable, intent(out) :: y

   allocate( y , source=x )
 end subroutine ho_clone

!-----------------------------------------------------------------------

 subroutine ho_compute_rhs(rhs,y)
  class(c_ode_state), intent(in)    :: y
  class(c_ode_state), intent(inout) :: rhs

   select type(y);   type is(t_ho_state)
   select type(rhs); type is(t_ho_state)

   rhs%u =             y%v
   rhs%v = -omega**2 * y%u

   end select
   end select
 end subroutine ho_compute_rhs

!-----------------------------------------------------------------------

end module mod_harmonic_oscillator

