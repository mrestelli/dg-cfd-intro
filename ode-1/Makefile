# For general information about GNU Make see
# https://www.gnu.org/software/make/manual/html_node

# Fortran compiler
FC=gfortran
FFLAGS=-g -O0 -pedantic -Wall -Wconversion-extra -fcheck=all -ffpe-trap=invalid -fbacktrace

# linker
LD=$(FC)
LDFLAGS=

# object files
OBJ=mod_kinds.o \
  mod_rk_ode.o \
  rk_solve.o


# Main target: the executable program
#
# This rule uses the linker LD (with the liker flags LDFLAGS) to link
# all the objects (listed in OBJ) and produce the desired executable
#
# See also "Automatic Variables"
# https://www.gnu.org/software/make/manual/html_node/Automatic-Variables.html#Automatic-Variables
rk_solve: $(OBJ)
	$(LD) $(LDFLAGS) $(OBJ) -o $@

# General rule to compile fortran source files
#
# This rule uses the fortran compiler FC (with the fortran flags
# FFLAGS) to compile a fortran source file (extension .f90) and
# produce the corresponding object file (extension .o).
#
# Remark: this operation produces also a .mod file, which is used
# internally by the compiler and can ignored by the user.
#
# See also "Pattern Rules"
# https://www.gnu.org/software/make/manual/html_node/Pattern-Intro.html#Pattern-Intro
%.o: %.f90
	$(FC) $(FFLAGS) -c $<

# Clean-up
#
# This rule removes all the files produced during the compilation; RM
# is a Make variable which contains the "remove" command. This target
# is declared "phony" because it does not correspond to a file called
# "clean", but simply identifies an action to be performed.
#
# See also "Phony Targets" and "Special Built-in Target Names"
# https://www.gnu.org/software/make/manual/html_node/Phony-Targets.html#Phony-Target
# https://www.gnu.org/software/make/manual/html_node/Special-Targets.html#Special-Targets
.PHONY: clean
clean:
	$(RM) *.o *.mod rk_solve


# Dependencies --------------------------------------------------------

mod_rk_ode.o: \
  mod_kinds.o

rk_solve.o: \
  mod_kinds.o \
  mod_rk_ode.o

