1) Implement the missing subroutines in mod_1d_scalar_ad_local_int.

Some HTML documentation is available in the DOC folder.

2) Test the method without stabilization of the diffusive numerical
flux, i.e.   ETA = 0   in  MOD_1D_SCALAR_AD_LOCAL_INT. Test the
resulting method:
* on a smooth solution
* on a solution which is 1 between x=0.5 and x=1.0 and zero elsewhere
* on a discontinuous solution which takes the values +1 and -1 at the
  vertexes of each element

3) Repeat the previous point with a positive stabilization coeff. ETA.

4) Extend the code to allow piecewise parabolic functions; notice that
this requires changing   MOD_1D_BASE   including
* the degree=2 option
* the definition of the three base functions
* choosing an appropriate quadrature rule

