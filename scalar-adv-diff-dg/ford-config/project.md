project: DG &ndash; scalar advection-diffusion equation
summary: DG discretization of the scalar advection-diffusion equation
author: Marco Restelli
email: mrestelli@gmail.com
website: http://www2.ipp.mpg.de/~marre
bitbucket: https://bitbucket.org/mrestelli
project_bitbucket: https://bitbucket.org/mrestelli/dg-cfd-intro
license: by
src_dir: ./../src
output_dir: ./../doc
exclude_dir: ./../doc
             ./../doc/src
mathjax_config: ./../ford-config/MathJax-latex-macros.js
predocmark: >
display: public
         protected
         private
source: false
graph: true
search: true
macro: TEST
       LOGIC=.true.

Solves the scalar advection-diffusion equation
\begin{align}
& \V{g} = \Grad a \\
& \partial_t a + \Div(\V{u}a-\nu\V{g}) = 0
\label{eq:scalar-adv-diff-DG}
\end{align}
with the Discontinuous Galerkin (DG) discretization
\begin{align}
\int_K \V{g}\varphi_h & = -\int_K a_h\Grad\varphi_h +
\int_{\partial K} \widehat{a} \varphi_h \V{n} \\
\int_K \partial_t a_h\varphi_h & = \int_K( \V{u}a_h-\nu\V{g}) 
\cdot\Grad\varphi_h
- \int_{\partial K} ( \widehat{\V{u}a} -\widehat{\nu\V{g}}) 
\cdot\V{n} \varphi_h.
\label{eq:scalar-adv-DG}
\end{align}
