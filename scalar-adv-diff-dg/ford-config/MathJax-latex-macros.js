window.MathJax = {
  TeX: {
     Macros: {
       V:    ["{\\boldsymbol #1}",1],
       Grad: "{\\nabla}",
       Div: "{\\nabla\\cdot}",
     }
   }
}
