module mod_1d_scalar_ad_local_int

 use mod_kinds, only: wp

 use mod_1d_grid, only: t_grid, t_s, t_e, grid_setup, grid_clean

 use mod_1d_base, only: t_base, base_setup, base_clean

 implicit none

 public :: element_mass_matrix, &
   element_integral, side_integral, &
   g_element_integral, g_side_integral, &
   upwind_flux, stab_diff_flux, &
   eta

 private

 !> Parameter \(\eta\) for the stabilization of the diffusive flux
 real(wp), parameter :: eta = 0.5_wp

contains

!-----------------------------------------------------------------------

 !> Compute the element integral
 !! \[
 !!  M^K_{ij} = \int_K \varphi_i^K \varphi_j^K
 !! \]
 !! for every element \(K\in\mathcal{T}_h\).
 !!
 !! Since \(K\) is a one-dimensional element, the integral is computed
 !! as
 !! \[
 !! \int_K \varphi_i^K \varphi_j^K =
 !! \frac{|K|}{|\tilde{K}|}\int_{\tilde{K}} \tilde{\varphi}_i
 !! \tilde{\varphi}_j 
 !! \approx 
 !! \frac{|K|}{|\tilde{K}|} \sum_{l=1}^m \tilde{w}^Q_l\, 
 !! \tilde{\varphi}_i (\tilde{x}^Q_l)\,
 !! \tilde{\varphi}_j (\tilde{x}^Q_l).
 !! \]
 pure subroutine element_mass_matrix(m, base,e)
  type(t_base), intent(in)  :: base  !! base and quadrature formula
  type(t_e),    intent(in)  :: e     !! element \(K\)
  real(wp),     intent(out) :: m(:,:)

  integer :: i, j, l

   m = 0.0_wp
   do l=1,base%m
     do j=1,base%n
       do i=1,base%n
         m(i,j) = m(i,j) + base%wq(l) * base%phi(i,l)*base%phi(j,l)
       enddo
     enddo
   enddo

   ! scale the matrix with the element area
   m = e%area/base%e_ref_area * m

 end subroutine element_mass_matrix

!-----------------------------------------------------------------------

 !> Compute the element integral
 !! \[
 !!  \int_K (\V{u}a_h|_K -\nu \V{g}_h|_K) \cdot\Grad\varphi_i^K,
 !! \]
 !! for every basis function \(\varphi_i^K\in V_h(K)\) and for a given
 !! element \(K\in\mathcal{T}_h\).
 !!
 !! Since \(K\) is a one-dimensional element, the integral is computed
 !! as
 !! \begin{align*}
 !! \int_K (ua_h|_K -\nu g_h|_K) \partial_x\varphi_i^K & =
 !! \frac{|K|}{|\tilde{K}|}\int_{\tilde{K}} (ua_h |_{\tilde{K}} 
 !! -\nu g_h |_{\tilde{K}} )
 !! \frac{|\tilde{K}|}{|K|}\partial_{\tilde{x}} \tilde{\varphi}_i \\
 !! & \approx 
 !! \sum_{l=1}^m \tilde{w}^Q_l\, ( ua_h(\tilde{x}^Q_l) 
 !! -\nu g_h(\tilde{x}^Q_l) )
 !! \partial_{\tilde{x}}\tilde{\varphi}_i (\tilde{x}^Q_l),
 !! \end{align*}
 !! using the fact that
 !! \[
 !!  a_h(\tilde{x}^Q_l) =
 !!  \sum_{j=1}^na^K_j\tilde{\varphi}_j(\tilde{x}^Q_l)
 !! \]
 !! and the analogous relation for \(g_h(\tilde{x}^Q_l)\).
 !!
 !! The element integral is *added* to ```e_rhs```.
 pure subroutine element_integral(e_rhs, u,nu, base,ak,gk)
  real(wp),     intent(in)    :: nu    !! diffusion coefficient
  real(wp),     intent(in)    :: u     !! advection coefficient
  type(t_base), intent(in)    :: base  !! base and quadrature formula
  !> \(a^K_i, \quad i=1,\ldots,n\)
  real(wp),     intent(in)    :: ak(:)
  !> \(\V{g}^K_i, \quad i=1,\ldots,n\)
  real(wp),     intent(in)    :: gk(:)
  real(wp),     intent(inout) :: e_rhs(:)

  integer :: i, l
  real(wp) :: ah_k(base%m), gh_k(base%m), tot_flux

 end subroutine element_integral

!-----------------------------------------------------------------------

 !> Compute the element integral
 !! \[
 !!  -\int_K a_h|_K \Grad\varphi_i^K,
 !! \]
 !! for every basis function \(\varphi_i^K\in V_h(K)\) and for a given
 !! element \(K\in\mathcal{T}_h\).
 !!
 !! The element integral is *added* to ```e_rhs```.
 pure subroutine g_element_integral(e_rhs, base,ak)
  type(t_base), intent(in)    :: base  !! base and quadrature formula
  real(wp),     intent(in)    :: ak(:) !! \(a^K_i, \quad i=1,\ldots,n\)
  real(wp),     intent(inout) :: e_rhs(:)

  integer :: i, l
  real(wp) :: ah_k(base%m)

 end subroutine g_element_integral

!-----------------------------------------------------------------------

 !> Compute the side integrals
 !! \[
 !!  -\int_{e} (\widehat{\V{u}a} - \widehat{\nu\V{g}})
 !!   \cdot\sigma_{K,e}\V{n}_e \varphi_i^K,
 !! \]
 !! for every basis function \(\varphi_i^K\in V_h(K)\), for \(
 !! K=K_1(e),K_2(e) \) and for a given
 !! side \(e\in\mathcal{E}_h\).
 !!
 !! In one spatial dimension, the sides reduce to points so that the
 !! integral yields
 !! \[
 !!  - \sigma_{K,e} \, (\widehat{ua}-\widehat{\nu g})\, \varphi_i^K.
 !! \]
 !!
 !! Recall that
 !! \[
 !!  \sigma_{K_1(e),e} = 1, \qquad
 !!  \sigma_{K_2(e),e} = -1.
 !! \]
 !!
 !! The side integrals are *added* to ```e_rhs1``` and ```e_rhs2```.
 pure subroutine side_integral(s_rhs1,s_rhs2, u,nu, base,s, &
                               ak1,ak2, gk1,gk2)
  !> advection coefficient
  real(wp),     intent(in)    :: u
  !> diffusion coefficient
  real(wp),     intent(in)    :: nu
  !> base and quadrature formula
  type(t_base), intent(in)    :: base
  !> side \( e \)
  type(t_s),    intent(in)    :: s
  !> \(a^{K_1}_i, \quad i=1,\ldots,n\)
  real(wp),     intent(in)    :: ak1(:)
  !> \(a^{K_2}_i, \quad i=1,\ldots,n\)
  real(wp),     intent(in)    :: ak2(:)
  !> \(g^{K_1}_i, \quad i=1,\ldots,n\)
  real(wp),     intent(in)    :: gk1(:)
  !> \(g^{K_2}_i, \quad i=1,\ldots,n\)
  real(wp),     intent(in)    :: gk2(:)
  !> \( - \sigma_{K_1(e),e} (\widehat{ua}-\widehat{\nu g})
  !! \varphi_i^{K_1(e)} \)
  real(wp),     intent(inout) :: s_rhs1(:)
  !> \( - \sigma_{K_2(e),e} (\widehat{ua}-\widehat{\nu g})
  !! \varphi_i^{K_2(e)} \)
  real(wp),     intent(inout) :: s_rhs2(:)

  integer :: i
  real(wp) :: ah_k1, ah_k2
  real(wp) :: gh_k1, gh_k2
  real(wp) :: adv_flux1, adv_flux2, diff_flux1, diff_flux2, numflux

 end subroutine side_integral

!-----------------------------------------------------------------------

 !> Compute the side integrals
 !! \[
 !!  \int_{e} \widehat{a} \sigma_{K,e}\V{n}_e \varphi_i^K,
 !! \]
 !! for every basis function \(\varphi_i^K\in V_h(K)\), for \(
 !! K=K_1(e),K_2(e) \) and for a given
 !! side \(e\in\mathcal{E}_h\).
 !!
 !! The side integrals are *added* to ```e_rhs1``` and ```e_rhs2```.
 pure subroutine g_side_integral(s_rhs1,s_rhs2, base,s,ak1,ak2)
  !> base and quadrature formula
  type(t_base), intent(in)    :: base
  !> side \( e \)
  type(t_s),    intent(in)    :: s
  !> \(a^{K_1}_i, \quad i=1,\ldots,n\)
  real(wp),     intent(in)    :: ak1(:)
  !> \(a^{K_2}_i, \quad i=1,\ldots,n\)
  real(wp),     intent(in)    :: ak2(:)
  !> \( - \sigma_{K_1(e),e} \, \widehat{ua} \, \varphi_i^{K_1(e)} \)
  real(wp),     intent(inout) :: s_rhs1(:)
  !> \( - \sigma_{K_2(e),e} \, \widehat{ua} \, \varphi_i^{K_2(e)} \)
  real(wp),     intent(inout) :: s_rhs2(:)

  integer :: i
  real(wp) :: ah_k1, ah_k2
  real(wp) :: numflux

 end subroutine g_side_integral

!-----------------------------------------------------------------------

 pure function upwind_flux(u,n,flux1,flux2) result(upwflux)
  real(wp), intent(in) :: u     ! advection velocity
  real(wp), intent(in) :: n     ! unit normal form e1 to e2
  real(wp), intent(in) :: flux1 ! flux on e1
  real(wp), intent(in) :: flux2 ! flux on e2
  real(wp) :: upwflux

 end function upwind_flux

!-----------------------------------------------------------------------

 pure function stab_diff_flux(nu,n,flux1,flux2,a1,a2) result(diffflux)
  real(wp), intent(in) :: nu    ! diffusion coefficient
  real(wp), intent(in) :: n     ! unit normal form e1 to e2
  real(wp), intent(in) :: flux1 ! flux on e1
  real(wp), intent(in) :: flux2 ! flux on e2
  real(wp), intent(in) :: a1    ! solution a on e1
  real(wp), intent(in) :: a2    ! solution a on e2
  real(wp) :: diffflux

 end function stab_diff_flux

!-----------------------------------------------------------------------

end module mod_1d_scalar_ad_local_int

