module mod_1d_grid

 use mod_kinds, only: wp
 
 implicit none

 public :: t_grid, grid_setup, grid_clean, t_s, t_e

 private

 type :: t_s
  integer  :: e1 ! first connected element
  integer  :: e2 ! second connected element
  integer  :: i_loc1 ! local side index on e1
  integer  :: i_loc2 ! local side index on e2
  real(wp) :: n  ! normal unit vector from e1 to e2
 end type t_s

 type :: t_e
  real(wp) :: xb    ! center of the element
  real(wp) :: xv(2) ! vertexes of the element
  real(wp) :: area
 end type t_e

 type :: t_grid
  integer :: ns ! number of sides
  integer :: ne ! number of elements
  type(t_s), allocatable :: s(:) ! sides
  type(t_e), allocatable :: e(:) ! elements
 end type t_grid

contains

 ! Setup the module variable grid
 subroutine grid_setup(grid, ne,x_left,x_right)
  integer,      intent(in) :: ne
  real(wp),     intent(in) :: x_left, x_right ! domain size
  type(t_grid), intent(out) :: grid

  integer :: is, ie

   grid%ns = ne ! periodic bcs
   grid%ne = ne

   ! allocate and define the side array
   allocate( grid%s( grid%ns ) )
   ! first side: periodic bcs
   associate( side => grid%s(1) )
   side%e1 = grid%ne
   side%e2 = 1
   side%i_loc1 = 2
   side%i_loc2 = 1
   side%n  = 1.0_wp
   end associate
   do is=2,grid%ns
     associate( side => grid%s(is) )
     side%e1 = is-1
     side%e2 = is
     side%i_loc1 = 2
     side%i_loc2 = 1
     side%n  = 1.0_wp
     end associate
   enddo

   ! allocate and define the element array
   allocate( grid%e( grid%ne ) )
   do ie=1,grid%ne
     associate( elem => grid%e(ie) )
     elem%area = (x_right-x_left)/real(grid%ne,wp)
     elem%xb   = x_left + &
       (0.5_wp+real(ie-1,wp))*(x_right-x_left)/real(grid%ne,wp)
     elem%xv(1) = x_left + &
       real(ie-1,wp)*(x_right-x_left)/real(grid%ne,wp)
     elem%xv(2) = x_left + &
       real(ie  ,wp)*(x_right-x_left)/real(grid%ne,wp)
     end associate
   enddo

 end subroutine grid_setup
 
 subroutine grid_clean(grid)
  type(t_grid), intent(inout) :: grid
   deallocate( grid%s )
   deallocate( grid%e )
 end subroutine grid_clean

end module mod_1d_grid
