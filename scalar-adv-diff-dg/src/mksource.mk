# Object files: modules and main program ------------------------------

# object files: modules
OBJ_MOD=mod_kinds.o \
  mod_ode_state.o \
  mod_ode_solver.o \
  mod_rk.o \
  mod_linalg.o \
  mod_1d_grid.o \
  mod_1d_base.o \
  mod_1d_scalar_ad_state.o \
  mod_1d_scalar_ad_local_int.o \
  mod_1d_scalar_ad_ode.o

# object files: main program
OBJ_MAIN=solve_1d_scalar_adv_diff.o

# Main targets and general rules --------------------------------------

# Main target: the executable program
solve_1d_scalar_adv_diff: $(OBJ_MOD) $(OBJ_MAIN)
	$(RECIPE)

# General rule to compile Fortran source files
%.o: %.f90
	$(RECIPE)

# Clean-up
.PHONY: clean
clean:
	$(CLEAN_RECIPE)

# Dependencies --------------------------------------------------------

mod_ode_state.o: \
  mod_kinds.o

mod_ode_solver.o: \
  mod_kinds.o \
  mod_ode_state.o

mod_rk.o: \
  mod_kinds.o \
  mod_ode_state.o \
  mod_ode_solver.o

mod_linalg.o: \
  mod_kinds.o

mod_1d_grid.o: \
  mod_kinds.o

mod_1d_base.o: \
  mod_kinds.o

mod_1d_scalar_ad_state.o: \
  mod_kinds.o \
  mod_ode_state.o

mod_1d_scalar_ad_local_int.o: \
  mod_kinds.o \
  mod_1d_grid.o \
  mod_1d_base.o

mod_1d_scalar_ad_ode.o: \
  mod_kinds.o \
  mod_ode_state.o \
  mod_ode_solver.o \
  mod_1d_grid.o \
  mod_1d_base.o \
  mod_1d_scalar_ad_state.o \
  mod_1d_scalar_ad_local_int.o

solve_1d_scalar_adv_diff.o: \
  mod_kinds.o \
  mod_1d_grid.o \
  mod_1d_base.o \
  mod_1d_scalar_ad_state.o \
  mod_1d_scalar_ad_ode.o \
  mod_rk.o

# Makefile logic: dispatching and doing real work ---------------------

# Define the recipe: 
# 1) during the dispatch phase: cd to the correct directory and make
# 2) during the make phase: compile, link or clean
ifeq (DISPATCH,$(PHASE))

# For each target, specify the working directory; then use the general
# rule from the main Makefile
solve_1d_scalar_adv_diff: WDIR=bin
%.o:                      WDIR=build

# Clean-up
CLEAN_RECIPE = \
  +$(MAKE) -C build -f $(ROOTDIR)/Makefile $@ && \
   $(MAKE) -C bin   -f $(ROOTDIR)/Makefile $@
	
else

# For each target, provide the build rule
solve_1d_scalar_adv_diff: RECIPE=$(LD) $(LDFLAGS) $^ -o $@
%.o:                      RECIPE=$(FC) $(FFLAGS) -c $< -o $@

ifeq (build,$(strip $(notdir $(CURDIR))))
  CLEAN_RECIPE = $(RM) *.o *.mod
else ifeq (bin,$(strip $(notdir $(CURDIR))))
  CLEAN_RECIPE = $(RM) solve_1d_scalar_adv_diff
endif

endif

