program solve_1d_scalar_adv_diff

 use mod_kinds,               only: wp
 use mod_1d_scalar_ad_state,  only: t_1d_scalar_ad_state
 use mod_1d_scalar_ad_ode,    only: t_1d_scalar_ad_ode, &
       scalar_ad_ode_setup, scalar_ad_ode_clean
 use mod_rk,                  only: t_explicit_euler, t_heun_method

 implicit none

 ! Equation coefficients
 real(wp), parameter :: u  = 5.0_wp
 real(wp), parameter :: nu = 0.1_wp
 ! Discretization parameters
 integer,  parameter :: n_elem = 50
 real(wp), parameter :: x_left = 0.0_wp, x_right = 2.0_wp
 integer,  parameter :: n_step = 250
 real(wp), parameter :: dt = 0.001_wp
 integer :: degree

 ! ODE
 integer :: n
 real(wp) :: t
 type(t_1d_scalar_ad_state) :: ynow, ynew
 type(t_1d_scalar_ad_ode)   :: ode

 ! Time integrator (choose one)
 !type(t_explicit_euler) :: time_integrator
 type(t_heun_method)    :: time_integrator

 ! Output file
 integer :: file_unit
 real(wp), allocatable :: xdata(:,:)

  write(*,*) "Choose the polynomial degree: 0 or 1"
  read(*,*) degree

  ! Set-up the problem: discretization and initial condition
  call scalar_ad_ode_setup( ode, u,nu, ne=n_elem ,  &
    x_left=x_left , x_right=x_right , degree=degree )
  allocate( ynow%a(ode%base%n,ode%grid%ne) )
  allocate( ynew%a(ode%base%n,ode%grid%ne) )
  allocate(xdata(ode%base%n,ode%grid%ne))
  select case(degree)
   case(0)
    xdata(1,:) = ode%grid%e%xb
   case(1)
    xdata(1,:) = ode%grid%e%xv(1)
    xdata(2,:) = ode%grid%e%xv(2)
   case default
    error stop "Not implemented"
  end select

  ynow%a = initial_condition( xdata )

  ! Prepare the output file
  call output_file_setup()

  ! Integrate the system
  call time_integrator%setup(ynow)
  do n=1,n_step
    t = real(n,wp)*dt

    ! Compute the time step
    call time_integrator%compute_step(ynew , ode,dt,ynow)
    call ynow%copy( ynew )

    ! Write the solution
    call output_file_write_data( t=t , a=ynow%a , &
        a_ex=exact_solution(xdata,t,ode%u,ode%nu) )

  enddo

  ! Finalize the output file
  call output_file_finalize()

  write(*,*) "Done, results written in 'solve_1d_scalar_adv_diff.out'."

  ! Clean-up
  deallocate( ynow%a )
  deallocate( ynew%a )
  deallocate( xdata )
  call scalar_ad_ode_clean(ode)
  call time_integrator%clean()

contains

 pure elemental function initial_condition(x) result(a0)
  real(wp), intent(in) :: x
  real(wp) :: a0

   a0 = exact_solution(x,0.0_wp,u,nu)

 end function initial_condition

 pure elemental function exact_solution(x,t,u,nu) result(a)
  real(wp), intent(in) :: x, t, u, nu
  real(wp) :: a

  integer, parameter :: m = 4
  real(wp), parameter :: pi = 3.14159265358979323846264338327950288_wp
  real(wp) :: l

   l = (2.0_wp*pi)/(x_right-x_left) * real(m,wp)
   a = exp( -nu*l**2 * t ) * sin( l * (x-u*t) )

 end function exact_solution

 subroutine output_file_setup()
   ! Open a file and connect it to a new unit
   open(newunit=file_unit, file="solve_1d_scalar_adv_diff.out", &
        status='replace',action='write')
   ! Write the file header
   write(file_unit,'(a)') &
     'title  = "Scalar advection equation"'
   write(file_unit,'(a)') &
     'xlabel = "t"'
   write(file_unit,'(a)') &
     'ylabel = ""'
   write(file_unit,'(a)') &
     'yNames = "exact solution" , "numerical solution"'
   write(file_unit,'(a,*(e23.15,:,","))') &
     'xdata  = ', xdata
 end subroutine output_file_setup

 subroutine output_file_finalize()
   ! Close the file
   close(unit=file_unit)
 end subroutine output_file_finalize

 subroutine output_file_write_data(t,a,a_ex)
  real(wp), intent(in) :: t
  real(wp), intent(in) :: a(:,:)
  real(wp), intent(in) :: a_ex(:,:)

   write(file_unit,'(a,e23.15)') &
     'time   = ',t
   write(file_unit,'(a,*(e23.15,:,","))') &
     '"exact solution"     = ',a_ex
   write(file_unit,'(a,*(e23.15,:,","))') &
     '"numerical solution" = ',a
 end subroutine output_file_write_data

end program solve_1d_scalar_adv_diff

