module mod_1d_scalar_ad_ode

 use mod_kinds, only: wp

 use mod_ode_state, only: c_ode_state

 use mod_ode_solver, only: c_ode

 use mod_linalg, only: invmat_chol

 use mod_1d_grid, only: t_grid, t_s, t_e, grid_setup, grid_clean

 use mod_1d_base, only: t_base, base_setup, base_clean

 use mod_1d_scalar_ad_state, only: t_1d_scalar_ad_state

 use mod_1d_scalar_ad_local_int, only: element_mass_matrix, &
   element_integral, side_integral, g_element_integral, g_side_integral

 implicit none

 public :: &
   t_1d_scalar_ad_ode, scalar_ad_ode_setup, scalar_ad_ode_clean

 private

 !> Compute the rhs resulting from the DG discretization of
 !! \[
 !!  \partial_t a +\partial_x( \V{u}a ) = 0
 !! \]
 !! where u is a constant.
 type, extends(c_ode) :: t_1d_scalar_ad_ode
  real(wp) :: u  !< advection coefficient
  real(wp) :: nu !< diffusion coefficient
  type(t_grid) :: grid
  type(t_base) :: base
  !> \(\left(M^K\right)^{-1}\) for all the elements
  real(wp), allocatable :: imass_matrices(:,:,:)
 contains
  procedure, pass(ode) :: compute_rhs => compute_1d_scalar_ad_rhs
 end type t_1d_scalar_ad_ode

contains

!-----------------------------------------------------------------------

 subroutine scalar_ad_ode_setup(ode, u,nu, ne,x_left,x_right,degree)
  real(wp), intent(in) :: u, nu           ! problem coefficients
  integer,  intent(in) :: ne              ! number of elements
  real(wp), intent(in) :: x_left, x_right ! domain size
  integer,  intent(in) :: degree          ! polynomial degree
  type(t_1d_scalar_ad_ode), intent(out) :: ode

  integer :: ie
  real(wp), allocatable :: m(:,:) ! work array for mass matrix

   ode%u  = u
   ode%nu = nu

   call grid_setup(ode%grid,ne,x_left,x_right)
   call base_setup(ode%base,degree)

   ! precompute the mass matrices
   allocate(m(ode%base%n,ode%base%n))
   allocate( ode%imass_matrices(ode%base%n,ode%base%n,ode%grid%ne) )
   do ie=1,ode%grid%ne
     ! mass matrix
     call element_mass_matrix( m, ode%base, ode%grid%e(ie) )
     ! invert the mass matrix
     call invmat_chol( m , ode%imass_matrices(:,:,ie) )
   enddo
   deallocate(m)

 end subroutine scalar_ad_ode_setup

 subroutine scalar_ad_ode_clean(ode)
  type(t_1d_scalar_ad_ode), intent(inout) :: ode

   deallocate( ode%imass_matrices )
   call base_clean(ode%base)
   call grid_clean(ode%grid)

 end subroutine scalar_ad_ode_clean

!-----------------------------------------------------------------------

 subroutine compute_1d_scalar_ad_rhs(rhs,ode,y)
  class(t_1d_scalar_ad_ode), intent(in) :: ode
  class(c_ode_state), intent(in)    :: y
  class(c_ode_state), intent(inout) :: rhs

  real(wp) :: g( ode%base%n , ode%grid%ne ) ! g = grad(a)

   select type(y);   type is(t_1d_scalar_ad_state)
   select type(rhs); type is(t_1d_scalar_ad_state)

   call compute_auxiliary_variable(g,ode,y)

   call compute_rhs(rhs,ode,y,g)

   end select
   end select
 end subroutine compute_1d_scalar_ad_rhs

!-----------------------------------------------------------------------

 pure subroutine compute_auxiliary_variable(g,ode,y)
  class(t_1d_scalar_ad_ode),   intent(in) :: ode
  class(t_1d_scalar_ad_state), intent(in) :: y
  real(wp), intent(out) :: g(:,:) !! \( \V{g} \approx \Grad a \)

  integer :: ie ! element index
  integer :: is ! side index
  integer :: e1, e2

   ! initialize g
   g = 0.0_wp

   ! element integrals
   do ie=1,ode%grid%ne
     call g_element_integral(g(:,ie), ode%base,y%a(:,ie))
   enddo

   ! side fluxes
   do is=1,ode%grid%ns
     associate( side => ode%grid%s(is) )
     e1 = side%e1
     e2 = side%e2
     call g_side_integral( s_rhs1=g(:,e1) , s_rhs2=g(:,e2),   &
       base=ode%base , s=side , ak1=y%a(:,e1) , ak2=y%a(:,e2) )
     end associate
   enddo

   ! include the mass matrix
   do ie=1,ode%grid%ne
     g(:,ie) = matmul(ode%imass_matrices(:,:,ie) , g(:,ie))
   enddo

 end subroutine compute_auxiliary_variable

!-----------------------------------------------------------------------

 pure subroutine compute_rhs(rhs,ode,y,g)
  class(t_1d_scalar_ad_ode),   intent(in)    :: ode
  class(t_1d_scalar_ad_state), intent(in)    :: y
  real(wp),                    intent(in)    :: g(:,:)
  class(t_1d_scalar_ad_state), intent(inout) :: rhs

  integer :: ie ! element index
  integer :: is ! side index
  integer :: e1, e2

   ! initialize rhs
   rhs%a = 0.0_wp

   do ie=1,ode%grid%ne
     associate( ak => y%a(:,ie) , gk => g(:,ie) )
     call element_integral(rhs%a(:,ie), ode%u,ode%nu, ode%base,ak,gk)
     end associate
   enddo

   ! side fluxes
   do is=1,ode%grid%ns
     associate( side => ode%grid%s(is) )
     e1 = side%e1
     e2 = side%e2
     associate( ak1 => y%a(:,e1) , ak2 => y%a(:,e2) , &
                gk1 => g(:,  e1) , gk2 => g(:,  e2) )
     call side_integral( s_rhs1=rhs%a(:,e1) , s_rhs2=rhs%a(:,e2), &
                      u=ode%u,nu=ode%nu, base=ode%base , s=side , &
                      ak1=ak1 , ak2=ak2 , gk1=gk1 , gk2=gk2 )
     end associate
     end associate
   enddo

   ! include the mass matrix
   do ie=1,ode%grid%ne
     rhs%a(:,ie) = matmul(ode%imass_matrices(:,:,ie) , rhs%a(:,ie))
   enddo

 end subroutine compute_rhs

!-----------------------------------------------------------------------

end module mod_1d_scalar_ad_ode
