module mod_1d_scalar_ad_state

 use mod_kinds, only: wp

 use mod_ode_state, only: c_ode_state

 implicit none

 public :: t_1d_scalar_ad_state

 private

 ! Using constant finite elements (i.e. finite volumes) there is one
 ! degree of freedom per element.
 type, extends(c_ode_state) :: t_1d_scalar_ad_state
  real(wp), allocatable :: a(:,:) ! degrees of freedom
 contains
  procedure, pass(x) :: increment       => increment
  procedure, pass(x) :: scalar_multiply => scalar_multiply
  procedure, pass(y) :: copy            => copy
  procedure, pass(x) :: clone           => clone
 end type t_1d_scalar_ad_state

contains

 subroutine increment(x,y)
  class(c_ode_state),           intent(in)    :: y
  class(t_1d_scalar_ad_state), intent(inout) :: x

   select type(y); type is(t_1d_scalar_ad_state)
   x%a = x%a + y%a
   end select
 end subroutine increment

 subroutine scalar_multiply(x,r)
  real(wp),                     intent(in)    :: r
  class(t_1d_scalar_ad_state), intent(inout) :: x

   x%a = r*x%a
 end subroutine scalar_multiply

 subroutine copy(y,x)
  class(c_ode_state),           intent(in)    :: x
  class(t_1d_scalar_ad_state), intent(inout) :: y

   select type(x); type is(t_1d_scalar_ad_state)
   y%a = x%a
   end select
 end subroutine copy

 subroutine clone(y,x)
  class(t_1d_scalar_ad_state),    intent(in)  :: x
  class(c_ode_state), allocatable, intent(out) :: y

   allocate( y , source=x )
 end subroutine clone

end module mod_1d_scalar_ad_state

