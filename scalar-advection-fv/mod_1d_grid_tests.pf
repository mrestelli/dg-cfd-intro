module mod_1d_grid_tests

 use mod_kinds, only: wp

 use mod_1d_grid, only: t_grid, grid, grid_setup, grid_clean

 ! This module is provided by the unit testing framework and must be
 ! used in every test collection.
 use pfunit_mod

 implicit none

 public

contains

!-----------------------------------------------------------------------

 @Before
 subroutine mod_1d_grid_tests_setup()

   call grid_setup( ne=5 , x_left=0.0_wp , x_right=1.0_wp )

 end subroutine mod_1d_grid_tests_setup

 @After
 subroutine mod_1d_grid_tests_teardown()
 
   call grid_clean()

 end subroutine mod_1d_grid_tests_teardown

!-----------------------------------------------------------------------

 @Test
 subroutine grid_set_up__sets_number_os_elements_and_sides()

   ! same number of sides and elements (periodic bcs)
   @assertEqual( 5 , grid%ns )
   @assertEqual( 5 , grid%ne )

 end subroutine grid_set_up__sets_number_os_elements_and_sides

!-----------------------------------------------------------------------

 @Test
 subroutine grid__given_side__recover_connected_elements()

  integer :: ie1, ie2

   ! elements connected to side 3
   ie1 = grid%s(3)%e1
   ie2 = grid%s(3)%e2

   @assertEqual( 2 , ie1 )
   @assertEqual( 3 , ie2 )

 end subroutine grid__given_side__recover_connected_elements

!-----------------------------------------------------------------------

 @Test
 subroutine grid__given_side__recover_connected_elements__periodic()

  integer :: e1, e2

   ! elements connected to side 1: needs periodic bcs
   e1 = grid%s(1)%e1
   e2 = grid%s(1)%e2

   @assertEqual( 5 , e1 )
   @assertEqual( 1 , e2 )

 end subroutine grid__given_side__recover_connected_elements__periodic

!-----------------------------------------------------------------------

 @Test
 subroutine grid__check_normal_unit_vector()

   ! all the normal unit vectors are simply 1
   @assertEqual( [1.0_wp,1.0_wp,1.0_wp,1.0_wp,1.0_wp] , grid%s%n )

 end subroutine grid__check_normal_unit_vector

!-----------------------------------------------------------------------

 @Test
 subroutine grid__check_element_area()

   ! all the elements have the same area: 1/5
   @assertEqual( [0.2_wp,0.2_wp,0.2_wp,0.2_wp,0.2_wp] , grid%e%area )

 end subroutine grid__check_element_area

!-----------------------------------------------------------------------

 @Test
 subroutine grid__check_element_centers()

   ! all the elements have the same area: 1/5
   @assertEqual( [0.1_wp,0.3_wp,0.5_wp,0.7_wp,0.9_wp] , grid%e%xb )

 end subroutine grid__check_element_centers

!-----------------------------------------------------------------------

end module mod_1d_grid_tests
