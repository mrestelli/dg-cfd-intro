module mod_1d_grid

 use mod_kinds, only: wp
 
 implicit none

 public :: t_grid, grid, grid_setup, grid_clean, t_s, t_e

 private

 type :: t_s
  integer  :: e1 ! first connected element
  integer  :: e2 ! second connected element
  real(wp) :: n  ! normal unit vector from e1 to e2
 end type t_s

 type :: t_e
  real(wp) :: xb ! center of the element
  real(wp) :: area
 end type t_e

 type :: t_grid
  integer :: ns ! number of sides
  integer :: ne ! number of elements
  type(t_s), allocatable :: s(:) ! sides
  type(t_e), allocatable :: e(:) ! elements
 end type t_grid

 type(t_grid) :: grid

contains

 ! Setup the module variable grid
 subroutine grid_setup(ne,x_left,x_right)
  integer, intent(in) :: ne
  real(wp), intent(in) :: x_left, x_right ! domain size

  integer :: is, ie

   ! allocate and define the side array
   allocate( grid%s( grid%ns ) )
   ! todo

   ! allocate and define the element array
   allocate( grid%e( grid%ne ) )
   ! todo

 end subroutine grid_setup
 
 subroutine grid_clean()
  deallocate( grid%s )
  deallocate( grid%e )
 end subroutine grid_clean

end module mod_1d_grid
