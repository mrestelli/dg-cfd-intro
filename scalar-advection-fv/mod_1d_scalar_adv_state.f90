module mod_1d_scalar_adv_state

 use mod_kinds, only: wp

 use mod_ode_state, only: c_ode_state

 implicit none

 public :: t_1d_scalar_adv_state

 private

 ! Using constant finite elements (i.e. finite volumes) there is one
 ! degree of freedom per element.
 type, extends(c_ode_state) :: t_1d_scalar_adv_state
  real(wp), allocatable :: a(:) ! element constant values
 contains
  procedure, pass(x) :: increment       => increment
  procedure, pass(x) :: scalar_multiply => scalar_multiply
  procedure, pass(y) :: copy            => copy
  procedure, pass(x) :: clone           => clone
 end type t_1d_scalar_adv_state

contains

 subroutine increment(x,y)
  class(c_ode_state),           intent(in)    :: y
  class(t_1d_scalar_adv_state), intent(inout) :: x

 end subroutine increment

 subroutine scalar_multiply(x,r)
  real(wp),                     intent(in)    :: r
  class(t_1d_scalar_adv_state), intent(inout) :: x

 end subroutine scalar_multiply

 subroutine copy(y,x)
  class(c_ode_state),           intent(in)    :: x
  class(t_1d_scalar_adv_state), intent(inout) :: y

 end subroutine copy

 subroutine clone(y,x)
  class(t_1d_scalar_adv_state),    intent(in)  :: x
  class(c_ode_state), allocatable, intent(out) :: y

 end subroutine clone

end module mod_1d_scalar_adv_state

