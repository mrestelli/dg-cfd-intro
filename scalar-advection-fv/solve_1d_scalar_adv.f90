program solve_1d_scalar_adv

 use mod_kinds,               only: wp
 use mod_1d_grid,             only: t_grid, grid, grid_setup, grid_clean
 use mod_1d_scalar_adv_state, only: t_1d_scalar_adv_state
 use mod_1d_scalar_adv_ode,   only: t_1d_scalar_adv_ode, u
 use mod_rk,                  only: t_explicit_euler, t_heun_method

 implicit none

 ! Discretization parameters
 integer,  parameter :: n_elem = 50
 real(wp), parameter :: x_left = 0.0_wp, x_right = 2.0_wp
 integer,  parameter :: n_step = 100
 real(wp), parameter :: dt = 0.01_wp
 !real(wp), parameter :: dt = 0.03225_wp ! unstable time step

 ! ODE
 integer :: n
 real(wp) :: t
 type(t_1d_scalar_adv_state) :: ynow, ynew
 type(t_1d_scalar_adv_ode)   :: ode

 ! Time integrator (choose one)
 !type(t_explicit_euler) :: time_integrator
 type(t_heun_method)    :: time_integrator

 ! Output file
 integer :: file_unit


  ! Set-up the problem: discretization and initial condition
  call grid_setup( ne=n_elem , x_left=x_left , x_right=x_right )
  allocate( ynow%a(grid%ne) )
  allocate( ynew%a(grid%ne) )
  ynow%a = initial_condition( grid%e%xb )

  ! Prepare the output file
  call output_file_setup()

  ! Integrate the system
  call time_integrator%setup(ynow)
  do n=1,n_step
    t = real(n,wp)*dt

    ! Compute the time step
    call time_integrator%compute_step(ynew , ode,dt,ynow)
    call ynow%copy( ynew )

    ! Write the solution
    call output_file_write_data( t=t , a=ynow%a , &
                 a_ex=exact_solution(grid%e%xb,t) )

  enddo

  ! Finalize the output file
  call output_file_finalize()

  ! Clean-up
  deallocate( ynow%a )
  deallocate( ynew%a )
  call grid_clean()

contains

 pure elemental function initial_condition(x) result(a0)
  real(wp), intent(in) :: x
  real(wp) :: a0

  integer, parameter :: m = 2
  real(wp), parameter :: pi = 3.14159265358979323846264338327950288_wp

   a0 = sin( (2.0_wp*pi)/(x_right-x_left) * real(m,wp) * x )
 end function initial_condition

 pure elemental function exact_solution(x,t) result(a)
  real(wp), intent(in) :: x, t
  real(wp) :: a

   a = initial_condition(x-u*t)
 end function exact_solution

 subroutine output_file_setup()
   ! Open a file and connect it to a new unit
   open(newunit=file_unit, file="solve_1d_scalar_adv.out", &
        status='replace',action='write')
   ! Write the file header
   write(file_unit,'(a)') &
     'title  = "Scalar advection equation"'
   write(file_unit,'(a)') &
     'xlabel = "t"'
   write(file_unit,'(a)') &
     'ylabel = ""'
   write(file_unit,'(a)') &
     'yNames = "exact solution" , "numerical solution"'
   write(file_unit,'(a,*(e23.15,:,","))') &
     'xdata  = ', grid%e%xb
 end subroutine output_file_setup

 subroutine output_file_finalize()
   ! Close the file
   close(unit=file_unit)
 end subroutine output_file_finalize

 subroutine output_file_write_data(t,a,a_ex)
  real(wp), intent(in) :: t
  real(wp), intent(in) :: a(:)
  real(wp), intent(in) :: a_ex(:)

   write(file_unit,'(a,e23.15)') &
     'time   = ',t
   write(file_unit,'(a,*(e23.15,:,","))') &
     '"exact solution"     = ',a_ex
   write(file_unit,'(a,*(e23.15,:,","))') &
     '"numerical solution" = ',a
 end subroutine output_file_write_data

end program solve_1d_scalar_adv

