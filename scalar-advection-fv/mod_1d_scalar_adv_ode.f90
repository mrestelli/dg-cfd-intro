module mod_1d_scalar_adv_ode

 use mod_kinds, only: wp

 use mod_ode_state, only: c_ode_state

 use mod_ode_solver, only: c_ode

 use mod_1d_grid, only: t_grid, grid, t_s

 use mod_1d_scalar_adv_state, only: t_1d_scalar_adv_state

 implicit none

 public :: t_1d_scalar_adv_ode, u, upwind_flux

 private

 ! Compute the rhs resulting from the DG discretization with constant
 ! finite elements (i.e. finite volumes) for the equation
 !
 !  da/dt + d/dx( u*a ) = 0
 !
 ! where u is a constant.
 type, extends(c_ode) :: t_1d_scalar_adv_ode
 contains
  procedure, nopass :: compute_rhs => compute_1d_scalar_adv_rhs
 end type t_1d_scalar_adv_ode

 real(wp), parameter :: u = 1.5_wp ! advection coefficient

contains

 subroutine compute_1d_scalar_adv_rhs(rhs,y)
  class(c_ode_state), intent(in)    :: y
  class(c_ode_state), intent(inout) :: rhs

  integer :: ie ! element index
  integer :: is ! side index
  integer :: e1, e2
  real(wp) :: n
  real(wp) :: flux1, flux2, numflux

 end subroutine compute_1d_scalar_adv_rhs

 pure function upwind_flux(u,n,flux1,flux2) result(upwflux)
  real(wp), intent(in) :: u     ! advection velocity
  real(wp), intent(in) :: n     ! unit normal form e1 to e2
  real(wp), intent(in) :: flux1 ! flux on e1
  real(wp), intent(in) :: flux2 ! flux on e2
  real(wp) :: upwflux
   
 end function upwind_flux

end module mod_1d_scalar_adv_ode
