! State variable
!
! Given two state variables x,y and a scalar r, the following
! operations are defined:
!
!   x%increment(y)        ->  x = x + y
!   x%scalar_multiply(r)  ->  x = r*x
!   y%copy(x)             ->  y = x
!
!   x%clone(y)            ->  allocate y to have the same type and
!                             value as x
!  
module mod_ode_state

 use mod_kinds, only: wp

 implicit none

 public :: c_ode_state

 private

 ! State variable
 type, abstract :: c_ode_state
 contains
  procedure(i_increment),       deferred, pass(x) :: increment
  procedure(i_scalar_multiply), deferred, pass(x) :: scalar_multiply
  procedure(i_copy),            deferred, pass(y) :: copy
  procedure(i_clone),           deferred, pass(x) :: clone
 end type c_ode_state

 abstract interface
  subroutine i_increment(x,y)
   import :: c_ode_state
   implicit none
   class(c_ode_state), intent(in)    :: y
   class(c_ode_state), intent(inout) :: x
  end subroutine i_increment
 end interface

 abstract interface
  subroutine i_scalar_multiply(x,r)
   import :: wp, c_ode_state
   implicit none
   real(wp),           intent(in)    :: r
   class(c_ode_state), intent(inout) :: x
  end subroutine i_scalar_multiply
 end interface

 abstract interface
  subroutine i_copy(y,x)
   import :: c_ode_state
   implicit none
   class(c_ode_state), intent(in)    :: x
   class(c_ode_state), intent(inout) :: y
  end subroutine i_copy
 end interface

 abstract interface
  subroutine i_clone(y,x)
   import :: c_ode_state
   implicit none
   class(c_ode_state),              intent(in)  :: x
   class(c_ode_state), allocatable, intent(out) :: y
  end subroutine i_clone
 end interface

end module mod_ode_state

