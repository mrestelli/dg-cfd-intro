# To use this include file, define the following variables:
#
# PFUNIT          : root directory of the pFUnit test framework
# FC FFLAGS       : fortran compiler and corresponding flags
# LD LDFLAGS      : linker and corresponding flags
# OBJ_MOD         : module object files for the production code

# For a production code module  prod_code_module.f90  the test module
# must be called  prod_code_module_tests.pf  and must be listed in
# PFUNIT_TEST_MOD.

# pFUnit test modules: extension .pf
PFUNIT_TEST_MOD=mod_rk_tests.pf \
  mod_linalg_tests.pf \
  mod_scalar_ad_local_int_tests.pf \
  mod_ns_local_int_tests.pf

PFUNIT_PP=$(PFUNIT)/bin/pFUnitParser.py
PFUNIT_INC=-I$(PFUNIT)/mod -I$(PFUNIT)/include 
PFUNIT_DRIVER=$(PFUNIT)/include/driver.F90
PFUNIT_LDFLAGS=-L$(PFUNIT)/lib
PFUNIT_LIBS=-lpfunit

# pFUnit source and object files
PFUNIT_SRC=$(patsubst %.pf,%.F90,$(PFUNIT_TEST_MOD))
PFUNIT_OBJ=$(patsubst %.pf,%.o,  $(PFUNIT_TEST_MOD))
PFUNIT_MOD=$(patsubst %.pf,%.mod,$(PFUNIT_TEST_MOD)) \
           $(patsubst %.pf,wrap%.mod,$(PFUNIT_TEST_MOD))

.PRECIOUS: %_tests.F90 %_tests.o

.PHONY: tests
tests: pFUnit-tests

# pFUnit preprocessor
$(PFUNIT_SRC): %_tests.F90: %_tests.pf
	$(RECIPE)

# Compile the _tests.F90 test collections
$(PFUNIT_OBJ): %_tests.o: %_tests.F90 %.o
	$(RECIPE)

# Compile the pFUnit driver
driver.o: testSuites.inc
	$(RECIPE)

# Link the test program
pFUnit-tests: $(OBJ_MOD) $(PFUNIT_OBJ) driver.o
	$(RECIPE)

.PHONY: clean-tests
clean-tests:
	$(TEST_CLEAN_RECIPE)

# Dependencies --------------------------------------------------------

mod_scalar_ad_local_int_tests.o: \
 mod_1d_grid.o \
 mod_2d_grid.o

# Makefile logic: dispatching and doing real work ---------------------

# Define the recipe: 
# 1) during the dispatch phase: cd to the correct directory and make
# 2) during the make phase: preproc, compile, link or clean
ifeq (DISPATCH,$(PHASE))

# For each target, specify the working directory; then use the general
# rule from the main Makefile
$(PFUNIT_SRC): WDIR=preproc
$(PFUNIT_OBJ): WDIR=build
driver.o:      WDIR=build
pFUnit-tests:  WDIR=bin

# Clean-up
TEST_CLEAN_RECIPE = \
  +$(MAKE) -C preproc -f $(ROOTDIR)/Makefile $@ && \
   $(MAKE) -C build   -f $(ROOTDIR)/Makefile $@ && \
   $(MAKE) -C bin     -f $(ROOTDIR)/Makefile $@
	
clean: clean-tests

else

$(PFUNIT_SRC): RECIPE=$(PFUNIT_PP) $< $@

$(PFUNIT_OBJ): RECIPE=$(FC) $(FFLAGS) -ffree-line-length-300  $(PFUNIT_INC) -c $< -o $@

driver.o:      RECIPE=$(FC) $(filter-out -pedantic -Wall -Wconversion-extra -fcheck=all, $(FFLAGS)) $(PFUNIT_INC) -I$(ROOTDIR)/tests -c $(PFUNIT_DRIVER) -o $@

pFUnit-tests:  RECIPE= $(LD) $(PFUNIT_LDFLAGS) $(LDFLAGS) $^ $(PFUNIT_LIBS) -o $@

ifeq (preproc,$(strip $(notdir $(CURDIR))))
  TEST_CLEAN_RECIPE = $(RM) $(PFUNIT_SRC)
else ifeq (build,$(strip $(notdir $(CURDIR))))
  TEST_CLEAN_RECIPE = $(RM) $(PFUNIT_OBJ) driver.o $(PFUNIT_MOD)
else ifeq (bin,$(strip $(notdir $(CURDIR))))
  TEST_CLEAN_RECIPE = $(RM) pFUnit-tests
endif

endif

